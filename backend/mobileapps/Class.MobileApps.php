<?php


class MobileApps{

	const MODULE_NAME = 'mobileapps';

	const ANDROID_PUBLIC_KEY = 'a8340e5fbd10aa7727195fb239c0a902';
	const ANDROID_PRIVATE_KEY = 'cae3c45c398fbfcd0b5bafbe52948fd3';

	public static function getAndroidQRToken(PDO $db, User $__user, $regenerate = false){
		$time = time();
		$app = new ApiApplication($db, self::ANDROID_PUBLIC_KEY, $time, md5($time. self::ANDROID_PRIVATE_KEY));
		if ($regenerate){
			return $__user->generateToken($app);
		}else{
			return $__user->getToken($app);
		}

	}
}