<?php
require_once 'Class.MobileApps.php';

$__modules[MobileApps::MODULE_NAME] = array(
	'GET' => array(
		'token' => function() use ($__user, $__args, $__db){
			if ($__args[0] == 'android'){
				if (isset($__args[1]) && $__args[1] == 'regenerate'){
					$regenerate = true;
				}else{
					$regenerate = false;
				}
				return MobileApps::getAndroidQRToken($__db, $__user, $regenerate);
			}
		}
	)
);