<?php

require_once $ROOT_PATH . '/backend/core/kaf/Class.AcademicDepartment.php';
require_once $ROOT_PATH . '/backend/core/institute/Class.Institute.php';

class StudentGroup {

    const MODULE_NAME = 'student_group';
    const MIN_LEVEL_FOR_UPDATE = 5;

    private $id;
    private $profile;
    private $program;
    private $edu_speciality_name;
    private $academic_department_id;
    private $shift;
    private $course;
    private $name;
    private $number;
    private $institute;
    private $academic_department;

    private $db;


    /**
     * @param $group_id
     * @param PDO $db
     * @throws DBQueryException
     */
    public function __construct($group_id, PDO $db) {
        $q_get_st_group = 'SELECT view_edu_groups.*
				FROM view_edu_groups
				WHERE view_edu_groups.edu_group_id=:edu_group_id';
        $p_get = $db->prepare($q_get_st_group);
        $p_get->execute(array(':edu_group_id' => $group_id));
        if ($p_get === FALSE) throw new DBQueryException('', $db);

        if (!$r_st_group = $p_get->fetch()) throw new InvalidArgumentException("no student group with id {$group_id}");
        $this->db = $db;
        $this->id = (int)$r_st_group['edu_group_id'];
        $this->program = $r_st_group['program'];
        $this->profile = $r_st_group['profile'];
        $this->edu_speciality_name = $r_st_group['edu_speciality_name'];
        $this->number = $r_st_group['group'];
        $this->academic_department_id = (int)$r_st_group['academic_department_id'];
        $this->course = (int)$r_st_group['course'];
        $this->name = $this->program != '' ? $this->program : ($this->profile != '' ? $this->profile : $this->edu_speciality_name);
    }

    /**
     * @return mixed
     */
    public function getShift() {
        return $this->shift;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return Institute
     */
    public function getInstitute() {
        if ($this->institute instanceof Institute) {
            return $this->institute;
        }
        $kaf = $this->getAcademicDepartment();
        $this->institute = new Institute($kaf->getInstituteId(), $this->db);
        return $this->institute;
    }

    /**
     * @return AcademicDepartment
     */
    private function getAcademicDepartment() {
        if ($this->academic_department instanceof AcademicDepartment) {
            return $this->academic_department;
        }
        $this->academic_department = new AcademicDepartment($this->academic_department_id, $this->db);
        return $this->academic_department;
    }

    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'name' => $this->name,
            'number' => $this->number,
            'academic_department_id' => $this->academic_department_id,
            'shift' => $this->shift,
            'course' => $this->course,
            'institute_name' => $this->getInstitute()->getName(),
            'kaf_name' => $this->getAcademicDepartment()->getName()
        ));
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    public function getStudentsList() {
        $q_get_students = 'SELECT last_name, first_name, middle_name, id, id as user_id, student_id
			FROM view_users
			WHERE view_users.edu_group_id = :edu_group_id
			ORDER BY last_name, first_name
		';
        $p_st = $this->db->prepare($q_get_students);
        $p_st->execute(array(
            ':edu_group_id' => $this->getId()
        ));
        if ($p_st === FALSE) throw new DBQueryException('', $this->db);
        return new Result(true, '', $p_st->fetchAll());
    }

    /**
     * Get students list with passport and registration address info
     * @return Result
     * @throws DBQueryException
     */
    public function getConfidentialInfoStudentsList() {
        $q_get_students = "SELECT view_users.id, view_users.last_name, view_users.first_name, view_users.middle_name,
                            users.email,
                            users.birth_date,
                            users.registration_address_id,
                            users.residence_address_id,
                            users.snils,
                            users.agreement,
                            users.phone_number,

                            addresses.id AS address_id,
                            addresses.country,
                            addresses.region,
                            addresses.district,
                            addresses.city,
                            addresses.street,
                            addresses.building,
                            addresses.flat,

                            passports.id AS passport_id,
                            passports.series,
                            passports.number,
                            passports.issue_place,
                            passports.issue_place_code,
                            passports.issue_date,
                            passports.doc_type_id
                            FROM view_users
                                JOIN users ON view_users.id = users.id
                                JOIN passports ON users.passport_id = passports.id
                                JOIN addresses ON addresses.id = users.registration_address_id
			                WHERE view_users.edu_group_id = :edu_group_id";
        $p_get_students = $this->db->prepare($q_get_students);
        $p_get_students->execute(array(
            ':edu_group_id' => $this->getId()
        ));
        if ($p_get_students === FALSE) throw new DBQueryException('Can not retrieve student information', $this->db);
        return new Result(true, '', $p_get_students->fetchAll());
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    public function getSubjects() {
        $q_get_sub = 'SELECT DISTINCT subject_name, subject_id
				FROM view_timetable
				WHERE edu_group_id = :edu_group_id';
        $p_subs = $this->db->prepare($q_get_sub);
        $p_subs->execute(array(
            ':edu_group_id' => $this->id
        ));
        if (!$p_subs) throw new DBQueryException('', $this->db);

        return new Result(true, '', $p_subs->fetchAll());
    }

}