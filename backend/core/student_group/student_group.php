<?php

require_once 'Class.StudentGroup.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/timetable/Class.Timetable.php';

$__modules[StudentGroup::MODULE_NAME] = array(
	'GET' => array(
		'{id:[0-9]+}' => function($id, $act) use ($__user, $__db){
			if ($act == 'timetable'){
				$student_group = new StudentGroup($id, $__db);
				$tt = new Timetable($student_group, $__db);
				return $tt->get();
			}elseif ($act == 'shift'){
				$student_group = new StudentGroup($id, $__db);
				return new Result(true, '', array('shift' => $student_group->getShift()));
			}
		}
	),
	'POST' => array(
		'{id:[0-9]+}' => function($id, $act) use ($__db, $__request){
			if($act == 'timetable'){
				$student_group = new StudentGroup($id, $__db);
				$timetable = new Timetable($student_group, $__db);
				return $timetable->update($__request);
			}
		}
	),
	'PUT' => array(
		'{id:[0-9]+}' => function($id, $act) use ($__db, $__request){
			if ($act == 'shift'){
				$student_group = new StudentGroup($id, $__db);
				$student_group->setShift($__request['shift']);
				return $student_group->update();
			}
		}
	),
);

/*switch ($method_name){

	case 'setShift': {
		$student_group = new StudentGroup($_REQUEST['student_group_id'], $__db);
		$student_group->setShift($_REQUEST['shift']);
		$result = $student_group->update();
		break;
	}
	case 'getTimetable': {
		break;
	}
}*/