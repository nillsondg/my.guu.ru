<?php

/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 16.10.2014
 * Time: 13:38
 */
class Institute {

    const MODULE_NAME = 'institute';
//  const MIN_LEVEL_FOR_READ = 10;
//  const MIN_LEVEL_FOR_UPDATE = 10;

    private $id;
    private $code;
    private $name;
    private $abbr;
    private $ou_id;
    private $soccardXML;
    private $created_at;
    private $updated_at;


    /**
     * @param $id
     * @param PDO $db
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public function __construct($id, PDO $db) {
        $this->db = $db;
        $this->id = (int)$id;
        $q_get_institute = 'SELECT institutes.*
				FROM  institutes
				WHERE institutes.id = :id';
        $p_get_institute = $db->prepare($q_get_institute);
        $p_get_institute->execute(array(
            ':id' => $id
        ));
        if ($p_get_institute === FALSE) throw new DBQueryException('Can not retrieve institute information', $db);
        $result = $p_get_institute->fetch();
        if (!$result) throw new InvalidArgumentException("No institute with id {$id}");

        $this->code = (int)$result['code'];
        $this->name = $result['name'];
        $this->abbr = $result['abbr'];
        $this->ou_id = (int)$result['ou_id'];
        $this->soccardXML = $result['soccardXML'];
        $this->created_at = (int)$result['created_at'];
        $this->updated_at = $result['updated_at'];
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get all information about institute
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'abbr' => $this->abbr,
            'ou_id' => $this->ou_id,
            'soccardXML' => $this->soccardXML,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
     * Get students groups in institute
     * @throws DBQueryException
     * @throws LogicException
     */
    public function getStudentsGroups() {
        $q_get_groups = "SELECT view_edu_groups.*
                FROM view_edu_groups
                WHERE view_edu_groups.institute_id = :institute_id";
        $p_get_groups = $this->db->prepare($q_get_groups);
        $p_get_groups->execute(array(
            ':institute_id' => $this->id
        ));
        if ($p_get_groups === FALSE) throw new DBQueryException('Can not retrieve students groups', $this->db);
        if ($res = $p_get_groups->fetchAll()) {
            foreach ($res as &$group)
                foreach ($group as &$value)
                    if (is_numeric($value))
                        $value = (int)$value;
        } else
            throw new LogicException('No groups in institute');
        return new Result(true, '', $res);
    }

    /**
     * DEPRECATED
     * forms part of the sql query to filter students group by parameters
     * @param $conditions
     * @return array
     */
    private function getConditionsValues($conditions) {
        $result = array(
            'str' => array(),
            'fields' => array()
        );
        if ($conditions == null || count($conditions) == 0) {
            $result['str'] = '';
            return $result;
        }
        foreach ($conditions as $condition => $value) {
            $str = null;
            $field = null;
            switch ($condition) {
                case 'shift': {
                    $str = ' student_groups.shift = :shift';
                    $field = ':shift';
                    break;
                }
                case 'course': {
                    $str = ' student_groups.course = :course';
                    $field = ':course';
                    break;
                }
            }
            if ($str != null) {
                $result['str'][$field] = $str;
                $result['fields'][$field] = $value;
            }
        }
        if (count($result['str']) > 0) {
            $result['str'] = ' AND ' . implode(' AND ', $result['str']);
        }
        return $result;
    }

    /*public function getStudentGroups($conditions = null){
        //$n_conditions = $this->getConditionsValues($conditions);
        $q_get_groups = "SELECT student_groups.*
            FROM student_groups
            INNER JOIN kafs_view ON kafs_view.kaf_id = student_groups.kaf_id
            INNER JOIN institutes_view ON kafs_view.institute_id = institutes_view.institute_id
            WHERE  institutes_view.institute_id=:institute_id
            {$n_conditions['str']}";
        $p_get_groups = $this->db->prepare($q_get_groups);
        $p_get_groups->execute(array_merge(array(
            ':institute_id' => $this->institute_id
        ), $n_conditions['fields']));
        if ($p_get_groups === FALSE) throw new DBQueryException('Не удалось получить список групп в институте', $this->db);
        return new Result(true, 'Данные получены', $p_get_groups->fetchAll());
    }
    */
    public function getStaff() {
        global $__user;
        if ($__user->hasRights(array(
            'module' => 'institute',
            'min_level' => '10'
        ))
        ) {

        }
    }

    public function getName() {
        return $this->name;
    }

    public function addErrorRequest($data) {
        global $__user;
        $q_ins_req = 'INSERT INTO users_error(from_user_id, description, status, solve, create_date, edu_group_id)
				VALUES (:from_user_id, :description, 1, null, NOW(), :edu_group_id)';

        $p_ins = $this->db->prepare($q_ins_req);
        $p_ins->execute(array(
            ':from_user_id' => $__user->getId(),
            ':description' => $data['description'],
            ':edu_group_id' => isset($data['edu_group_id']) ? $data['edu_group_id'] : null
        ));
        if ($p_ins === FALSE) throw new DBQueryException('', $this->db);
        return new Result(true, 'Заявка успешно отправлена. Скоро она будет обработана и появится в списке заявок на исправление');
    }

    //TODO
    public function updateStudentsPassport($data, PDO $__pg_db) {

        $q_ins_address = 'INSERT INTO addresses(country, region, district, city, street, building, flat, kladr)
				VALUES(:country, :region, :district, :city, :street, :building, :flat, NULL)
				RETURNING id';

        $q_upd_student = 'UPDATE users SET
				email = :email,
				birth_date = :birth_date,
				registration_address_id = :registration_address_id,
				residence_address_id = :residence_address_id,
				snils = :snils,
				updated_at = NOW(),
				agreement = :agreement,
				phone_number = :phone_number
				WHERE users.id = :id
			';

        $q_get_passport_id = 'SELECT passport_id FROM users
				WHERE users.id = :id AND passport_id IS NOT NULL
			';

        $q_upd_passport_id = 'UPDATE users SET
				passport_id = :passport_id,
				updated_at = NOW()
				WHERE users.id = :id
			';

        $q_upd_passport = 'UPDATE passports SET
				series = :series,
				number = :number,
				issue_date = :issue_date,
				issue_place = :issue_place,
				doc_type_id = :doc_type_id
				WHERE passports.id = (SELECT passport_id FROM users WHERE users.id = :id)
			';

        $q_ins_passport = 'INSERT INTO passports(series, number, issue_date, issue_place, doc_type_id)
				VALUES(:series, :number, :issue_date, :issue_place, :doc_type_id)
				RETURNING id
			';

        $p_ins_registration = $__pg_db->prepare($q_ins_address);
        $p_ins_residence = $__pg_db->prepare($q_ins_address);
        $p_user = $__pg_db->prepare($q_upd_student);
        $p_passport = $__pg_db->prepare($q_upd_passport);
        $p_get_passport_id = $__pg_db->prepare($q_get_passport_id);
        $p_ins_passport = $__pg_db->prepare($q_ins_passport);
        $p_upd_passport_id = $__pg_db->prepare($q_upd_passport_id);


        try {
            $p_ins_registration->execute(array(
                ':country' => $data['registration-country'],
                ':region' => $data['registration-region'],
                ':district' => $data['registration-district'],
                ':city' => $data['registration-city'],
                ':street' => $data['registration-street'],
                ':building' => $data['registration-building'],
                ':flat' => $data['registration-flat']
            ));
            $reg_addr_id = $p_ins_registration->fetch();
            $reg_addr_id = $reg_addr_id['id'];


            $p_ins_residence->execute(array(
                ':country' => $data['residence-country'],
                ':region' => $data['residence-region'],
                ':district' => $data['residence-district'],
                ':city' => $data['residence-city'],
                ':street' => $data['residence-street'],
                ':building' => $data['residence-building'],
                ':flat' => $data['residence-flat']
            ));
            $res_addr_id = $p_ins_residence->fetch();
            $res_addr_id = $res_addr_id['id'];

            $p_user->execute(array(
                ':email' => $data['email'],
                ':birth_date' => (string)$data['birth_date'],
                ':registration_address_id' => is_numeric($reg_addr_id) ? $reg_addr_id : NULL,
                ':residence_address_id' => is_numeric($res_addr_id) ? $res_addr_id : NULL,
                ':snils' => (string)$data['snils'],
                ':id' => (string)$data['id'],
                ':phone_number' => (string)$data['phone_number'],
                ':agreement' => (string)($data['agreement'] == '1' ? 'true' : 'false')
            ));
            if (!$p_user) throw new DBQueryException($p_user->errorCode(), $this->db);


            $p_get_passport_id->execute(array(
                ':id' => (string)$data['id'],
            ));

            if ($p_get_passport_id->rowCount() != 0) {
                $p_passport->execute(array(
                    ':series' => ($data['series'] == '') ? null : $data['series'],
                    ':number' => ($data['number'] == '') ? null : $data['number'],
                    ':issue_date' => ($data['issue_date'] == '0000-00-00') ? null : $data['issue_date'],
                    ':issue_place' => ($data['issue_place'] == '') ? null : $data['issue_place'],
                    ':doc_type_id' => ($data['doc_type_id'] == '') ? null : $data['doc_type_id'],
                    ':id' => $data['id'],
                ));
            } else {
                $p_ins_passport->execute(array(
                    ':series' => ($data['series'] == '') ? null : $data['series'],
                    ':number' => ($data['number'] == '') ? null : $data['number'],
                    ':issue_date' => ($data['issue_date'] == '0000-00-00') ? null : $data['issue_date'],
                    ':issue_place' => ($data['issue_place'] == '') ? null : $data['issue_place'],
                    ':doc_type_id' => ($data['doc_type_id'] == '') ? null : $data['doc_type_id']
                ));
                $passport_id = $p_ins_passport->fetch();
                $passport_id = $passport_id['id'];
                $p_upd_passport_id->execute(array(
                    ':passport_id' => $passport_id,
                    ':id' => $data['id']
                ));
            }

            if (!$p_passport) throw new DBQueryException($p_passport->errorCode(), $this->db);

            return new Result(true, 'Данные успешно обновлены');
        } catch (Exception $e) {
            throw $e;
        }
    }

}