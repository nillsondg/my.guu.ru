<?php

require_once $ROOT_PATH . '/backend/core/admin/Class.Admin.php';

$__modules[Admin::MODULE_NAME] = array(
	'GET' => array(
		'syncUsers' => function() use ($__db, $__user) {
			$admin = new Admin($__db);
			return $admin->syncUsers();
		},
		'usersWithRightsInModule' => function() use ($__db, $__user, $__args) {
			$admin = new Admin($__db);
			return $admin->getUsersWithRolesInModule($__args[0]);
		},
		'roles' => function() use ($__db, $__user, $__args) {
			$admin = new Admin($__db);
			return $admin->getRolesInModule($__args[0]);
		}
	),
	'PUT' => array(
		'timetable' => function() use($__db, $__user, $__request, $__args){
			$admin = new Admin($__db);
			return $admin->changeRole($__args[0], $__args[1], $__request['group_id']);
		}
	)
);