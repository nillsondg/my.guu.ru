<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 24.02.2015
 * Time: 20:37
 */

class DormRoom{
    const MODULE_NAME = 'dormitories';
    private $db;
    private $id;
    private $block_id;
    private $room_area;
    private $function;
    private $status_id;
    private $places_count;
    private $places_reserved_count;
    private $places_available_count;
    private $residents_count;
    private $residents = array();
    private $created_at;
    private $updated_at;

    private static $available = array(
        'block_id', 'room_area', 'function', 'places_count', 'places_reserved_count', 'status_id'
    );
    private static $required = array(
        'block_id', 'places_count'
    );

    /**
     * @param $id
     * @param PDO $db
     * @throws DBQueryException
     * @throws InvalidArgumentException
     */
    public function __construct( $id, PDO $db ) {
        $this->id = (int)$id;
        $this->db = $db;

        $q_get_room = 'SELECT block_id, room_area, function, status_id, places_count, places_reserved_count,
                        created_at, updated_at FROM dorm_rooms WHERE id = :id';
        $q_get_residents_id = 'SELECT id FROM dorm_residents WHERE room_id = :id';
        $p_get_room = $db->prepare($q_get_room);
        $p_get_room->execute(array( ':id' => $id ));
        if( $p_get_room === FALSE ) throw new DBQueryException('Can not retrieve dormitory room information', $db);
        if( !$result = $p_get_room->fetch() ) throw new InvalidArgumentException('No dormitory room with id {$id}');
        $this->block_id = (int)$result['block_id'];
        $this->room_area = (float)$result['room_area'];
        $this->function = $result['function'];
        $this->status_id = $result['status_id'];
        $this->places_count = (int)$result['places_count'];
        $this->places_reserved_count = (int)$result['places_reserved_count'];
        $this->created_at = $result['created_at'];
        $this->updated_at = $result['updated_at'];

        $p_get_residents = $db->prepare($q_get_residents_id);
        $p_get_residents->execute(array( ':id' => $id ));
        if( $p_get_residents === FALSE ) throw new DBQueryException('Can not retrieve residents information', $db);
        $residents_id = $p_get_residents->fetchAll();
        if( $residents_id )
            foreach( $residents_id as $id ){
                $resident = new DormResident($id, $db);
                $this->residents[] = array(
                    'id' => $resident->getId(),
                    'first_name' => $resident->getFirstName(),
                    'middle_name' => $resident->getMiddleName(),
                    'last_name' => $resident->getLastName(),
                    'sex' => $resident->getSex(),
                    'institute_name' => $resident->getInstituteName(),
                    'phone_number' => $resident->getPhoneNumber(),
                    'course' => $resident->getCourse(),
                    'application_document' => $resident->getApplicationDocument(),
                );
        }
        $this->residents_count = count($this->residents);
        $this->places_available_count = $this->places_count - $this->places_reserved_count - $this->residents_count;
    }

    /**
     * @return Result
     */
    public function getResidents() {
        return new Result(true, '', $this->residents);
    }


    /**
     * @return Result
     */
    public function getInfo() {
        return new Result(true, '', array(
            'id' => $this->id,
            'block_id' => $this->block_id,
            'room_area' => $this->room_area,
            'function' => $this->function,
            'status_id' => $this->status_id,
            'places_count' => $this->places_count,
            'places_reserved_count' => $this->places_reserved_count,
            'places_available_count' => $this->places_available_count,
            'residents_count' => $this->residents_count,
            'residents' => $this->residents,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ));
    }

    /**
     * @return Result
     * @param array
     */
    public function setInfo( $param ) {
        foreach ( $param as $key => $value ){
            if( in_array($key, self::$available ))
                is_numeric($value) ? (fmod((float)$value, 1) == 0 ? $this->$key = (int)$value : $this->$key = (float)$value) : $this->$key = $value;
        }
        return $this->update();
    }

    /**
     * @return Result
     * @throws DBQueryException
     */
    private function update() {
        $q_set = 'UPDATE dorm_rooms SET
            block_id = :block_id, room_area = :room_area,
            function = :function, places_count = :places_count,
            places_reserved_count = :places_reserved_count, status = :status
            WHERE id = :id';
        $p_set = $this->db->prepare($q_set);

        $query_param = array();

        foreach (self::$available as $key){
            $query_param[':' . $key] = $this->$key;
        }
        $query_param[':id'] = $this->id;
        $p_set->execute($query_param);
        if( $p_set === FALSE ) throw new DBQueryException('UPDATE_ERROR', $this->db, 'Извините, произошла ошибка');
        return new Result(true);
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     */
    public static function search( $db, $request ){
        $q_get = 'SELECT * FROM dorm_rooms';
        $first = TRUE;
        $search = array();
        foreach ($request as $param => $value)
            if (in_array($param, self::$available)) {
                if($first) {
                    $q_get .= " WHERE {$param} = :{$param}";
                    $first = FALSE;
                }
                else
                    $q_get .= " AND {$param} = :{$param}";
                $search[':' . $param] = $value;
            }
        $p_get = $db->prepare($q_get);
        $p_get->execute($search);
        if( $p_get === FALSE )
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else{
            $result = $p_get->fetchAll();
            if( count( $result ) != 0 ){
                foreach( $result as &$row )
                    foreach( $row as &$value )
                        if( is_numeric($value) )
                            fmod((float)$value, 1) == 0 ? $value = (int)$value : $value = (float)$value;
                return new Result( 'true', '', $result );
            }
            else
                return new Result(false, 'Не найдено');
        }
    }

    /**
     * @return bool
     */
    public function isAvailable(){
        if( $this->places_available_count > 0)
            return true;
        else
            return false;
    }

    /**
     * @return Result
     * @param PDO $db
     * @param array $request
     * @throws DBQueryException
     */
    public static function addRoom($db, $request){
        $q_set_start = 'INSERT INTO dorm_rooms (';
        $q_set_columns = 'block_id, places_count, created_at';
        $q_set_middle = ')VALUES(';
        $q_set_values = ':block_id, :places_count, NOW()';
        $q_set_end = ')';
        $query_param = array();

        $required_count = 0;
        foreach ($request as $param => $value) {
            if (in_array($param, self::$available) and !in_array($param, self::$required)) {
                $q_set_columns .= ', ' . $param;
                $q_set_values .= ', :' . $param;
                $query_param[':' . $param] = $value;
            }
            elseif (in_array($param, self::$required )) {
                $query_param[':' . $param] = $value;
                $required_count += 1;
            }
            //elseif (!in_array($param, self::$required))
            //  throw new InvalidArgumentException;
            //из-за _url нельзя вбросить исключение
        }
        if ($required_count != count(self::$required) ) throw new InvalidArgumentException;
        $q_set = $q_set_start . $q_set_columns . $q_set_middle . $q_set_values . $q_set_end;
        $p_set = $db->prepare($q_set);
        $p_set->execute($query_param);
        if( $p_set === FALSE )
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else
            return new Result(true);
    }

    /**
     * @param $db
     * @return Result
     * @throws DBQueryException
     */
    public static function getStatuses($db) {
        $q_get = 'SELECT id, room_status FROM dorm_rooms_statuses';
        $p_get = $db->prepare($q_get);
        $p_get->execute();
        if ($p_get === FALSE)
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else{
            $result = $p_get->fetchAll();
            if( count( $result ) != 0)
                foreach( $result as &$row )
                    foreach( $row as &$value )
                        if(is_numeric($value))
                            $value = (int)$value;
            return new Result(true, "", $result);
        }
    }

    /**
     * @return mixed
     */
    public function getPlacesAvailable() {
        return $this->places_available_count;
    }

    /**
     * @return int
     */
    public function getResidentsCount() {
        return $this->residents_count;
    }

    /**
    * @return mixed
    */
    public function getId() {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBlockId() {
        return $this->block_id;
    }

    /**
     * @return mixed
     */
    public function getRoomArea() {
        return $this->room_area;
    }

    /**
     * @return mixed
     */
    public function getFunction() {
        return $this->function;
    }

    /**
     * @return mixed
     */
    public function getPlacesCount() {
        return $this->places_count;
    }

    /**
     * @return mixed
     */
    public function getPlacesReservedCount() {
        return $this->places_reserved_count;
    }

    /**
     * @return mixed
     */
    public function getStatusId() {
        return $this->status_id;
    }
} 