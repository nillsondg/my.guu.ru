<?php

class Classroom {

    const MODULE_NAME = 'classrooms';
    private $db;
    private $id;
    private $building_id;
    private $room;
    private $room_type;
    private $room_area;
    private $fact_room_type;
    private $allow_student_count;

    public function __construct($id, PDO $db) {
        $q_get_classroom = 'SELECT classrooms.building_id,
			classrooms.room, classrooms.room_type, classrooms.room_area,
			classrooms.fact_room_type, classrooms.allow_student_count
			FROM classrooms
			WHERE id = :id';
        $p_get = $db->prepare($q_get_classroom);
        $p_get->execute(array( ':id' => $id ));
        if ($p_get === FALSE) throw new DBQueryException('', $db);
        $res = $p_get->fetch();
        if( !$res ) throw new InvalidArgumentException;
        $this->id = $id;
        $this->db = $db;
        $this->building_id = $res['building_id'];
        $this->room = $res['room'];
        $this->room_type = $res['room_type'];
        $this->room_area = $res['room_area'];
        $this->fact_room_type = $res['fact_room_type'];
        $this->allow_student_count = $res['allow_student_count'];
    }

    public function getId() {
        return $this->id;
    }
    public function getInfo() {
        return new Result(true, '', array(
            'classroom_id' => $this->id,
            'building_id' => $this->building_id,
            'room' => $this->room,
            'room_type' => $this->room_type,
            'room_area' => $this->room_area,
            'fact_room_type' => $this->fact_room_type,
            'allow_student_count' => $this->allow_student_count,
        ));
    }
    public function setAll( $param ) {
        $possible = array(
            'classroom_id' => &$this->id,
            'building_id' => &$this->building_id,
            'room' => &$this->room,
            'room_type' => &$this->room_type,
            'room_area' => &$this->room_area,
            'fact_room_type' => &$this->fact_room_type,
            'allow_student_count' => &$this->allow_student_count,
        );
        foreach ( $param as $key => $value ){
            if( array_key_exists($key, $possible ))
                $possible[$key] = $value;
        }
        $this->update();
    }
    public function getBuildingId() {
        return $this->building_id;
    }
    public function getRoom() {
        return $this->room;
    }
    public function getRoomType() {
        return $this->room_type;
    }
    public function getRoomArea() {
        return $this->room_area;
    }
    public function setRoomArea($building_id) {
        $this->building_id = $building_id;
        $this->update();
    }
    public function getFactRoomType() {
        return $this->fact_room_type;
    }
    public function setFactRoomType($fact_room_type) {
        $this->fact_room_type = $fact_room_type;
        $this->update();
    }
    public function getAllowStudentCount() {
        return  $this->allow_student_count;
    }

    private function update() {
        $q_set = 'UPDATE classrooms SET
            building_id = :building_id, room = :room,
            room_type = :room_type, room_area = :room_area,
            fact_room_type = :fact_room_type, allow_student_count = :allow_student_count
            WHERE id = :id';
        $p_set = $this->db->prepare($q_set);
        $p_set->execute(array(
            ':building_id' => $this->building_id,
            ':room' => $this->room,
            ':room_type' => $this->room_type,
            ':room_area' => $this->room_area,
            ':fact_room_type' => $this->fact_room_type,
            ':allow_student_count' => $this->allow_student_count,
            ':id' => $this->id,
        ));
        if( $p_set === FALSE ) throw new DBQueryException('UPDATE_ERROR', $this->db, 'Извините, произошла ошибка');
    }

    public static function search( $db, $request ) {
        $q_get = 'SELECT * FROM classrooms';
        $search = array();
        $possible = array( 'building_id', 'room', 'room_type', 'room_area', 'fact_room_type', 'allow_student_count' );
        $first = TRUE;
        foreach ($request as $param => $value)
            if (in_array($param, $possible)) {
                if($first) {
                    $q_get .= " WHERE {$param} = :{$param}";
                    $first = FALSE;
                }
                else
                    $q_get .= " AND {$param} = :{$param}";
                $search[':' . $param] = $value;
            }
        $p_get = $db->prepare($q_get);
        $p_get->execute($search);
        if( $p_get === FALSE )
            throw new DBQueryException('GET_ERROR', $db, 'Извините, произошла ошибка');
        else
            return new Result(true, '', $p_get->fetchAll());
    }
}