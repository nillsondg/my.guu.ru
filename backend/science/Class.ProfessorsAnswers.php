<?php

require_once 'Class.QuestionsList.php';
require_once $ROOT_PATH . '/backend/core/kaf/Class.AcademicDepartment.php';

class ProfessorsAnswers{

	private $professor;
	private $db;
	private $batch_data;

	private $staff_id;

	private static $points_tree;


	public function __construct(Professor $professor = null, PDO $db){
		$this->professor = $professor;
		$this->db = $db;
		$this->batch_data = array();
	}

	public function getRatingForStaffId($staff_id = null, $period_id = null) {
		$this->setStaffId($staff_id);
		$period_parts = $this->buildPeriodPartOfQuery($period_id);

		$q_get_rating = 'SELECT DISTINCT id as value_id,
			value, updated_at,
 			created_at, period_id, indicator_id
			FROM science_indicators_values
			WHERE staff_id = :staff_id
			AND period_id = ' . $period_parts['query_part'];
		$p_get_rating = $this->db->prepare($q_get_rating);
		$params = $period_parts['params'];
		$params[':staff_id'] = $this->staff_id;
		$p_get_rating->execute($params);

		if ($p_get_rating === FALSE) throw new DBQueryException('', $this->db);
		if ($p_get_rating->rowCount() == 0) return array();

		return new Result(true, '', $p_get_rating->fetchAll());
	}

	public function setStaffId($staff_id){
		if ($this->professor == null && !is_numeric($staff_id)) throw new LogicException('Professor not exists');
		$this->staff_id = $staff_id;
	}

	public function commitBatch(){
		$p_ins_items = $this->db->prepare('INSERT INTO science_answers(staff_id, mask_question_id, value_numeric, value_boolean, value_item_id, created_at)
		VALUES (:staff_id, :mask_question_id, :numeric, :boolean, :item_id, NOW())
		ON DUPLICATE KEY UPDATE
		value_numeric = :numeric,
		value_boolean = :boolean,
		value_item_id = :item_id
		');

		foreach($this->batch_data as $query_params){
			$p_ins_items->execute($query_params);
			if ($p_ins_items === FALSE) throw new DBQueryException('INS_ANSWER_ERROR', $this->db);
		}
	}

	private function calculate(array &$elements, &$parent){
		foreach($elements as &$element){
			if (isset($element['items'])){
				$element['sum'] = 0;
				self::calculate($element['items'], $element);
			}else{
				if ($parent == '') continue;
				$parent['sum'] += $element['points'];//self::sumItems($parent['items']);
				$parent['max_points'] = isset($parent['max_points']) ? $parent['max_points'] : PHP_INT_MAX;
				$parent['points'] = ($parent['sum'] > $parent['max_points'] ? $parent['max_points'] : $parent['sum']);
				//unset($parent['items']);
			}
		}
	}

	private function finalCalculate($sections){
		$indicators = $this->db->query('SELECT * FROM science_indicators')->fetchAll();
		$p_ins_result = $this->db->prepare('INSERT INTO science_indicators_values(staff_id, indicator_id, period_id, value, created_at)
			SELECT
				:staff_id as staff_id,
				:indicator_id as indicator_id,
				id as period_id,
				:value as value,
				NOW() as created_at
				FROM science_periods
				WHERE start_date <= NOW() AND finish_date > NOW()
				ON DUPLICATE KEY UPDATE
				value = :value');

		foreach ($indicators as $key => $indicator){
			$formula = $indicator['formula'];
			foreach($sections as $section){
				$formula = preg_replace('/{section_' . $section['section_id'] .'}/i', $section['points'], $formula);
			}
			if (!MathExpression::validate($formula)) throw new LogicException('Wrong formula!');
			//echo $formula;
			$indicators[$key]['result'] = eval('return ('. $formula .');');
			if ($this->staff_id == null && $this->professor) throw new LogicException('PROFESSOR_NOT_SPECIFIED');
			$p_ins_result->execute(array(
				':staff_id' => $this->staff_id ? : $this->professor->getStaffId(),
				':indicator_id' => $indicators[$key]['id'],
				':value' => $indicators[$key]['result']
			));
			if ($p_ins_result === FALSE) throw new DBQueryException('SCIENCE_SAVE_ERROR', $this->db);
		}
		return new Result(true, '', $indicators);
	}

	private static function findOptionValue($item_id, $options){
		foreach($options as $option){
			if ($option['mask_question_id'] == $item_id){
				return floatval($option['point_per_unit']);
			}
		}
	}

	private static function buildTree(array $elements, $parent_id = 0){
		$branch = array();
		foreach ($elements as $element) {
			if ($element['parent_id'] == $parent_id) {
				$children = self::buildTree($elements, $element['question_id']);
				if ($children) {
					$children_name = ($children[0]['type_id'] == '5') ? 'options' : 'items';
					if ($children_name == 'options'){
						if (isset($element['value_item_id'])){
							$element['points'] = self::findOptionValue($element['value_item_id'], $children);
						}else{
							$element['points'] = 0;
						}
					}else{
						$element[$children_name] = $children;
					}
				}
				if (isset($element['value_boolean']) || isset($element['value_numeric'])){
					$value = $element['value_boolean'] !== null ? $element['value_boolean'] : ($element['value_numeric'] !== null ? $element['value_numeric'] : null);
					$element['points'] = $element['point_per_unit'] * $value;
				}

				if (isset($element['points']) && $element['points'] > $element['max_points'] && $element['max_points'] != null){
					$element['points'] = $element['max_points'];
				}elseif(!isset($element['points'])){
					$element['points'] = 0;
				}
				$branch[] = $element;
			}
		}
		return $branch;
	}

	public function calculatePoints($staff_id = null, $period_id = null){
		if ($this->professor == null && !is_numeric($staff_id)) throw new LogicException('Professor not exists');

		$this->setStaffId($staff_id);

		$questions = QuestionsList::getFullList($this->db, null, false)->getData();
		$answers = $this->getFullList($staff_id, $period_id)->getData();

		$questions_array = array();
		$answers_array = array();
		$merged_array = array();


		foreach($questions as $question) {
			$questions_array["{$question['mask_question_id']}"] = $question;
		}

		foreach($answers as $answer) {
			$answers_array["{$answer['mask_question_id']}"] = $answer;
		}

		foreach($questions_array as $id => $question){
			if(isset($answers_array[$id])){
				$merged_array[$id] = array_merge($answers_array[$id], $question);
			}else{
				$merged_array[$id] = $question;
			}
		}

		self::$points_tree = self::buildTree($merged_array, null);
		$parent = '';
		self::calculate(self::$points_tree, $parent);
		$array_by_sections = array();
		$sections = QuestionsList::getSections($this->db)->getData();

		foreach($sections as $section){
			$array_by_sections[$section['section_id']] = $section;
			$array_by_sections[$section['section_id']]['points'] = 0;
			$array_by_sections[$section['section_id']]['items'] = array();
		}

		foreach(self::$points_tree as $item){
			$section_id = $item['section_id'];
			$item['points'] = isset($item['points']) ? $item['points'] : 0;
			if (isset($array_by_sections[$section_id])){
				$array_by_sections[$section_id]['points'] += $item['points'];
				$array_by_sections[$section_id]['items'][] = $item;
			}
		}

		foreach($array_by_sections as $section_id => $value){
			if ($value['points'] > $sections[$section_id]['section_max_points']){
				$array_by_sections[$section_id]['points'] = $sections[$section_id]['section_max_points'];
			}
		}
		return new Result(true, 'Данные успешно получены', array(
			'final' => $this->finalCalculate($array_by_sections)->getData(),
			'tree' => $array_by_sections,
			'verification' => $this->getVerificationInfo($period_id)
		));
	}

	private function getCanVerify(Staff $staff){
		$job_info = $staff->getJobInfo()->getData();
		$is_head = $staff->hasRights(array(
			'module' => 'academic_department',
			'min_level' => AcademicDepartment::HEAD_LEVEL_IN_MODULE
		));
		$same_dep = false;
		$q_get_can_verify = 'SELECT roles.ou_id
			FROM users_roles
			INNER JOIN roles ON roles.id = users_roles.role_id
			WHERE users_roles.user_id =
				(SELECT staff.user_id FROM staff WHERE staff.id = :staff_id)
				AND users_roles.active = 1';
		$p_can_verify = $this->db->prepare($q_get_can_verify);
		$p_can_verify->execute(array(
			':staff_id' => $this->staff_id
		));
		if ($p_can_verify === FALSE) throw new DBQueryException('CANT_GET_OU', $this->db);
		$res = $p_can_verify->fetchAll();
		foreach($res as $ou){
			if ($ou['ou_id'] == $job_info['ou_id']){
				$same_dep = true;
			}
		}
		return $same_dep && $is_head;
	}

	private function getVerificationInfo($period_id = null){
		$parts = $this->buildPeriodPartOfQuery($period_id);

		$q_get_verified = 'SELECT science_verifications.verified, science_verifications.verified_by_staff_id FROM science_verifications
			WHERE staff_id = :staff_id
		  	AND period_id = ' . $parts['query_part'];
		$p_get_verified = $this->db->prepare($q_get_verified);
		$p_get_verified->execute($parts['params']);

		$result_array = array(
			'verified' => false,
			'can_verify' => false
		);

		if ($p_get_verified === FALSE) throw new DBQueryException('', $this->db);

		if ($p_get_verified->rowCount() == 1){
			$res = $p_get_verified->fetch();
			$result_array['verified'] = $res['verified'] == 1;
		}
		global $__user;
		$result_array['can_verify'] = $this->getCanVerify($__user->getStaffInstance());
		return $result_array;
	}

	private function getPeriodId($period_id = null){
		$args = array();
		if (is_numeric($period_id) && $period_id != 0){
			$q_check_period = 'SELECT science_periods.id FROM science_periods WHERE id = :id';
			$args = array(':id' => $period_id);
		}else{
			$q_check_period = 'SELECT id FROM science_periods WHERE NOW() BETWEEN start_date AND finish_date';
		}
		$p_get_per = $this->db->prepare($q_check_period);
		$p_get_per->execute($args);
		if ($p_get_per === FALSE) throw new DBQueryException('CANT_FIND_PERIOD', $this->db);
		if ($p_get_per->rowCount() != 1) throw new InvalidArgumentException('BAD_PERIOD_ID');
		$res = $p_get_per->fetch();
		return $res['id'];
	}

	public function setVerified($staff_id, $period_id, $value, Staff $staff){

		$this->setStaffId($staff_id);

		if (!$this->getCanVerify($staff)) throw new LogicException('DONT_HAVE_PERMISSIONS');
		$q_upd_verified = 'INSERT INTO science_verifications(verified, staff_id, period_id, verified_by_staff_id, created_at)
			VALUES(:verified, :staff_id, :period_id, :verified_by_staff_id, NOW())
			ON DUPLICATE KEY UPDATE verified = :verified, verified_by_staff_id = :verified_by_staff_id';
		$p_upd_verified = $this->db->prepare($q_upd_verified);
		$p_upd_verified->execute(array(
			':verified' => ($value == 1 || $value == true) ? 1 : 0,
			':staff_id' => $this->staff_id,
			':period_id' => $this->getPeriodId($period_id),
			':verified_by_staff_id' => $staff->getStaffId()
		));
		if ($p_upd_verified === FALSE) throw new DBQueryException('VERIFICATION_UPDATE_ERROR', $this->db);
		return new Result(true, '');
	}

	public function addValueToBatch($mask_id, array $values){
		$this->batch_data[] = array_merge($values, array(
			':mask_question_id' => $mask_id,
			':staff_id' => $this->professor->getStaffId()
		));
	}

	private function buildPeriodPartOfQuery($period_id = null){
		$period_arr = array(
			':staff_id' => $this->staff_id
		);
		if ($period_id == null || $period_id == 'current' || $period_id == 0){
			$period_var = '(SELECT id FROM science_periods WHERE NOW() BETWEEN start_date AND finish_date)';
		}else{
			$period_var = ':period_id';
			$period_arr[':period_id'] = $period_id;
		}
		return array(
			'query_part' => $period_var,
			'params' => $period_arr
		);
	}

	public function getFullList($staff_id = null, $period_id = null){
		$staff_id = $staff_id ? $staff_id : ($this->professor instanceof Professor ? $this->professor->getStaffInstance()->getStaffId() : null);

		$this->setStaffId($staff_id);
		$parts = $this->buildPeriodPartOfQuery($period_id);

		$p_get_answers = $this->db->prepare('SELECT science_answers.*, science_answers.value_item_id as vii, science_period_mask.question_id,
			(SELECT sq.name
				FROM science_questions sq
				INNER JOIN science_period_mask spm ON spm.question_id = sq.id
				WHERE spm.id = vii) as value_item_name
			FROM science_answers
			 INNER JOIN science_period_mask ON science_period_mask.id = science_answers.mask_question_id
			WHERE staff_id = :staff_id
			AND science_period_mask.period_id = ' . $parts['query_part'] . ' ORDER BY id ');
		$p_get_answers->execute($parts['params']);

		return new Result(true, 'Данные успешно получены', $p_get_answers->fetchAll());


	}

	public function save(array $data){
		if ($this->professor == null) throw new LogicException('Professor not exists');
		$questions = QuestionsList::getFullList($this->db, null, false)->getData();
		$norm_questions = array();

		foreach($questions as $question){
			$norm_questions[$question['id']] = $question;
		}
		foreach($data as $mask_id => $value){
			$mask_id = str_replace('item-', '', $mask_id);
			//check value and question_type

			if (isset($norm_questions[$mask_id])){
				switch ($norm_questions[$mask_id]['type_name']){
					case 'numeric': {
						if (is_numeric($value)){
							$this->addValueToBatch($mask_id, array(
								':numeric' => $value,
								':boolean' => null,
								':item_id' => null,
							));
						}
						break;
					}
					case 'select': {
						if (is_numeric($value) && isset($norm_questions[$value]) && $norm_questions[$value]['type_name'] == 'option'){
							$this->addValueToBatch($mask_id,  array(
								':numeric' => null,
								':boolean' => null,
								':item_id' => $value,
							));
						}
						break;
					}
					case 'checkbox': {
						$bool_val = ($value == '1' || $value == 'true') ? 1 : 0;
						$this->addValueToBatch($mask_id, array(
							':numeric' => null,
							':boolean' => $bool_val,
							':item_id' => null
						));
						break;
					}
					case 'option': case 'group': {break;}
				}
			}
		}
		$this->commitBatch();
		return $this->calculatePoints();
	}
}



class MathExpression {

	private static $parentheses_open = array('(', '{', '[');
	private static $parentheses_close = array(')', '}', ']');

	protected static function getParenthesesType( $c ) {
		if(in_array($c,MathExpression::$parentheses_open)) {
			return array_search($c, MathExpression::$parentheses_open);
		} elseif(in_array($c,MathExpression::$parentheses_close)) {
			return array_search($c, MathExpression::$parentheses_close);
		} else {
			return false;
		}
	}

	public static function validate( $expression ) {
		$size = strlen( $expression );
		$tmp = array();
		for ($i=0; $i<$size; $i++) {
			if(in_array($expression[$i],MathExpression::$parentheses_open)) {
				$tmp[] = $expression[$i];
			} elseif(in_array($expression[$i],MathExpression::$parentheses_close)) {
				if (count($tmp) == 0 ) {
					return false;
				}
				if(MathExpression::getParenthesesType(array_pop($tmp))
					!= MathExpression::getParenthesesType($expression[$i])) {
					return false;
				}
			}
		}
		if (count($tmp) == 0 ) {
			return true;
		} else {
			return false;
		}
	}
}