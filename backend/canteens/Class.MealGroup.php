<?php
/**
 * Created by PhpStorm.
 * User: Инал
 * Date: 12.11.2014
 * Time: 21:59
 */


class MealGroup{

	private $group_id;
	private $name;
	private $status;
	private $db;

	public function __construct($group_id, PDO $db){
		$q_get_meal_group = '
			SELECT canteens_meal_groups.* FROM canteens_meal_groups
			 WHERE canteens_meal_groups.id = :meal_group_id
			 AND status = 1';

		$p_group = $db->prepare($q_get_meal_group);
		$p_group->execute(array(
			':meal_group_id' => $group_id
		));
		if ($p_group === FALSE) throw new DBQueryException('', $db);
		if ($p_group->rowCount() == 0) throw new LogicException('');

		$result = $p_group->fetch();

		$this->group_id = $result['meal_group_id'];
		$this->name = $result['meal_group_name'];
		$this->status = $result['status'];

		$this->db = $db;
	}

	public function getFullMenu(Canteen $canteen){
		$q_meals = 'SELECT
			canteens.canteen_name,
			canteens_meal_groups.name as meal_group_name,
			canteens_meals.name as meal_name,
			canteens_exist_meals.meal_group_id,
			canteens_exist_meals.meal_id,
			canteens_exist_meals.canteen_id,
			canteens_meals_prices.price
		FROM canteens_exist_meals
		INNER JOIN canteens ON canteens.id = canteens_exist_meals.canteen_id
		INNER JOIN canteens_meal_groups ON canteens_meal_groups.id = canteens_exist_meals.meal_group_id
		INNER JOIN canteens_meals ON canteens_meals.id = canteens_exist_meals.meal_id
		INNER JOIN canteens_meals_prices ON canteens_meals_prices.canteen_exist_id = canteens_exist_meals.id
		WHERE canteens_meals_prices.status = 1
		AND canteens.id = :canteen_id
		AND canteens_meal_groups.id = :group_id';

		$p_meals = $this->db->prepare($q_meals);

		$p_meals->execute(array(
			':canteen_id' => $canteen->getId(),
			':group_id' => $this->group_id
		));

		if ($p_meals === FALSE) throw new DBQueryException('', $this->db);

		return new Result(true, '', array(
			'meal_group' => array(
				'meal_group_id' => $this->group_id,
				'name' => $this->name
			),
			'meals' => $p_meals->fetchAll()));
	}


}