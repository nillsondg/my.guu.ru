<?php



class RequisitionsToInstituteCollection extends AbstractRequisitionsCollection{

	private $db;
	private $items;
	private $query;


	//TODO: add personal_file field
	public function __construct($type = 'my', User $__user, PDO $__db){
		if ($type == 'my'){
			$this->query = 'SELECT institute_requisitions.*, requisition_statuses.*, institute_requisitions.from_user_id as fuid,
				users.first_name, users.last_name, users.middle_name
				FROM institute_requisitions
				INNER JOIN requisition_statuses ON requisition_statuses.status_id = institute_requisitions.requisition_status_id
				INNER JOIN users ON users.id = institute_requisitions.from_user_id
				WHERE institute_requisitions.from_user_id = :id';

			$this->args = array(
				':id' => $__user->getId()
			);
		}elseif ($type == 'all'){
			if (!$__user->hasRights(array('module' => 'institute', 'min_level' => 6))){
				throw new PrivilegesException('У вас недостаточно прав для данного действия', $__db);
			}
			$this->query = 'SELECT institute_requisitions.*, requisition_statuses.*, institute_requisitions.from_user_id as fuid,
				users.first_name, users.last_name, users.middle_name
				FROM institute_requisitions
				INNER JOIN requisition_statuses ON requisition_statuses.status_id = institute_requisitions.requisition_status_id
				INNER JOIN users ON users.id = institute_requisitions.from_user_id
				WHERE institute_requisitions.institute_id = :institute_id';
			$this->args = array(
				':institute_id' => $__user->getInstituteId()
			);
		}
		$this->db = $__db;

	}


	public function get(){
		$p_get = $this->db->prepare($this->query);
		$p_get->execute($this->args);
		$items = $p_get->fetchAll();
		return new Result(true, '', $items);
	}
}