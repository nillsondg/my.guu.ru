<?php

class RequisitionToInstitute extends AbstractRequisition{

	private $requisition_id;
	private $user;
	private $to_organization;
	private $seal_type;
	private $additional_info;
	private $count;
	private $create_date;
	private $from_user_id;
	private $changer_id;
	private $change_date;
	private $status_id;
	private $status_text;
	private $db;

	public function __construct($id, User $__user, PDO $db){

		$this->user = $__user;
		if ($__user->isStaff()){
			$user = $__user->getStaffInstance();
		}elseif($__user->isStudent()){
			$user = $__user->getStudentInstance();
		}else{
			throw new InvalidArgumentException('');
		}

		$q_get_req = 'SELECT institute_requisitions.*, requisition_statuses.*
			FROM institute_requisitions
			INNER JOIN requisition_statuses ON requisition_statuses.status_id = institute_requisitions.requisition_status_id
			WHERE institute_requisitions.requisition_id = :requisition_id';

		$p_req = $db->prepare($q_get_req);
		$p_req->execute(array(
			':requisition_id' => $id
		));
		if (!$p_req) throw new DBQueryException('', $db);
		if ($p_req->rowCount() == 0) throw new InvalidArgumentException('', $db);

		$row = $p_req->fetch();


		if ($row['from_user_id'] == $__user->getId()
			|| ($__user->hasRights(array('module' => 'institute', 'min_level' => 6))
				&& $user->getInstituteId() == $row['institute_id'])){

			$this->requisition_id = $id;
			$this->additional_info = $row['additional_info'];
			$this->change_date = $row['change_date'];
			$this->changer_id = $row['changer_id'];
			$this->create_date = $row['create_date'];
			$this->seal_type = $row['seal_type'];
			$this->to_organization = $row['to_organization'];
			$this->count = $row['count'];
			$this->status_id = $row['status_id'];
			$this->status_text = $row['status_text'];
			$this->from_user_id = $row['from_user_id'];
			$this->db = $db;


			return new Result(true, 'Данные успешно получены', $row);
		}else{
			throw new InvalidArgumentException('У вас недостаточно прав');
		}
	}

	public static function create(array $data, User $__user, PDO $db){
		$q_ins_req = 'INSERT INTO institute_requisitions(to_organization, seal_type, additional_info,
			requisition_status_id, count, create_date, from_user_id, changer_id, institute_id)
			VALUES (:to_organization, :seal_type, :additional_info,
				1, :count, NOW(), :from_user_id, :changer_id, :institute_id)';
		$p_ins = $db->prepare($q_ins_req);
		$p_ins->execute(array(
			':to_organization' => $data['to_organization'],
			':seal_type' => $data['seal_type'],
			':additional_info' => $data['additional_info'],
			':count' => $data['count'],
			':from_user_id' => $__user->getId(),
			':changer_id' => $__user->getId(),
			':institute_id' => $__user->getInstituteId()
		));
		if (!$p_ins) throw new DBQueryException('', $db);

		return new Result(true, 'Данные успешно добавлены', array(
			'requisition_id' => $db->lastInsertId()
		));
	}

	public function getFullInfo(){
		return new Result(true, 'Данные успешно получены', array(
			'requisition_id' => $this->requisition_id,
			'to_organization' => $this->to_organization,
			'change_date' => $this->change_date,
			'seal_type' => $this->seal_type,
			'additional_info' => $this->additional_info,
			'requisition_status_id' => $this->status_id,
			'status_text' => $this->status_text,
			'count' => $this->count,
		));
	}

	public function update(){

	}

	public function setStatus($status_id){
		$q_upd_status = 'UPDATE institute_requisitions SET requisition_status_id = :status_id,
			change_date = NOW(),
			changer_id = :id
			WHERE requisition_id = :requisition_id';
		$p_upd_req = $this->db->prepare($q_upd_status);
		$p_upd_req->execute(array(
			':status_id' => $status_id,
			':id' => $this->user->getId(),
			':requisition_id' => $this->requisition_id
		));
		if (!$p_upd_req) throw new DBQueryException('', $this->db);
		return new Result(true, 'Данные успешно обновлены');
	}
}