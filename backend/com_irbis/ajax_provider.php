<?php

require_once('jirbis_link_libraries.php');

header('Content-Type: text/html; charset=utf-8');

if (DEBUG)
	ji_log::$debug_enable = true;
if (DEBUG)
	ilog::$debug_enable = true;


//\-------------------ИНИЦИАЛИЗАЦИЯ ПОДКЛЮЧЕНИЙ К ЛОКАЛЬНЫМ БД------------------------------

$task = '';

if (DEBUG_SINGLE)
	$task = 'search_single';
if (DEBUG_BROAD)
	$task = 'search_broadcast';
if (DEBUG_REFORMAT)
	$task = 'single_reformat';
if (DEBUG_FREE)
	$task = 'free_process';
if (DEBUG_TERMS)
	$task = 'get_terms';
if (DEBUG_RESULTS)
	$task = 'show_results';
if (DEBUG_BO_GRID) {
	$g = new ji_bo_grid();
	$g->bo_list();
}


//$task='show_search_form';

$task = mosGetParam($_REQUEST, 'task', $task);

call_user_func(array('ic', $task));


?>





<?php

//=========================================================================================
//=============================IC БЛОК ОСНОВНЫХ ФУНКЦИЙ====================================
//=========================================================================================

class ic {
	public static function search_broadcast() {
		global $CFG, $__db, $max_portion_profiles;

		//ignore_user_abort(true);		
		ji_log::i()->replace_debugs();
		/* @var $ic broad_search_unit */


		ji_log::i()->w("BEGIN", I_INFO);
		$st = new ji_st();
		$st->set_selected_user_profile();

		//if ($st->req_source!=='old_session'){
		$form = new ji_search_form();
		$form->request_array = $_REQUEST;
		$form->logic_form = $st->logic_form;
		$form->fp = $st->fp;

		$st->req = $form->build_request();
		//}

		$ic = new broad_search_unit($st->req, $st->bl_id_array, $st->bl_id_array_selected, $st->req_id_client);
		//
		ji_log::i()->w("Сгенерирован запрос: $st->req к источникам {$st->bl_id_string} ( выбраны {$st->bl_id_string_selected}). Вывод с $st->first_number ", I_INFO, $st->req_id_client);
		//echo $req;
		$ic->profile = $st->profile;
		//$ic->max_portion_profiles=$max_portion_profiles;
		$ic->src = $st->src;


		$ic->max_process_count = $CFG['max_process_count'];
		$ic->max_process_live_time = $CFG['max_process_live_time'];
		$ic->optimal_records_for_process = $CFG['optimal_records_for_process'];
		$ic->cache_relevance_time = $CFG['cache_relevance_time'];
		$ic->bad_connection_pause = $CFG['bad_connection_pause'];
		$ic->self_net_path = JI_FILE_PATH_AUTO_REQUESTS_NET_LOCALHOST_PROVIDER;
		$ic->mysql_single_wait_time = $CFG['mysql_single_wait_time'];
		$ic->reformat_enable = $st->reformat_enable;
		$ic->max_portion_profiles = $st->max_portion_profiles;
		// Заменить проверку первого номера на флаг нового запроса
		if ($st->first_number == 1)
			$ic->build_req_id();
		// Выполняем построение похожего кэша только для первого запроса.
		if ($st->permanent_output && $st->first_number == 1 && !$ic->find_relevant_cache())
			$ic->build_similar_cache();


		$sources = $ic->build_connections_list();
		/*if (!$sources){				
			ji_log::i()->w("Отсутствуют доступные источники для поиска записей. Вероятно, работает временная блокировка недоступных баз.",I_ERROR,'',3);
		}		*/
		$ic->calculate_cache($sources);

		ji_log::i()->w("Найдено в кэше last_number_assimilated: {$ic->last_number_assimilated}, last_number: {$ic->last_number}, total: " . array_sum($ic->total), I_INFO, $st->req_id_client);
		$st->move_first_number($ic->cache_count_relevant, $ic->recs_total);

		// Если стоит флаг полного сканирования всех источников и при этом общее количество записей меньше максимально допустимого для сортировки
		if ($ic->all_sources_requested_flag && $ic->recs_total < $CFG['max_sorted_records'] && $st->sort && $ic->recs_total)
			$st->portion = $ic->recs_total;
		else {
			$st->sort = '';
			$st->sort_name = '';
		}

		$ic->last_number = $st->portion + $st->first_number_normalized;
		$ic->first_number = $st->first_number_normalized;
		$ic->portion = $st->portion;
		//------------Хорошо бы вместо этих сложных условий просто реализовать защиту от пустого параметра 
		//

		// Раньше здесь было :array_sum($ic->total), но зачем нам использовать общее количество записей?
		// Если НЕ отсканированы не все источники 
		//или общий объём релевантного кэша меньше последнего запрашиваемого номера и при этом потенциально доступное количество записей больше начального номера
		if (!$ic->all_sources_requested_flag || ($ic->cache_count_relevant < $ic->last_number)) {
			$sources = $ic->build_useful_connections($sources);

			$sources = $ic->cut_portions($sources);


			$ic->make_search_session($sources);
			$st->write_close();
			$ic->start_search_process(array(&$ic, 'callback_search'), $sources);
		}
		else {

			$ic->make_search_session(array());
			$st->write_close();

		}

		//Что ещё за переформатирование???		
		if ($st->reformat_enable) {
			$recs = $ic->get_reformated_records();
			$ic->start_reformat_process(array(&$ic, 'callback_reformat'), $recs);
		}


		// SESSION DATA	
		$sd = ji_search_session::read($st->req_id_client);


		if ($sd['reqs'] < $sd['reqs_need'])
			ji_log::i()->w("Количество выполненных запросов({$sd['reqs']}) меньше количества требуемых запросов({$sd['reqs_need']}). Есть некорректно завершенные процессы.", I_ERROR, $ic->req_id_client, 2);

		if ($sd['reformating_portions'] < $sd['reformating_portions_need'])
			ji_log::i()->w("Количество выполненых расформатирований({$sd['reformating_portions']}) меньше количества требуемых({$sd['reformating_portions_need']}). Есть незавершенные процессы расформатирования.", I_ERROR, $ic->req_id_client, 3);

		$sd['finish'] = true;

		ji_log::i()->w("Broad Search завершен", I_INFO, $st->req_id_client);
		$sd['errors'] = errors::add_objects_array($sd['errors']);

		ji_search_session::write($sd);

		if (errors::is_errors()) {
			ji_log::i()->replace_debugs(true);
		}
		answer::broad_result(array('req_id_client' => $st->req_id_client, 'errors' => errors::get_objects_array()));

	}


	public static function search_broadcast_selected() {
		global $CFG, $__db, $max_portion_profiles;
		//ignore_user_abort(true);

		/* @var $ic broad_search_unit */
		ji_log::i()->w("Broad Search selected begin", I_INFO);
		$st = new ji_st();
		$st->set_selected_user_profile();

		$ic = new broad_search_unit('', $st->bl_id_array, $st->bl_id_array_selected, $st->req_id_client);
		//log::finalization();
		ji_log::i()->w("Broad Search Selected  Сгенерирован запрос: к источникам {$st->bl_id_string} ( выбраны {$st->bl_id_string_selected}). Вывод с $st->first_number ", I_INFO, $st->req_id_client);
		//echo $req;
		$ic->profile = $st->profile;
		//$ic->max_portion_profiles=$max_portion_profiles;
		$ic->src = $st->src;
		$ic->portion = $st->portion;

		$ic->max_process_count = $CFG['max_process_count'];
		$ic->max_process_live_time = $CFG['max_process_live_time'];
		$ic->optimal_records_for_process = $CFG['optimal_records_for_process'];
		$ic->cache_relevance_time = $CFG['cache_relevance_time'];
		$ic->bad_connection_pause = $CFG['bad_connection_pause'];
		$ic->self_net_path = JI_FILE_PATH_AUTO_REQUESTS_NET_LOCALHOST_PROVIDER;
		$ic->mysql_single_wait_time = $CFG['mysql_single_wait_time'];
		$ic->reformat_enable = $st->reformat_enable;
		$ic->max_portion_profiles = $st->max_portion_profiles;

		$ic->last_number = $st->portion + $st->first_number;
		$st->first_number_normalized = $st->first_number;
		$ic->first_number = $st->first_number_normalized;
		// Если отсканированы не все источники но или кэш не достаточен
		//$st->write_close();


		$ic->req_id = '';
		$ic->cache_count_relevant = count($st->rec_id_array_selected);
		$ic->cache_count = count($st->rec_id_array_selected);

		$ic->rec_id_array_selected = $st->rec_id_array_selected;
		$ic->rec_id_string_selected = $st->rec_id_string_selected;


		$ic->make_search_session(array());
		$st->write_close();

		if ($st->reformat_enable) {
			$recs = $ic->get_reformated_records($st->rec_id_array_selected);
			$ic->start_reformat_process(array(&$ic, 'callback_reformat'), $recs);
		}


		// SESSION DATA	

		$sd = ji_search_session::read($st->req_id_client);

		if ($sd['reformating_portions'] < $sd['reformating_portions_need'])
			ji_log::i()->w("Количество выполненых расформатирований({$sd['reformating_portions']}) меньше количества требуемых({$sd['reformating_portions_need']}). Есть незавершенные процессы расформатирования.", I_ERROR, $st->req_id_client, 11);
		//ji_log::i()->w("",I_ERROR,'',11);

		$sd['finish'] = true;
		ji_log::i()->w("Broad Search Selected завершен", I_INFO, $st->req_id_client);


		ji_search_session::write($sd);

		answer::broad_result(array('req_id_client' => $st->req_id_client, 'errors' => errors::get_objects_array()));

	}


	public static function search_single() {
		global $CFG, $__db, $single_debug;
		/* @var $rec record */

		ignore_user_abort(true);
		// Используются в Запросе ИРБИС

		$r = mosGetParam($_REQUEST, 'r', serialize($single_debug));

		//file_put_contents('C:\xampp\htdocs\jirbis2\SINGLE.txt',stripslashes($r));
		$r = unserialize(urldecode($r));


		$ss = new single_search_unit($r);
		//$stat=new stat($ss->req_id,'stat_src');
		//$stat->set_bl_id($ss->bl_id);

		// Выполняем запрос ИРБИС
		$recs = $ss->lib_request();

		// В дальнейшем это значение будет перезаписано
		//$stat->set_finish();
		//ji_log::i()->w("TTTTTTTTTTTTTTTTTTT",I_INFO);
		// Если возникла ошибка, ставим признак неудачного соединения
		if ($recs < 0) {
			if ($ss->connection->is_crytical_errors())
				$ss->set_bug_marker();
			$ss->single_output_results();
		}

		if (!$recs)
			$ss->single_output_results();

		if ($ss->bugs_count)
			$ss->destroy_bug_marker();

		$ss->sql_cache($recs);

		//$count=count($recs);
		//$stat->set_rec_count($count);
		//$stat->set_finish();
		$ss->single_output_results();
		//ji_log::i()->w("Single Search ".(($recs>0)? 'Успешный': 'Безрезультатный')." поиск. Перед обновлением найдено $count записей",I_INFO);


	}


	public static function single_reformat() {
		global $CFG, $__db, $reformat_debug;
		/* @var $rec record */

		ignore_user_abort(true);

		$r = mosGetParam($_REQUEST, 'r', $reformat_debug);
		$r = unserialize(urldecode($r));


		$ss = new single_search_unit($r);
		$recs = $ss->reformat_records();

		if ($recs < 0) {
			$ss->single_output_results();
			ji_log::i()->w("Ошибка переформатирования записей", I_ERROR);
			return;
		}

		$ss->sql_cache($recs, true);


		ji_log::i()->w("REFORMAT ", I_INFO);
		$ss->single_output_results();
	}

	public static function free_process() {
		global $CFG, $__db, $free_debug;
		/* @var $rec record */

		ignore_user_abort(true);

		$r = mosGetParam($_REQUEST, 'r', serialize($free_debug));
		$r = unserialize(urldecode($r));


		$ss = new single_search_unit($r);


		$recs = $ss->free_request();

		if ($recs < 0) {
			$ss->set_bug_marker();
		}

		if (!$recs)
			$ss->single_output_results('0');

		if ($ss->bugs_count)
			$ss->destroy_bug_marker();
		$ss->free_output_results($recs);

	}


	public static function get_options() {
		global $CFG;
		$st = new ji_st(JI_LOCK_NO);

		$search_form_ko = new ji_search_form_ko();

		$search_form_ko->fp = $st->fp;
		$search_form_ko->user = $st->user;
		$search_form_ko->cache_live_time = $CFG['resources_cache_time'];
		$search_form_ko->db_ko = $CFG['irb64_format_base'];
		$search_form_ko->ko_search_vuz_enable = $CFG['ko_search_vuz_enable'];

		$answer = array();
		if ($st->dic_function) {
			try {
				$answer = call_user_func(array($search_form_ko, $st->dic_function), $st->prefix, $st->request_array);
			}
			catch (Exception $e) {
				$answer[''] = 'Ошибка: ' . $e->getMessage() . ($e->getCode() ? '(' . $e->getCode() . ')' : '');
			}
		}

		//Вот это всё поместить в ji_search_form_ko!!!!!
		if ($st->dic_function !== 'get_disc_multiselect') {
			$search_form_ko->output_like_options_object($answer);
		}
		else {

			answer::jsone($answer);
		}
	}


	public static function get_terms() {
		global $__db, $CFG;
//====================================================================================		
		$st = new ji_st(JI_LOCK_NO);

		//$this->bns, $this->prefix, $this->start_term, $this->portion, $this->format,$this->req,$this->expired


		/*		$st=new stdClass();
				$st->local=false;
				$st->request_type='dic';
				$st->req='';
				$st->portion=10;
				$st->prefix='A=';
				$st->expired=0;
				$st->format='';
				$st->start_term='';


		/*		$sql = "
				SELECT libraries.*, bases.*
				FROM bases
				JOIN libraries USING(lib_id)
				";

				idb::i()->setQuery($sql);
				$st->src=idb::i()->loadAssocList('bl_id');*/
//====================================================================================
		/*		echo '{"success":"true",
						"result":{
							"история":"философии",
							"key2":"value2"
						}
					}';
				die();
		*/


		$ic = new broad_search_unit('', $st->bl_id_array, $st->bl_id_array_selected);

		$ic->src = $st->src;

		$ic->max_process_count = $CFG['max_process_count'];
		$ic->max_process_live_time = $CFG['max_process_live_time'];
		$ic->bad_connection_pause = $CFG['bad_connection_pause'];
		$ic->self_net_path = JI_FILE_PATH_AUTO_REQUESTS_NET_LOCALHOST_PROVIDER;
		$ic->bad_connection_pause = $CFG['bad_connection_pause'];

		$params['expired'] = $CFG['cache_relevance_time'];
		$params['local'] = $st->local = false;

		$params['request_type'] = 'dic';
		$params['format'] = '';
		$params['req'] = '';

		$params['portion'] = $st->portion_dic;
		$params['prefix'] = $st->prefix;
		$params['start_term'] = $st->term;

		$ic->start_free_process(array(&$ic, 'callback_free'), $params);

		$res = array();
		foreach ($ic->get_free_output() as $output) {

			if (!is_array($output))
				continue;

			foreach ($output as $term) {

				if (!is_array($term))
					continue;

				//$res[]=mb_strtolower($term['key'],'UTF-8');
				$res[] = $term['key'];

			}
		}
		$res = array_unique($res);
		sort($res);

		$terms_array = array_slice($res, 0, $st->portion_dic);
		answer::dic_terms($terms_array);
	}


	public static function show_results() {
		global $CFG, $__db, $qp;
		//die();


		$out = new output();

		//ji_log::i()->w("{$st->req_id_client}| OUTPUT: Начало show_results st->first_number={$st->first_number}",I_INFO);
		$out->max_results_renew_time = $CFG['max_results_renew_time'];
		$out->min_results_renew_time = $CFG['min_results_renew_time'];
		$out->sleep_time = $CFG['sleep_time'];
		$out->cache_relevance_time = $CFG['cache_relevance_time'];
		$output = $out->get_new_records();

		$st = $out->st;
		$GLOBALS['st'] = $st;

		//echo $st->keywords;

		$grid = new  ji_bo_grid();
		$grid->rp = $st->rp;
		$grid->number_enable = $st->number_enable;
		$grid->download_enable = $st->download_enable;
		//print_r($st->user);
		$grid->upload_enable = ji_upload::check_upload_rights($st);


		$grid->checkbox_enable = $st->checkbox_enable;
		$grid->cover_enable = $st->cover_enable;
		$grid->zebra_enable = $st->zebra_enable;
		$grid->pager_enable = $st->pager_enable;
		$grid->self_net_path = JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER;
		// Здесь действиетльно должен использоваться сетевой путь
		$grid->covers_path_local = JI_PATH_COVERS_NET;
		$grid->rec_id_array_selected = $st->rec_id_array_selected;

		$grid->cover_width = $CFG['cover_width'];
		$grid->cover_height = $CFG['cover_height'];

		$grid->books_raeting_enable = $CFG['books_raeting_enable'];
		$grid->books_comments_enable = $CFG['books_comments_enable'];
		if ($CFG['books_raeting_enable'] && is_array($output['recs']))
			$grid->books_raeting_array = $out->get_raeting_array(array_keys($output['recs']));

		$grid->first_number = $output['first_number'];

		$output['recs'] = $grid->bo_list($output);
		answer::jsone($output);
	}

	public static function print_results() {
		global $CFG, $__db, $qp;
		//die();
		$st = new ji_st();
		$st->write_close();

		$out = new output($st);
		$out->indication_mode = true;

		ji_log::i()->w("Начало print_results", I_INFO, $st->req_id_client);
		$out->max_results_renew_time = $CFG['max_results_renew_time'];
		$out->min_results_renew_time = $CFG['min_results_renew_time'];
		$out->sleep_time = $CFG['sleep_time'];
		$out->cache_relevance_time = $CFG['cache_relevance_time'];

		$output = $out->get_new_records();

		if ($output['recs'] && $st->finish_flag === 'last') {
			$print = new  ji_print();
			// Выполняем добавление заголовков.
			if ($st->print_titles) {
				//file_put_contents('C:\temp\output2',serialize($output));
				$output['recs'] = call_user_func("ji_titles::$st->print_titles", $output['recs'], $st->form_request);


			}

			$name = crc32($output['req'] . time());
			switch ($st->print_output) {
				case 'word':
					$result = $print->rtf($output);
					$ext = 'rtf';
					$file_path = JI_PATH_TMP_LOCAL . "/$name.$ext";
					@file_put_contents($file_path, $result);
					break;
				case 'printer':
					$result = $print->html($output);
					$ext = 'htm';
					$file_path = JI_PATH_TMP_LOCAL . "/$name.$ext";
					@file_put_contents($file_path, $result);
					break;

				case 'email':
					$result = $print->rtf($output);
					$ext = 'rtf';
					$file_path = JI_PATH_TMP_LOCAL . "/$name.$ext";
					ob_start();
					file_put_contents($file_path, $result);
					$mail = new PHPMailer();
					//$mail->SetLanguage('ru');

					$mail->Body = 'В прикреплённом к письму файле содержится список с отобранными Вами библиографическими записями';
					//$mail->CharSet="Windows-1251";
					$mail->CharSet = "UTF-8";
					//$mail->WordWrap = 75; 
					$mail->AddAddress($st->email, '');
					$mail->IsHTML(true);
					$mail->From = $CFG['mail_from'];
					$mail->FromName = 'Электронный каталог';
					$mail->AddAddress($email, '');
					$mail->Subject = 'Библиографические записи';
					$mail->SMTPAuth = true;
					$mail->Username = $CFG['mail_user_name'];
					$mail->Password = $CFG['mail_password'];
					$mail->Host = $CFG['mail_host'];
					$mail->Port = $CFG['mail_port'];
					$mail->Mailer = $CFG['mail_mailer'];
					$mail->AddAttachment($file_path);

					if (!$mail->Send())
						ji_log::i()->w("Не удалось отправить письмо: $mail->ErrorInfo", I_ERROR, '', 300);

					ob_clean();
					break;
			}

			$output['file_name'] = "$name.$ext";
		}


		$output['recs'] = '';
		answer::jsone($output);
	}


	public static function show_search_form() {
		global $CFG, $__db;
		// В данном случае берём параметры запроса запроса
		$st = new ji_st(JI_LOCK_FULL);
		$st->set_form_url();


		$search_form = new ji_search_form();
		$search_form->fp = $st->fp;
		$search_form->form_path = $st->form_path;
		$search_form->user = $st->user;

		$search_form->show();
		// st, search_form,CFG		

		$st->write_close();
	}


	public static function show_quick_format_special() {
		global $CFG;
		try {
			// Получает в качестве параметров $st->rec_id, $st->format
			$st = new ji_st(JI_LOCK_NO);

			$irb = new jwrapper(true);
			$out = new output($st);

			$rec = $out->get_rec_by_rec_id($st->rec_id);

			$format = $st->get_special_format_from_profile($st->bl_id, $st->format_type);
			//$CFG['cache_relevance_time']
			$result = $irb->RecVirtualFormat($CFG['irb64_format_base'], $format, $rec, 0);
			if ($result < 0)
				$result = "При расформатировании записи возникла ошибка: {$irb->get_last_error_message()}({$irb->get_last_error_code()}) ";
		}
		catch (Exception $e) {
			$result = 'Ошибка: ' . $e->getMessage() . ($e->getCode() ? '(' . $e->getCode() . ')' : '');
		}

		answer::quick_format($result);
	}

	public static function show_quick_format() {
		global $CFG;
		try {
			// Получает в качестве параметров $st->rec_id, $st->format
			$st = new ji_st(JI_LOCK_NO);

			$irb = new jwrapper(true);
			$out = new output($st);

			$rec = $out->get_rec_by_rec_id($st->rec_id);
			//$CFG['cache_relevance_time']
			$result = $irb->RecVirtualFormat($CFG['irb64_format_base'], '@' . $st->format, $rec, 0);
			if ($result < 0)
				$result = "При расформатировании записи возникла ошибка: {$irb->get_last_error_message()}({$irb->get_last_error_code()}) ";

		}
		catch (Exception $e) {
			$result = 'Ошибка: ' . $e->getMessage() . ($e->getCode() ? '(' . $e->getCode() . ')' : '');
		}
		answer::quick_format($result);
	}

	public static function set_selected_bases() {
		$st = new ji_st();
		$result = $st->set_bl_id_array_selected();
		answer::text($result);
	}

	public static function set_selected_user_profile() {
		$st = new ji_st(JI_LOCK_FULL);
		$result = $st->set_selected_user_profile();
		answer::text($result);
	}

	public static function set_selected_records() {
		$st = new ji_st();
		$result = $st->set_selected_records();
		answer::text($result);
	}


	public static function get_selected_count() {
		$st = new ji_st(JI_LOCK_NO);
		answer::text(count($st->rec_id_array_selected));
	}

	public static function clean_selected() {
		$st = new ji_st();
		$st->rec_id_array_selected = array();
		answer::text('0');
	}


	public static function clean_cache() {
		global $__db;
		idb::i()->setQuery('DELETE FROM #__portions');
		idb::i()->Query();
		idb::i()->setQuery('DELETE FROM #__req');
		idb::i()->Query();
		idb::i()->setQuery('DELETE FROM #__req_rec');
		idb::i()->Query();
		idb::i()->setQuery('DELETE FROM #__rec');
		idb::i()->Query();
		idb::i()->setQuery('DELETE FROM #__sort');
		idb::i()->Query();
		answer::text('Кэш очищен');
	}

	public static function clean_session() {
		$st = new ji_st();
		$st->clean_session();
		answer::text('Сессия очищена');
	}

	public static function get_cover() {
		global $CFG;
		//$st=new ji_st(true);	
		$isbn = mosGetParam($_REQUEST, 'isbn', '');
		if (!$isbn)
			return;
		$covers = new covers();

		$covers->covers_path_local = JI_PATH_COVERS_LOCAL;
		$covers->covers_download_enable = $CFG['covers_download_enable'];
		$covers->default_no_cover_name = JI_FILE_DEFAULT_COVER_IMAGE_NAME;
		$covers->get_image_by_isbn($isbn);
	}


	public static function show_exemp() {

		global $CFG;

		try {
			//print_r(ji_st::i(JI_LOCK_NO)->user);
			$rec_id = mosGetParam($_REQUEST, 'rec_id', '');

			$irb = new jwrapper(true);
			$out = new output();

			//$rec=$out->get_rec_by_rec_id($rec_id);
			$rec = ji_rec_common::get_orig_rec_by_rec_id($rec_id);

			//if (DEBUG_EXEMPLARS) {
			//$rec=unserialize($bo_grid_debug);
			//$rec=$rec[1];
			//}

			// Наследник ST!!!
			$ex = new ji_exemplars();
			$mhr_table = $ex->get_mhr_table();
			$mhr_ex = $ex->get_ex_count($rec);
			$mhr_data = $ex->get_full_mhr_data($mhr_ex, $mhr_table);
			$ex->show_ex($rec_id, $mhr_data);
		}
		catch (Exception $e) {
			answer::text('Ошибка: ' . $e->getMessage() . ($e->getCode() ? '(' . $e->getCode() . ')' : ''));
		}
	}

	public static function show_session() {
		//@session_start();
		$st = new ji_st(JI_LOCK_NO);
		answer::debug($st->r);

	}

	public static function show_log() {
		ji_log::i()->formated_output(ji_log::i()->get_txt_as_array());
		die();
	}

	public static function set_session_id() {
		$st = new ji_st();
		echo 'ID=' . session_id();
		echo 'NAME=' . session_name();

	}

	public static function confirm_update_adress() {
		answer::text('TRUE');
	}


	// Старые функции


	public static function update_list() {
		global $CFG;
		$file = $_REQUEST['file'];
		$login = mosGetParam($_REQUEST, 'login', '');
		$password = mosGetParam($_REQUEST, 'password', '');
		if ($login != $CFG['irb64_user'] || $password != $CFG['irb64_password'])
			answer::text("Ошибка в пароле($password) или логине($login)! Доступ запрещен!");
		if (($files_array = @unserialize(stripslashes($file))) === false)
			answer::text('Ошибка при unserialize списка файлов');
		$result = ji_update::get_list_of_required_files($files_array);
		answer::serialize($result);
	}

	public static function update() {
		global $CFG, $__db;
		$file = $_REQUEST['file'];
		$login = mosGetParam($_REQUEST, 'login', '');
		$password = mosGetParam($_REQUEST, 'password', '');
		@@u::del_full_dir(JI_PATH_TEMP_UPDATE_LOCAL);
		@@unlink(JI_PATH_TMP_LOCAL . '/' . JI_FILE_UPDATE);

		if ($login != $CFG['irb64_user'] || $password != $CFG['irb64_password'])
			answer::text('Ошибка в пароле или логине! Доступ запрещен!');
		if (!$file)
			answer::text('Отсутствует файл обновления! Возможно, действует ограничение на размер POST запроса');
		if (!($file_contents = base64_decode($file)))
			answer::text('Ошибка при декодировании файла обновления:' . $file);
		if (@file_put_contents(JI_PATH_TMP_LOCAL . '/' . JI_FILE_UPDATE, $file_contents) === false)
			answer::text('Ошибка при записи файла обновления:' . $file);
		//@@mkdir(JI_PATH_TEMP_UPDATE_LOCAL);
		//die();
		if (($result = ji_update::unzip()))
			answer::text("Ошибка при извлечении обновления: " . $result[0]);

		$result = ji_update::install_update_files();

		// Удаляем пользовательские сессии
		@@ji_update::clean_all_sessions();
		answer::serialize($result + ji_update::get_list_file(JI_PATH_COMPONENT_LOCAL . '/' . JI_FILE_NO_UPDATE_FILES_LIST));
	}


	public static function show_owners() {

		global $CFG;

		$irb = new jwrapper(true);
		$st = new ji_st();
		$out = new output($st);

		$rec = $out->get_rec_by_rec_id($st->rec_id);
		//if (DEBUG_EXEMPLARS) {
		//$rec=unserialize($bo_grid_debug);
		//$rec=$rec[1];
		//}


		$s = new ji_sigla();
		$bl_id_array = $s->get_bl_id_array($rec);
		$sources = $s->get_sources($bl_id_array);
		$output_array = $s->get_output_array($sources, $rec);
		$s->show_owners($output_array);

	}


	public static function ed() {

		global $CFG;
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '');

		$out = new output();
		$rec = $out->get_rec_by_rec_id($rec_id);

		$ed = new ji_ed();
		$ed->set_rec($rec);

		$files_array = $ed->get_avalable_files_array();
		$files_array = $ed->filter_avalable_files($files_array);
		$files_array = $ed->update_file_patchs($files_array);
		$ed->cache_files_array($files_array);
		$groupd_files_array = $ed->group_files_by_ext($files_array);
		$ed->show_ed($groupd_files_array);
	}

	public static function ed_download() {
		$bl_id = mosGetParam($_REQUEST, 'bl_id', '');
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '');
		$occ = mosGetParam($_REQUEST, 'occ', '');

		$ed = new ji_ed();
		// Вот таких ситуаций быть не должно.
		if (!($fa = $ed->get_download_file($rec_id, $occ))) {

			$out = new output();
			$rec = $out->get_rec_by_rec_id($rec_id);

			$ed->set_rec($rec);
			$files_array = $ed->get_avalable_files_array();
			$files_array = $ed->update_file_patchs($files_array);
			$fa = u::get_first_assoc_value($files_array);
		}
		$ed->download_file($fa);

	}


	public static function ed_pdf_view() {
		$bl_id = mosGetParam($_REQUEST, 'bl_id', '');
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '');
		$occ = mosGetParam($_REQUEST, 'occ', '');
		$ed = new ji_ed();
		$fa = $ed->get_download_file($rec_id, $occ);
		$ed->pdf_view_file($fa);

	}

	public static function show_upload_form() {
		$bl_id = mosGetParam($_REQUEST, 'bl_id', '1');
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '804345976');
		$mfn = mosGetParam($_REQUEST, 'mfn', '1');
		$bns = mosGetParam($_REQUEST, 'bns', 'IBIS');

		ji_upload::show_form($bl_id, $rec_id, $mfn, $bns);
	}


	public static function upload_file() {
		global $__db, $CFG;

		$bl_id = mosGetParam($_REQUEST, 'bl_id', '1');
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '804345976');
		$mfn = mosGetParam($_REQUEST, 'mfn', '1');
		$bns = mosGetParam($_REQUEST, 'bns', 'IBIS');
		$timestamp = mosGetParam($_REQUEST, 'timestamp', '');
		$token = mosGetParam($_REQUEST, 'token', '');

		try {
			if ($token !== md5($CFG['irb64_password'] . $timestamp))
				throw new Exception("Попытка взлома!", 6);

			/* @var $c jwrapper */
			$c = new jwrapper(true, 0, array('arm' => 'C'));
			$rec = $c->read_record($bns, $mfn, false);
			//($msg='',$type=I_INFO,$client_id='',$error_number=0,$error_level=0)
			if ($c->is_errors())
				throw new Exception("Ошибка доступа и блокировки MFN $mfn:" . $c->get_last_error_message(), $c->get_last_error_code());

			$upl = new ji_upload($rec, $_FILES['files'], $CFG['ed_path'], $bns, $mfn, $rec_id);
			$upl->check($CFG['ed_avalable_extensions']);
			$upl->set_file_path();
			$upl->build_dirs_file_move();
			$upl->update_rec();

			if (($c->RecWrite($bns, false, true, $upl->rec)) < 0)
				throw new Exception('Не удалось выполнить запись в БД ИРБИС: ' . $c->get_last_error_message(), $c->get_last_error_code());
			$upl->update_cached_rec($upl->rec);

		}
		catch (Exception $e) {
			ji_log::i()->w('Критическая ошибка: ' . $e->getMessage(), I_ERROR, 0, $e->getCode());
			answer::text('Критическая ошибка: ' . $e->getMessage() . " (" . $e->getCode() . ")");
		}
		answer::text('');
	}


	public static function order() {

		$rec_id = mosGetParam($_REQUEST, 'rec_id', '');
		$mhr = mosGetParam($_REQUEST, 'mhr_shot_name', '');
		$kaf = mosGetParam($_REQUEST, 'kaf_shot_name', '');
		$ex = new ji_exemplars();
		$answer = $ex->order($rec_id, $mhr, $kaf);

		answer::jsone($answer);

	}

	public static function grnti() {

		$rub = mosGetParam($_REQUEST, 'id', '');
		$grnti = new ji_rubrics();
		$answer = $grnti->grnti($rub);

		answer::jsone($answer);

	}

	public static function set_book_raeting() {
		global $__db;
		$rate = mosGetParam($_REQUEST, 'score', '');
		$rec_id = mosGetParam($_REQUEST, 'vote-id', '');

		$result = array();
		if ($rate) {
			idb::i()->setQuery("
				INSERT INTO `#__rec_rate` (`rec_id`, `rate`) 
				VALUES($rec_id, $rate)");

			idb::i()->Query();
			$result['status'] = 'OK';
		}
		else {
			$result['ok'] = 'start';
		}
		answer::jsone($result);

	}

	public static function rpc() {
		$class = mosGetParam($_REQUEST, 'class', 'jwrapper');
		$server = null;
		$server = new rpc($class);
		$server->ContentType = null;
		$server->Execute();
	}


	// Старые функции


	public static function download_record() {
		global $df, $sf;
		$rec_id = mosGetParam($_REQUEST, 'rec_id', '-2082672713');

		$ui = new user_interaction();
		$rec = $ui->get_rec_by_rec_id($rec_id);
		file_put_contents('rec', $rec->GetAsIrbRec());
		$library_data = $ui->get_library_by_rec_id($rec_id);

		$df = $ui->prepare_elements($df, $ui->deleted_fields);
		$rec_prepared = $ui->prepare_downloaded_rec($rec, $rec_id, $df);

		$rec_prepared->AddField(907, '^CICORP^A' . date('Ymd') . '^B' . $library_data[0]['shot_name']);

		if ($ui->generation == 32)
			$iso_rec = $ui->prepare_iso_size($rec, $sf);
		else
			$iso_rec = get_iso_rec($rec);

		show::iso_record($iso_rec);
		die();
	}


}


$sqlrez = idb::i()->getErrorNum();
if ($sqlrez <> 0 && $sqlrez <> 1007) {
	echo 'Ошибка БД: ' . idb::i()->getErrorMsg();
	//log::w('Ошибка БД: ' . idb::i()->getErrorMsg(),'err_finish' );
}



?>