<?php
if (!defined('_VALID_JIRBIS')){
	if (file_exists('../jirbis_link_libraries.php'))
		require_once('../jirbis_link_libraries.php');
			else 
		require_once('../../jirbis_link_libraries.php');
}
chdir(dirname(__FILE__));
class Config {
	public $config;
	protected $configFileName;
	protected $pdf_path_value='';

	public function __construct() {
		
		if (!defined('ROOT')) {
			define('ROOT', dirname(dirname(dirname(__FILE__))));
		}

		if (!defined('APP_DIR')) {
			define('APP_DIR', basename(dirname(dirname(__FILE__))));
		}
		


		//$this->configFileName = $this->getConfigFilename();
		//$temp_ini=$this->read_php_ini($this->configFileName);
		
		
		$this->config = $this->newConfig();
		/*SyntaxError: malformed hexadecimal character escape sequence
http://localhost/jirbis2/components/com_irbis/pdf_view/
Line 0
		*/
    }

	public function getConfig($key = null) {
		if ($key==='splitmode')
		return true;
		
		if($key !== null) {
/*			if ($key==='path.pdf'){
				if ($this->pdf_path_value)
				return $this->pdf_path_value;
				
				$ed=new ji_ed(true);		
				
				$fa=$ed->get_cached_file_data();				
					if (!is_array($fa)|| !$fa){
						echo 'Некорректный вызов просмотрщика';
						die();
					}				
				return dirname($fa['file_path_real']).'/';
			}
*/				
			
			if(isset($this->config[$key])) {
			  return $this->config[$key];
			}
			else {
			  return null;
			}
		}
		return $this->config;
	}

	public function getDocUrl() {
		return "<br/><br/>Click <a href='http://flexpaper.devaldi.com/docs_php.jsp'>here</a> for more information on configuring FlexPaper with PHP";
	}

	public function getConfigFilename() {
		if(strstr(PHP_OS, "WIN"))
			return ROOT . '\\' . APP_DIR . '\\config\\config.ini.win.php';
		return ROOT . '/' . APP_DIR . '/config/config.ini.nix.php';
	}

	public function saveConfig($array) {
		$this->write_php_ini($array, $this->configFileName);
	}

	function write_php_ini($array, $file) {
		$res = json_encode($array);
		$this->safefilerewrite($file, $res);
	}

	// read Config file.
	function read_php_ini($file) {
		if(file_exists($file)){
			$handle = fopen($file, "r");
			$contents = trim(fread($handle, filesize($file)));
			fclose($handle);
			$ret = json_decode($contents, true);
			if($ret != null)
				return $ret;
		}
		$ret = $this->newConfig();
		$this->saveConfig($ret);
		return $ret;
	}

	// Ключ -S позволяет не использовать шрифты в SWF
	// Ключ --password pass  позволяет подставлять пароль
	// -s protect -- реализует защиту от открытия в flash редакторах
	//(22558 )

	
	function newConfig(){
		// Если разрешено копирование -- никаких параметров. Если запрещено -- установлено true.
		$pdf_view_text_enable=u::ga($GLOBALS['CFG'],'ed_pdf_view_text_enable','') ? '' : ' -S';
		$pdf_view_password=(u::ga($GLOBALS['CFG'],'ed_pdf_view_password','')) ? (' --password '.u::ga($GLOBALS['CFG'],'ed_pdf_view_password','')) : '';
	
		
		$exe = strstr(PHP_OS, "WIN") ? ".exe" : "";
		$config["allowcache"]						=	true;
		$config["splitmode"]						=	true;
		$config["path.pdf"]							=	u::get_temp()."/";
		$config["path.swf"]							=	u::get_temp()."/";
		$config["renderingorder.primary"]			=	"flash";
		$config["renderingorder.secondary"]			=	"html";
		$config["pdf2swf"]			=	true;
		$config["licensekey"]			=	'';
		$config["cmd.conversion.singledoc"]			=	JI_PATH_PDF_VIEW_LOCAL."/exe/pdf2swf$exe {path.pdf}{pdffile} -o {path.swf}{pdffile}.swf -f -T 9 -t -s storeallcharacters -s linknameurl".$pdf_view_text_enable.$pdf_view_password;
		$config["cmd.conversion.splitpages"]		=	JI_PATH_PDF_VIEW_LOCAL."/exe/pdf2swf$exe {path.pdf}{pdffile} -o {path.swf}{pdffile}_%.swf -f -T 9 -t -s storeallcharacters -s linknameurl".$pdf_view_text_enable.$pdf_view_password;
		$config["cmd.conversion.renderpage"]		=	JI_PATH_PDF_VIEW_LOCAL."/exe/swfrender$exe {path.swf}{swffile} -p {page} -o {path.swf}{pdffile}_{page}.png -X 1024 -s keepaspectratio";
		$config["cmd.conversion.rendersplitpage"]	=	JI_PATH_PDF_VIEW_LOCAL."/exe/swfrender$exe {path.swf}{swffile} -o {path.swf}{pdffile}_{page}.png -X 1024 -s keepaspectratio";
		$config["cmd.conversion.jsonfile"]			=	JI_PATH_PDF_VIEW_LOCAL."/exe/pdf2json$exe {path.pdf}{pdffile} -enc UTF-8 -compress {path.swf}{pdffile}.js";
		$config["cmd.conversion.splitjsonfile"]		=	JI_PATH_PDF_VIEW_LOCAL."/exe/pdf2json$exe {path.pdf}{pdffile} -enc UTF-8 -compress -split 10 {path.swf}{pdffile}_%.js";
		$config["cmd.searching.extracttext"]		=	JI_PATH_PDF_VIEW_LOCAL."/exe/swfstrings$exe {swffile}";
		$config["cmd.query.swfwidth"]				=	JI_PATH_PDF_VIEW_LOCAL."/exe/swfdump$exe {swffile} -X";
		$config["cmd.query.swfheight"]				=	JI_PATH_PDF_VIEW_LOCAL."/exe/swfdump$exe {swffile} -Y";
		return $config;
	}

	function trace($data) {
		$this->safefilerewrite(ROOT . '\\' . APP_DIR . "\\config\\log.txt", date("Y/m/d g:i a :: ") . $data . "\r\n", "a+");
	}

	function safefilerewrite($fileName, $dataToSave, $log = "w") {
		if ($fp = fopen($fileName, $log)) {
			$startTime = microtime();
			do {
				$canWrite = flock($fp, LOCK_EX);
				// If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
				if(!$canWrite) usleep(round(rand(0, 100)*1000));
			} while ((!$canWrite)and((microtime()-$startTime) < 1000));

			//file was locked so now we can store information
			if ($canWrite) {
				fwrite($fp, $dataToSave);
				flock($fp, LOCK_UN);
			}
			fclose($fp);
		}else{
			die("<b>Can't write to config $fileName </b>");
		}
	}
}