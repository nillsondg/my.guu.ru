<?php
require_once('conv_base.php');

/*require_once('u.php');

if (!function_exists('__autoload')) {
	function __autoload($classname){
		$file="$classname.php";
		if (file_exists($file))	
			require_once($file);
	}
}*/

class conv_knigafund extends conv_base{
	public function convert(jrecord $rec){
		//REC_OUT
		$recn=new jrecord();
		$recn->add_from_another_rec(951,'I',$rec,'856','U');
		$recn->add_from_another_rec(331,'',$rec,330,'A');
		$recn->add_from_another_rec(101,'',$rec,101,'A');
		$recn->add_from_another_rec(210,'D',$rec,210,'D');
		$recn->add_from_another_rec(210,'C',$rec,210,'C');
		$recn->add_from_another_rec(210,'A',$rec,210,'A');
		$recn->add_from_another_rec(610,'',$rec,610,'A');

		$recn->SetSubField(200,1,'A',$this->convert_title($rec->GetSubField(200,1,'A')));	
		$recn->SetField(700,1,$this->convert_author($rec->GetSubField(700,1,'A').', '.$rec->GetSubField(700,1,'B')));
		
		
		$id=$this->get_id($rec->GetField(903,1));
		$recn->SetField(903,1,$id);		
		$recn->SetRec_id(crc32($id));

		$recn->SetField(101,1,'rus');		
		$recn->SetField(919,1,'^Arus^N50^Gca');		
		$recn->SetField(900,1,'^Tl^b05');		
		$recn->SetField(920,1,'PAZK');
		return $recn;
	}
	
	private function convert_title($title){
		$output='';		
		if (preg_match('{(.+): ?([^:]+)$}',$title,$ap)){			
			return $ap[1].'^E'.$ap[2];
		}		
		return $title;
		
	}
	private function convert_author($author){
		$output='';		
		if (preg_match('{([^,\. ]+)\,? ?([^,\. ]*)\.? ?([^,\. ]*)\.?}',$author,$ap)){
			$output=($ap[2]) ? $ap[1].'^B'.mb_substr($ap[2],0,1,'UTF-8').'.' : $ap[1];
			$output.=($ap[3]) ? ' '.mb_substr($ap[3],0,1,'UTF-8').'.' : '' ;
			return '^A'.$output;
		}		
		return '';
	}
	private function get_id($book_url){
		$output='';		
		if (preg_match('{\d+}',$book_url,$ap)){
			return 'kf'.$ap[0];
		}		
		return '';
	}

}
/*$rec_array=unserialize(file_get_contents('z39.res'));    	
print_r($rec_array[0]);
$c=new conv_knigafund();

echo $c->get_id('http://www.knigafund.ru/books/52');
*/
//print_r($c->convert($rec_array[0]));

?>