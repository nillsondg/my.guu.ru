<?php 



define( "_MOS_NOTRIM", 0x0001 );
define( "_MOS_ALLOWHTML", 0x0002 );
function mosGetParam( &$arr, $name, $def=null, $mask=0 ) {
	$return = null;
	if (isset( $arr[$name] )) {
		if (is_string( $arr[$name] )) {
			if (!($mask&_MOS_NOTRIM)) {
				$arr[$name] = trim( $arr[$name] );
			}
			if (!($mask&_MOS_ALLOWHTML)) {
				$arr[$name] = strip_tags( $arr[$name] );
			}
			if (!get_magic_quotes_gpc()) {
				$arr[$name] = addslashes( $arr[$name] );
			}
		}
		return $arr[$name];
	} else {
		return $def;
	}
}

    function get_profile_as_array($profile,$crc_for_string=false){
		$res=array();
		if ($profile){
			foreach ($profile as $par){
				if(strpos($par['format'],'@')!==false){
					$res[]=$par['format'];
				}else {
					$res[]=$crc_for_string ? crc32($par['format']) :$par['format'];
				}
			}
		}
		sort($res);
		return $res;
	}

	
	function get_profile_types_as_array($profile){
		$res=array();
		if ($profile)
			$res=array_keys($profile);
		else 
		return array();			
		sort($res);
		return $res;
	}
	
	function get_format_from_profile($format_type,$profile){
		if (isset($profile[$format_type]['format'])) 
			return $profile[$format_type]['format'];
		else 
			return '';		
	}
	// Проверяет соответствие профиля записи текущему профилю
    function check_rec_profile_congruents($profile=array(),$jrecord){
		$res=array();
		$rec_profile=$jrecord->GetProfileAsArray();
		if ($profile && $jrecord){
			foreach ($profile as $type=>$par){
				if 	(empty($rec_profile[$type])) return false;
			}
		}
		return true;
	}
	
    function update_base_profile($base_profile,$special_profile){
		foreach($special_profile as $special_key=>$special_value){
				$base_profile[$special_key]=array('format'=>$special_value['format'],'type'=>(isset($special_value['type']) ? $special_value['type'] : 'bo'));				
		}		
    	return $base_profile;
	}	

	
function get_iso_rec($irbis_rec){
  $fields_delimiter="\x1E";

  $marker='';
  $guide='';
  $data='';

  $data_length=0;
  $guide_len=0;
  
  $content=$irbis_rec->GetContent();
  
  if (!is_array($content)) return '';
  
	foreach($content as $field_number=>$field_value){
		 foreach($field_value as $field_occ_value){		 		
		      	$field_occ_value.=$fields_delimiter; 
		       $guide.=sprintf('%03d%04d%05d',$field_number,strlen($field_occ_value),$data_length);
		       $data.=$field_occ_value;
		       $data_length+=strlen($field_occ_value);
		       $guide_len+=12;				
		}
	}	
	//Длинна записи и  Базовый адрес данных
  $marker=sprintf("%05d0000022%05d0004500",strlen($data)+26+strlen($guide),25+strlen($guide));
  
  return $marker.$guide.$fields_delimiter.$data.$fields_delimiter;
}


function get_command_line_params(){
	$command_line=array();
	if ($_SERVER['argv']){
		
		foreach ((array)$_SERVER['argv'] as $v)
		{
		     $arr = explode("=",$v);
		     if (is_array($arr)) $command_line[$arr[0]] = $arr[1];
		}
		return $command_line;
	}
	return array();
}


function arrayToObject($array){
	if(count($array)>0){
		foreach($array as $key => $value){
			if(is_array($value)){
				$array[$key] = o($value);
			}
		}
	 
		return (object)$array;
	}else{
		return false;
	}
}


function object_to_array($object){
	if(is_object($object)){
		$array=(array)$object;
		foreach($array as $key => $value){
			if(is_object($value)){
				$array[$key] = (array)$value;
			}
		}
	 
		return (array)$array;
	}else{
		return false;
	}
}


?>