<?php

class ji_sigla {
	


	public function get_bl_id_array($rec){
		$bl_id_array=array();		
		for($i=1;(($bl_id=$rec->GetSubField(902,$i,'X')));$i++)
			$bl_id_array[]=$bl_id;		
		return $bl_id_array;
	}
	
	public function get_sources($bl_id_array=array()){
		global $dbi,$CFG;
		
		if (!$bl_id_array)
		 return array();
		 
		 $bl_id_string=implode(',',$bl_id_array);
		 
		
		$svk_libraries_base=empty($CFG['svk_libraries_base']) ? 'ICORP': $CFG['svk_libraries_base'];
		//$dbi= new database( $CFG['mysql_host'], $CFG['mysql_user'], $CFG['mysql_password'], $svk_libraries_base, $CFG['mysql_database_prefix'] ,false);
	
		
		$sql="
		SELECT *  
		FROM #__libraries, #__bases
		WHERE   #__libraries.lib_id = #__bases.lib_id 
		AND #__libraries.svk=1			
		AND  #__bases.bl_id IN ($bl_id_string)			
        ORDER BY #__libraries.lib_id           
		";
		$dbi->setQuery($sql);				
		$sources=$dbi->loadAssocList('sigla');	
		return $sources;
	}	

	

	public function get_output_array($sources,$rec){
		$output_array=array();
		
		$format_bo=',"(<.>G="v210^d"$<.>)","(<.>G="v463^j"$<.>)","*(<.>A="v700^a"$<.>)", "*(<.>T="v200^a"$<.>)","*(<.>T="v461^c"$<.>)",';
		$format_isbn='"(<.>B="v10^a"$<.>)"';		
		$format=$rec->GetSubField(10,1,'A') ?  $format_isbn : $format_bo;		
		$request=$rec->pfte($format);
		foreach($sources as $src_key=>$src_value){
			$is_web_irbis=strpos($src_value['url'],'cgiirb')!==false ? true : false;        
			$o=array();
			$o['sigla']=$src_key;
			$o['url']="http://{$src_value['url']}?I21DBN={$src_value['bns']}&P21DBN={$src_value['bns']}&I21DBNAM={$src_value['bns']}&C21COM=S&option=com_irbis&Itemid=300&S21ALL=".u::to_uri( $is_web_irbis ? $request : u::utf_win($request));
			$o['full_name']=$src_value['full_name'];
			$output_array[]=$o;
		}
		return $output_array;		
	}
	public function show_owners($output_array){	
		?>
		<table class="show_ex">
		<?php
		// 1.Название отдела или кафедры выдачи
		// 2.Количество экземпляров
		// 3. Количество свободных экземпляров 
		if (!$output_array){
			?>
			<tr>	
				<td>
				<?php
				 echo '<span class="no_exems">Не найдены допустимые значения сигл</span>';			  
				?>
				</td>
			</tr>
			<?php		
			return;
		}
		
			?>
		<tr>				
			<th class="ex_number_cell">
			№
			</th>
	
			<th class="ex_full_name_cell">
			Название библиотеки
			</th>
		</tr>
		<?php 

		
		
		$i=0;
		foreach($output_array as $src){
			?>
			<tr>	
				<td class="ex_number_cell">
				<?php
				 echo ++$i.". ";
				?>
				</td>
				
				<td class="ex_full_name_cell">
				<?php
				 echo "<a  target=\"_blank\" class=\"sigla_link\"  href=\"{$src['url']}\">"; 
				 echo $src['full_name'];			  
				 echo "</a>"
				?>
				</td>
			</tr>
		<?php 
		}
		?>
		</table>
		<?php
	}
	
}



?>