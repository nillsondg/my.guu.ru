<?php

class iexception extends Exception{
    public $level;

    public function __construct($message='', $code = 0,$level=0) {
        parent::__construct($message, $code);
        $this->level=(int)$level;
    }

    
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getLevel() {
        return $this->level;
    }

    public function setLevel($level) {
        return $this->level=$level;
    }


}

?>