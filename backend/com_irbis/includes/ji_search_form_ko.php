<?php

// Похоже класс действительно нужно включать в ji_search_form_ko.php
class ji_search_form_ko extends ji_search_form{
	public $cache_live_time=36000;
	public $db_ko='IBIS';
	public $ko_search_vuz_enable=true;
	public $vuz_req_rules_data=array('spec'=>'DiscS=','vo'=>'DiscVO=','sem'=>'DSS=','kafch'=>'KAFCH=');
	// НИкакой прорисовки здесь быть не должно!!!!!!!! Пользовать должен иметь полный контроль над оформлением 
	//и выводить вывод каких-то элементов за пределы формы -- концептуальное нарушение логики автодополнением!!!!!
	

	//==============================ФУНКЦИИ ПОЛУЧЕНИЯ ДАННЫХ ДЛЯ ВЫПАДАЮЩИХ СПИСКОВ ФОРМЫ=========================================
	// Аргумент в пользу того, что дисциплины нужно брать из базы VUZ -- нам нужны не только обеспеченные, но и не обеспеченные дисциплины. 
	// А специальности скорее всего все будут обеспечены. 
	
	// Контекстно-независимое получение дисциплин для мультиселекта
	public function get_disc_multiselect($prf,$req=array()){
		// Это вообще должно всё выводиться по запросу. 
		
		$result=array();
		$result['page']=1;
		$result['total']=1;		
		$result['records']=0;
		// Чтобы корректно обработать эксепшен, перехватываем его здесь.
		try{
			
			
			$vuz_req='';
			$vuz_req_field='';
			foreach ($this->vuz_req_rules_data as $field_name=>$prf){
				if (empty($req[$field_name]) or !$req[$field_name])
					continue;
				switch($field_name){
					case 'spec':
					case 'vo':
					case 'kafch':
						$vuz_req_field.=($vuz_req_field? '*' :'')."<.>$prf{$req[$field_name]}<.>";
					break;
					/*	Семестры из базы VUZ не индексируются для записей DISC, а альтернативных вариантов обработки попросту нет */
					case 'sem':
						//$vuz_req_field.=($vuz_req_field ? '*' :'')."<.>$prf".u::get_number($req[$field_name])."<.>";	
					break;									
								
					default:
						$vuz_req.=($vuz_req ? '*' :'')."(<.>$prf{$req[$field_name]}<.>)";								
					
/*	Семестры из базы VUZ не индексируются для записей DISC, а альтернативных вариантов обработки попросту нет
*/				}		
				
					
				
			}
			$vuz_req.=($vuz_req_field && $vuz_req ? '*' :'').($vuz_req_field ?  "($vuz_req_field)" : '');	
			// Если есть какие-то элементы отбора и база VUZ Используется, то 
			if ($this->ko_search_vuz_enable && $vuz_req){

				
				$recs_disc=ji_rec_common::find_records('VUZ',$vuz_req,'',1,0,$this->cache_live_time);
				if ($recs_disc){
					$disc_array=array();
					foreach($recs_disc as &$rec){
						$disc_array[]=$rec->GetSubField(3,1,'A');
						$rec=null;
					}					
					
					$disc_array=array_unique($disc_array);
					sort($disc_array);
					
					$result['records']=count($disc_array);
																	
					foreach($disc_array as $disc){
						$result['rows'][]=array('id'=>$disc,'cell'=>array($disc));				
					}
			
					return $result;	
				}					
			
			}elseif (($dic_terms=ji_rec_common::get_dic_terms($this->db_ko, 'DISC=', '', 0,$this->cache_live_time))){			
				$result['records']=count($dic_terms);
				foreach($dic_terms as $term){
					$result['rows'][]=array('id'=>$term['key'],'cell'=>array($term['key']));				
				}
		
				return $result;					
			}else {
				throw new Exception("В базе каталога не найден словарь дисциплин",11);
			}
		}catch (Exception $e){
			$result['rows'][]=array('id'=>'','cell'=>array('Ошибка: '.$e->getMessage().($e->getCode()? '('.$e->getCode().')' : '')));
			return $result;
		}		
		
		$result['rows'][]=array('id'=>'','cell'=>array('Дисциплины не найдены')); 
		return $result;

	}

	
	// Контекстно-независимое получение дисциплин
	public function get_disc($prf){
		// Это вообще должно всё выводиться по запросу. 
		if (($res=$this->get_dic_and_mnu_decode($prf)))
				return array((string)''=>(string)'')+$res;
			else 
				return array(''=>'Дисциплины не найдены');

	}
	
	public function get_sem($prf){	
		/* @var $this->user jrecord */	
		/* @var $this->user jrecord */	
		$sem_array=array();		
		for($i=1;$i<=12;$i++)
			$sem_array['s'.$i]=(string)$i;
			
		if ($this->user and ($user_key=$this->user->GetSubField(90,1,'F'))){			
			$sem_array=array('s'.$user_key=>$sem_array['s'.$user_key])+array_diff_key($sem_array,array('s'.$user_key=>''));
			return $sem_array;
		}
		
		return array((string)''=>(string)'')+$sem_array;
	}	
	
	public function get_purpose($prf){				
		$res=$this->get_dic_and_mnu_decode($prf,JI_FILE_PURPOSE_MNU,'VUZ'); 
		return array((string)''=>(string)'')+$res;
	}	
	
	public function get_spec($prf){
		if (($res=$this->get_dic_and_mnu_decode_with_rdr($prf,JI_FILE_SPEC_MNU,'VUZ',90,'C')))
				return $res;
			else 
				return array((string)''=>'Специальности не найдены');
	}	

	public function get_kafch($prf){
		
		if (($res=$this->get_dic_and_mnu_decode($prf,JI_FILE_KAF_MNU,'VUZ')))
				return array((string)''=>(string)'')+$res;
			else 
				return array(''=>'Кафедры не найдены');
	}	

	
	public function get_vo($prf){
		// Если существут нужный нам словарь
		if (($dic_data=$this->get_dic_and_mnu_decode_with_rdr($prf,JI_FILE_VO_MNU,'VUZ',90,'V'))){		
			return $dic_data;				
		}else{ 
			return array(''=>'Виды обучения не найдены');
		}
	}	
	

}



?>