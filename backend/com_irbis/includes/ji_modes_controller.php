<?php

class ji_modes_controller{
	public static function run($params_text='',$search_mode='',$mode=''){		
		$mode= !$mode ? 'main_form' :$mode;
		@define('JI_MODE',$mode);
		
		$class_name="ji_$mode";	
		$class_path=JI_PATH_COMPONENT_LOCAL."/modes/$mode/$class_name.php";
		$class_show_path=JI_PATH_COMPONENT_LOCAL."/modes/$mode/".$class_name."_show.php";
		if (!file_exists($class_path))
			throw new Exception('Не найден класс '.$class_path);								
		
		$params_array=self::component_params_translate($params_text);
						
		include_once($class_path);
		@include_once($class_show_path);
		// -- добавить!!!!!!!!!!!!!!
		if (!JFactory::getUser()->id && ji_st::i(JI_LOCK_FULL,$params_array)->user){
				ji_st::i()->user=null;
		}
		ji_st::i()->write_close();		
		call_user_func(array($class_name,'output'),$params_array,$search_mode);					
	}	

	public static function component_params_translate($params_field_data=''){

		if (!$params_field_data) 
			return array();
		
		$params=array();	
		$params_array=explode("\r\n",trim($params_field_data));
		
		foreach ($params_array as $param_string){
			$param_array=explode('=',$param_string);
			
			if (($length=strpos($param_array[0],'[]'))!==false){
				$par_name=substr($param_array[0],0,$length);
				if (!isset($params[$par_name])) 
					$params[$par_name]=array(); 
				$params[$par_name][]=$param_array[1];				
			}else{
				$params[$param_array[0]]=$param_array[1];				
			}
			
		}
			
			
		
		return $params;
	}
	
}

?>