<?php

class session{
	
	public $session_id='JSESS';
	public $session_prefix='jsess_';
	public $session_dir_path='';
	public $jsession_lock_wait_timeout=10;
	public $make_empty_session_in_lock_no=true;
	protected $session=array();
	protected $session_file=null;
	


	public function __construct($read_only=JI_LOCK_FULL,$session_id='JSESS',$session_prefix='jsess_',$session_dir_path='',$jsession_lock_wait_timeout=10,$make_empty_session_in_lock_no=true){
		
		$this->session_id=$session_id;
		$this->session_prefix=$session_prefix;
		$this->session_dir_path=$session_dir_path;
		$this->jsession_lock_wait_timeout=$jsession_lock_wait_timeout;
		$this->make_empty_session_in_lock_no=$make_empty_session_in_lock_no;
		
		$session_path=$this->get_session_path();
		$this->session=array();
		//while(file_exists($session_path) and !is_writable($session_path)) usleep(10);
		
		// Если файл присутствует или создание сессии запрещено
		if ($read_only==JI_LOCK_NO && (file_exists($session_path) || !$this->make_empty_session_in_lock_no)){
				$this->read_session($session_path);								
		}else{			
			$this->lock_session($session_path);
			
		}
		
		
	}
	
	public function read_session($session_path){
		for($i=0;;$i++){
			
			//$this->session_file=fopen($session_path,'r');
			if (file_exists($session_path)){
				if (($content=@file_get_contents($session_path))!==false){
					
					if ($content && ($this->session=@unserialize($content))!==false){
						break;
					}else {
						if (!$content)
						ji_log::i()->w("Ошибка при интерпретации файла сессии без блокировки -- пустой файл: ".$session_path,I_ERROR);	
						else 
						ji_log::i()->w("Ошибка при интерпретации файла сессии без блокировки: ".$session_path.'|'.$content,I_ERROR);	
						//$this->session=array();
					}
					
				}else{
					ji_log::i()->w("Ошибка при чтении файла сессии без блокировки: ".$session_path,I_ERROR);	
				}					
			}else{
					ji_log::i()->w("Отсутствует файл сессии".$session_path,I_ERROR);	
			}
			
			if ($i>$this->jsession_lock_wait_timeout){
				return false;
			}
			
			usleep(1000);
			
		}
		
		//$this->session=array();		
		return true;
	}
	
	public function lock_session($session_path){
		
			//register_shutdown_function(array(&$this,'on_abort_action'));
			//die($session_path);
			if (($this->session_file=fopen($session_path,'a+b'))===false){
				ji_log::i()->w("Ошибка при попытке открытия файла сессии с блокировкой",I_INFO);
				
			}else{
				// Страховка от чрезмерно длительной блокировки сессии. Если время блокировки больше разумного, копируем сессию в новый файл			
				for($i=0;;$i++){
					
					// Если блокировка устанавливается
					if (flock($this->session_file,LOCK_EX+LOCK_NB)){
						$this->get_session_file();	
						break;
					}
						
					ji_log::i()->w("Дожидаемся освобождения сессии $session_path",I_INFO);							
					if ($i>$this->jsession_lock_wait_timeout){						
						$this->get_session_file();	
						// Генерируем новый путь к сессии			
						ji_log::i()->w("Не дождались освобождения сессии, генерируем новый путь",I_INFO);							
						$session_path=$this->get_session_path(true);
						$this->session_file=fopen($session_path,'a+b');	
						flock($this->session_file,LOCK_EX);
						break;					  		
					}
					
					usleep(1000);
				}
				//\ Страховка от чрезмерно длительной блокировки сессии
			}		
	}
	
	
	public function write_close(){
		if (is_resource($this->session_file)){
			ftruncate($this->session_file,0);
			fwrite($this->session_file,serialize($this->session));
			// Вот это и приводило к проблемам!!!! 
			//@flush($this->session_file);
			@flock($this->session_file, LOCK_UN);
			fclose($this->session_file);
			$this->session_file=null;
			//ji_log::i()->w("Разблокирован файл ".$this->get_session_path(),I_INFO);	
		}

	}

	public function clean_session(){
		if (is_resource($this->session_file)){
			ftruncate($this->session_file,0);
			//flush($this->session_file);
			@flock($this->session_file, LOCK_UN);
			fclose($this->session_file);
			$this->session_file=null;

		}

	}
	
	public function get_session(){
		return $this->session; 
	}
	
	protected function get_session_file(){
		if (($file=fgets($this->session_file))){
			if (($this->session=@unserialize($file))===false)
				ji_log::i()->w("Ошибка при интерпретации файла кэша!",I_ERROR,'',10);						
		}
		fseek($this->session_file,0);
	}
	
	protected function get_session_path($new=false){
		global $CFG;
		$session_name=isset($_COOKIE[$this->session_id]) ? $_COOKIE[$this->session_id] : '';
		
		//print_r($_COOKIE);
		//if (count(@$_COOKIE[$session_name])>1) die('DDDDDDDDDDDDD') ;
		
		//ji_log::i()->w();
		//file_put_contents('cookie.txt',var_export($_COOKIE));
		//.$CFG['ji_dir_jirbis'] ? $CFG['ji_dir_jirbis'].'/': '' 
		if (!$session_name || $new){
			
			$session_name=md5(@session_id().time());			
			@setcookie($this->session_id,$session_name,time()+365*24*60*60,'/' );
			
			ji_log::i()->w("Создана новая сессия с идентификатором: $session_name. new=$new",I_INFO);													
		}

		$session_path=$this->session_dir_path.'/'.$this->session_prefix.$session_name;
		//while(file_exists($session_path) and !is_writable($session_path)) usleep(10);
		
		return $session_path;
	}
	
	
	
	
}
?>