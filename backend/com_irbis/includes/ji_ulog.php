<?php
require_once('ilog.php');

class ji_ulog extends ilog{
	
 
	protected static $instance;  // object instance
		
	public function w($msg='',$type=I_INFO,$client_id='',$error_number=0,$error_level=0){				
		
	
		
		if (self::$display_text) 
			$this->output_string_text($msg,$type,$error_number,$error_level);	
		else 	
			$this->output_string($msg,$type,$error_number,$error_level);	

		parent::w($msg,$type,$client_id);		
	}	
	
	/**
     * Форматирование и вывод протокола
     *
     * @return ji_ulog
     */

	//Форматирование протокола
	public function formated_output($log_array){
		
		if (!is_array($log_array))
			echo 'Данные протокола отсутствуют!';
			
		foreach($log_array as $c){
			$this->output_string($c['msg'],$c['type']);
		}
	}
	
	
	public function output_string($msg='',$type=I_INFO,$error_number=0,$error_level=0){	
		switch ($type){
			case I_ERROR: $message='<span style="background-color:red">'.$msg.'</span></br>'; break;
			case I_INFO_HEADER:	$message='<h1>'.$msg.'</h1></br>'; break;
			case I_INFO: 
			default: 
			$message=''.$msg.'';	break;
		}
		
		
		?>
		<tr>

			<td>
				<?php echo floatval((u::get_microtime()-self::$start_microtime)/10000) ?>
			</td>
							
						
			<td>
				<?php echo $message; ?>
			</td>			
			
		</tr>
		<?php
		@ob_flush();
		flush();
		
	}

	public function output_string_text($msg='',$type=I_INFO,$error_number=0,$error_level=0){	
		if (!$msg) 
			return;
		switch ($type){
			case I_ERROR: $message='!!!!!!'.$msg.'!!!!!!!'; break;
			case I_INFO_HEADER: "\r\n".	$message=mb_strtoupper($msg,'UTF-8')."\r\n"; break;
			case I_INFO: 
			default: 
			$message=''.$msg.'';	break;
		}		
			
		echo floatval((u::get_microtime()-self::$start_microtime)/10000);
		echo @iconv('UTF-8','cp866//IGNORE', " --- $message, Тип: $type". (($error_number) ? "Номер ошибки: $error_number, Уровень ошибки: $error_level" : '')."\r\n");
			
			flush();
	}	
		
		
	public function __construct(){
		self::$debug_path=JI_PATH_DEBUG_LOCAL;
		self::$start_microtime=u::get_microtime();

		if (self::$display_text) {
			echo "\r\n********************************\r\n";
			return;
		}
		
		@header('Content-Type: text/html; charset=utf-8');
		@ob_flush();
		flush();
		
		?>		
		<html>
			<head>
					<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
			</head>					
		<body>
		<table width="100%" border="1">		
		<?php
		flush();
		@ob_flush();
	}
	public function __destruct(){
		if (self::$display_text) {
			echo "\r\n********************************\r\n";
			return;
		}

		?>
		</table>
		</body>
		</html>
		<?php		
	}
    /**
     * Возвращает единственный экземпляр класса
     *
     * @return ji_ulog
     */
    public static function i() {
        if ( is_null(self::$instance) ) {
            self::$instance = new ji_ulog;
        }
        return self::$instance;
    }	
	
}

/* @var ilog::i() ilog */
/* @var ilog::i ilog */
/*jlog::$debug_enable=true;
jlog::$debug_path='C:/temp';
jlog::i()->w('main','6666','Message',I_INFO);
jlog::i()->w('temp','888','MEssage',I_INFO);
jlog::i()->formated_output(jlog::i()->get_txt_as_array());
jlog::i()->sql('SQL','sssss',array('f'));
jlog::i()->packet('PACKET','dfsdfsdf');
jlog::i()->replace_debugs();
*/


?>