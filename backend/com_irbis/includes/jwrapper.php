<?php




/*include("$mosConfig_absolute_path/jirbis_configuration.php");
require_once("u.php");


define('NO_PAR','You must enter components parametrs!');

$menu = $mainframe->get( 'menu' );
$params_txt= new mosParameters($menu->params );
$params=$params_txt->toArray();





*/

/*function __autoload($classname){
	
	$file="$classname.php";
	if (file_exists($file))	
		require_once($file);
}
*/
if (!function_exists('__autoload')) {
	function __autoload($classname){
		$file="$classname.php";
		if (file_exists($file))	
			require_once($file);
	}
}
class jwrapper{
	
	public $connection;
	public $params;	
	public $last_req_full_count=0;
	public $bl_id;
	public $format_fst=array('USMARC'=>'SMARCI','UNIMARC'=>'UMARCIW','RUSMARC'=>'RMARCI');	
	public $fst_php_names=array('z3950.knigafund.ru'=>'conv_knigafund');	
	public $gbl_php_after_fst_names=array('USMARC'=>'gbl_marc','RUSMARC'=>'gbl_marc');
	private $temp_path;
	private $connection_home=null;
	private $local=false;
	
	
	public function __construct($local=true, $bl_id=0,  $params=array()){
		global $CFG;
		
		$this->bl_id=$bl_id;

		$this->params=$params;


		$this->params['irb64_arm']=$CFG['irb64_arm'];
		$this->params['irb64_host']=$CFG['irb64_host'];
		$this->params['irb64_port']=$CFG['irb64_port'];
		$this->params['irb64_user']=$CFG['irb64_user'];
		$this->params['irb64_password']=$CFG['irb64_password'];
		$this->params['irb64_timeout']=$CFG['irb64_timeout'];
		$this->params['irb64_format_base']=$CFG['irb64_format_base'];
		
		if ($local) {
			$this->local=true;
			$this->params['connection_type']='iserver64';
			$this->params['url']=$this->params['irb64_host'];
			$this->params['port']=$this->params['irb64_port'];
			$this->params['login']=$this->params['irb64_user'];
			$this->params['password']=$this->params['irb64_password'];
			$this->params['timeout']=$this->params['irb64_timeout'];
		}
		
		$this->params['charset']=(isset($this->params['charset']) ? $this->params['charset'] :'utf');
		$this->params['arm']=(isset($this->params['arm']) ? $this->params['arm'] :'R');
		
		// Для Z39 сервера
		

		$this->params['login']=(isset($this->params['login'])  && $this->params['login'] ? $this->params['login'] :'');
		$this->params['password']=(isset($this->params['password']) && $this->params['password'] ? $this->params['password'] :'');

		$this->params['remote_encoding']=(isset($this->params['remote_encoding']) ? $this->params['remote_encoding'] :'utf');		
		$this->params['remote_format']=(isset($this->params['remote_format']) ? $this->params['remote_format'] :'USMARC');		
		$this->params['fst_out_name']=(isset($this->params['fst_out_name']) ? $this->params['fst_out_name'] : @$this->format_fst[$this->params['remote_format']]);
		$this->params['is_persistent']=(isset($this->params['is_persistent']) ? $this->params['is_persistent'] :true);		
		$this->params['is_piggy_back']=(isset($this->params['is_piggy_back']) ? $this->params['is_piggy_back'] :true);		
		
			
		

		
		switch($this->params['connection_type']){
			case 'iwi':	
				$this->connection=new iwi($this->params['url'],$this->params['port'],$this->params['timeout'],$this->params['password'],$this->params['charset'],$this->bl_id);
				break;				
			case 'iwi_ex': 
				$this->connection=new iwi_ex($this->params['url'],$this->params['port'],$this->params['timeout'],$this->params['password'],$this->params['charset'],$this->bl_id);
				break;
				//$c=new iwi_ex($src['url'],$src['port'],$src['timeout'],$src['password']); break;				
			case 'iserver64':	
				$this->connection=new iserver64($this->params['url'],$this->params['port'],$this->params['timeout'],$this->params['login'],$this->params['password'],$this->params['charset'],$this->params['arm'],$this->bl_id);				
				break;
			case 'iz39':	
				$this->connection=new iz39($this->params['url'],$this->params['port'],$this->params['timeout'],$this->params['login'],$this->params['password'],$this->params['remote_encoding'],$this->params['remote_format'],$this->params['is_persistent'],$this->params['is_piggy_back'],$this->bl_id);				
				break;
				
		}
		
		$this->temp_path=u::get_temp();
	}
	
	public function __call($metod,$arg){
		for($astr='',$ig='',$i=0;$i<count($arg);$astr.=$ig.'$arg['.$i.']',$i++,$ig=',');
		eval('$res=$this->connection->'.$metod.'('.$astr.');');
		return $res;
	}
	
	
	public function find_jrecords($db,  $search_expression, $seq = '', $first_number=1, $portion=0,$format_types=array(), $expired=0) {
		$bl_id=$this->bl_id;
		
		if (($res=$this->get_cache('j',$expired,$bl_id.$db,$search_expression.$seq.$portion.$first_number.serialize($format_types)))!==false) return $res;        
		
		
		switch ($this->params['connection_type']){
			case 'iserver64':
				$res=$this->connection->find_jrecords($db, $search_expression, $seq, $first_number, $portion, $format_types);
				$this->last_req_full_count=$this->connection->last_req_full_count;			
				return $res;
				
			break;
			
			case 'iwi':
			case 'iwi_ex':
				
				// Здесь нужно вначале получить количество записей, если нет записей, то и выполнять запрос бесполезно записи... 
				$this->last_req_full_count=$this->connection->req_full_count($db,$search_expression);
				
				if ($this->last_req_full_count<0)	
					return $this->last_req_full_count;
				if ($this->last_req_full_count==0) 
					return array();        										
				$recs=$this->FindRecords($db, $search_expression, $seq, $first_number, $portion);				
			break;
				
			case 'iz39':
				$recs=$this->FindRecords($db, $search_expression, $seq, $first_number, $portion);
				$this->last_req_full_count=$this->connection->last_req_full_count;								
			break;						
		}
		
		// ------- Выполняется для всех случаев кроме iserver64---------------
		
		if (!is_array($recs) || !$recs || !$this->last_req_full_count) 
			return $recs; 
		 
		$this->make_home_connection();
		
		$format='';			
		foreach($format_types as $format_values){
			$format.="'".FD."'".((substr($format_values['format'],0,1)==='@') ? "\x1C".substr($format_values['format'],1)."\x1D" : $c->rep_special($format_values['format']));
		}
		
		$res_str_arr=$this->connection_home->format_virtual_records($this->params['irb64_format_base'], $format, $recs);				
		
		if ($this->connection_home->is_crytical_errors()){
			if (!$this->local)
				$this->connection->add_errors($this->connection_home->get_errors());
			return $this->connection_home->get_last_error_code();
		}
		
        for ($rn=0;$rn < count($res_str_arr); $rn++){
				$res_parts=explode(FD,$res_str_arr[$rn]);	        
				//$res[$rn] = new jrecord();
	            $res[$rn]=$recs[$rn];
	            $res[$rn]->Set_bl_id($this->bl_id);
		        $res[$rn]->SetProfileAsArray($format_types);
	         // Раскладываем по массиву расформатирований   
	        $k=0;
            foreach($format_types as $format_key=>$format_values){   
            	$k++;
            	if (empty($res_parts[$k])) 
            			$res[$rn]->SetFormValues($format_key,"Отсутствует результат расформатирования для $format_key. Возможно, ошибка в формате.");	
            		else
            			$res[$rn]->SetFormValues($format_key,$res_parts[$k]);	
            }
        }
		
		
		$this->set_cache('j',$expired,$bl_id.$db,$search_expression.$seq.$portion.$first_number.serialize($format_types),$res);	
		return $res;
	}
	
	
		public function reformat_records($jrecords,$format_types=array()) {
			if (!$jrecords) return ;

			 $format_needed_array=array();
			 $format_strings_array=array();
			 for($i=0;$i<count($jrecords);$i++){
			 	$format_strings_array[$i]='';
		 		$formats_array=$jrecords[$i]->GetFormatsAsArray();
				if ($format_types){ // В случае если настройки расформатирования заданы в профиле и уканазы в параметрах функции
			 		foreach($format_types as $format_key=>$format_values){
						if (array_search($format_values['format'],$formats_array)===false){
							$format_strings_array[$i].="'".FD."'".((substr($format_values['format'],0,1)==='@') ? "\x1C".substr($format_values['format'],1)."\x1D" : $c->rep_special($format_values['format']));
							$format_needed_array[$i][$format_key]=$format_values;
						}
					}
				}else{
					// В случае если присутствуют пустые значения формата в самой записи
					foreach($jrecords[$i]->GetProfileAsArray() as $format_key=>$format_values){
						if ($format_values['format'] && empty($format_values['value'])){
							$format_strings_array[$i].="'".FD."'".((substr($format_values['format'],0,1)==='@') ? "\x1C".substr($format_values['format'],1)."\x1D" : $c->rep_special($format_values['format']));
							$format_needed_array[$i][$format_key]=$format_values;							
						}
					}
				}
				

			 }
			
			 
			 // Если массивы требуемых форматов не заполнены, значит ничего доформатировать не требуется. Возвращаем исходный массив. 
			if  (!$format_needed_array) 
				return $jrecords;
				

					
			$this->make_home_connection();
			
			if ($this->connection_home->is_crytical_errors()){
				if (!$this->local)
					$this->connection->add_errors($this->connection_home->get_errors());
				return $this->connection_home->get_last_error_code();
			}
			
			$unique_formats=array_unique($format_strings_array);
			$res_str_arr=array();
			// Перебираем униальные варианты форматов
			foreach($unique_formats as $unique_format){
				$recs=array();				
				$recs_numbers=array();
				// Набираем порцию для расформатирования
				// Создаём подборку записей с уникальным форматом
				foreach($format_strings_array as $record_number=>$format_string){
										
					if ($format_string===$unique_format) {
						$recs[]=$jrecords[$record_number];
						$recs_numbers[]=$record_number;
					}
				}	

				$res_str_arr=$this->connection_home->format_virtual_records($this->params['irb64_format_base'], $unique_format, $recs);	
							
				if ($this->connection_home->is_crytical_errors()){
					if (!$this->local)
						$this->connection->add_errors($this->connection_home->get_errors());
					return $this->connection_home->get_last_error_code();
				}
								
				// Обработка полученной порции записей				
		        for ($d=0;$d < count($res_str_arr); $d++){
		        	// Определяем номер текущей записи в исходном массиве записей
		        	$rn=$recs_numbers[$d];
					$res_parts=explode(FD,$res_str_arr[$d]);	        

					if ($format_types){
			            // Объединяем профили, затирая оригинальные новыми. Только в том случае, если общий профиль для записей указан. 
			            $new_profile=array_merge($format_types,$jrecords[$rn]->GetProfileAsArray());
				        $jrecords[$rn]->SetProfileAsArray($new_profile);
					}
		         
			        // Раскладываем по массиву расформатирований   
			        $k=0;
		            foreach($format_needed_array[$rn] as $format_key=>$format_values){   
		            	if (isset($res_parts[$k+1]))
		            		$jrecords[$rn]->SetFormValues($format_key,$res_parts[++$k]);	
		            }
		        }

			}
			
		return $jrecords;
	}

	
	public function search_list($db_string, $search_expression, $seq, $format, $portion = 0,$first_number = 1,   $expired=0){
        
		if (($res=$this->get_cache('l',$expired,$db_string,$search_expression.$seq.substr($format,0,120).$portion.$first_number))!==false) return $res;        
        
        $db_array=explode(',',$db_string);
        $res_summary='';
        
        foreach ($db_array as $db){
        	
        	$res=$this->connection->search_list($db, $search_expression, $seq, $format, $portion, $first_number);        	
        	if ($res<0)	return $res;        	
        	$res_summary.=$res;        	
        }
        
        $this->set_cache('l',$expired,$db_string,$search_expression.$seq.substr($format,0,120).$portion.$first_number,$res_summary);
        return $res_summary;
    }
    
	public function FindRecords($db_string, $search_expression, $seq = '', $first_number = 1, $portion = 0,$expired=0) {
		$res_summary=array();
		$res=array();
		
		if (($res=$this->get_cache('r',$expired,$db_string,$search_expression.$seq.$portion.$first_number))!==false) return $res;        
        
        $db_array=explode(',',$db_string);
        
        
        foreach ($db_array as $db){

			switch($this->params['connection_type']){
				case 'iz39':	
				
					$res=$this->connection->find_marc_jrecords($db, $search_expression, $seq, $first_number, $portion);
		    		
					if ($res<0)	
		        		return $res;        	
		        	if (!is_array($res) || !$res) 
		    			continue;          		
		    							
	        		if (($php_fst_name=u::ga($this->fst_php_names,$this->params['url']))){
						$res=$this->apply_php_to_rec_array($php_fst_name,$res);
					}else{ 
		        		$this->make_home_connection();
						
						if ($this->connection_home->is_crytical_errors()){
							// Если CONNECTION_HOME и CONNECTION -- не одно не явяются частями одного и того же локального соединиения
							if (!$this->local)
								$this->connection->add_errors($this->connection_home->get_errors());
							return $this->connection_home->get_last_error_code();
						}        		
		        		// Если переформатирование не удаётся, инициируются connection_home ошибка
			        	$res=$this->connection_home->apply_fst_to_rec_array(APATH_DB,$this->params['irb64_format_base'],$this->params['fst_out_name'],$res);  
			    		
			        	
						if ($this->connection_home->is_crytical_errors()){
							if (!$this->local)
								$this->connection->add_errors($this->connection_home->get_errors());
							return $this->connection_home->get_last_error_code();
						}
						// Выполняем постредактуру
						if (($php_gbl_name=u::ga($this->gbl_php_after_fst_names,$this->params['remote_format'])))
							$res=$this->apply_php_to_rec_array($php_gbl_name,$res);					
					}
				break;
				default:
		    		$res=$this->connection->FindRecords($db, $search_expression, $seq, $first_number, $portion);        			        		
			}				

        	if ($res<0)	
        		return $res;        	
        	if (!is_array($res) || !$res) 
    			continue;          		
       		$res_summary=array_merge($res_summary,$res);
        }
        
        $this->set_cache('r',$expired,$db_string,$search_expression.$seq.$portion.$first_number,$res_summary);
		return $res_summary;
    }
	
    public function RecVirtualFormat($db, $format, $rec,$expired=0){
    	$rec_txt= (is_object($rec)) ? $rec->GetAsIrbRec() : $rec;
    	if (($res=$this->get_cache('v',$expired,$db,$format.$rec_txt))!==false) return $res;        
    	$res=$this->connection->RecVirtualFormat($db, $format, $rec_txt);        	
    	$this->set_cache('v',$expired,$db,$format.$rec_txt,$res);
		return $res;    	
    }
    
    public function GetTermList($db, $prf='', $start_term='', $count='', $format = '',$req='',$expired){
      if (($res=$this->get_cache('t',$expired,$db,$prf.$start_term.$count.$req))!==false) 
      return $res;        
      else 
       $res=$this->connection->GetTermList($db, $prf, $start_term, $count, $format,$req);
       if ($res<0)	return $res;        	
       
        $this->set_cache('t',$expired,$db,$prf.$start_term.$count.$req,$res);
    	return $res;
    }
    
    public function last_search_total_count(){
    	return $this->last_req_full_count;
    }

	public function apply_php_to_rec_array($php_fst,$rec_array){
		$output_rec_array=array();
		if (is_array($rec_array)){
			foreach($rec_array as &$rec){
				$output_rec_array[]=$this->apply_php_to_rec($php_fst,$rec);
			}
		}
		return $output_rec_array;
		
	}
    
	public function apply_php_to_rec($php_fst,$rec){		
		include_once("$php_fst.php");
		$c=new $php_fst;
		return $c->convert($rec,$this->params,$this->bl_id);
	}

    public function GetFile($path='', $db='', $file_name,$expired=0){
    	if (($res=$this->get_cache('fi',$expired,$db,$path.$file_name))!==false) 
    		return $res;        
    	$res=$this->connection->GetFile($path, $db, $file_name);        	
    	$this->set_cache('fi',$expired,$db,$path.$file_name,$res);
		return $res;    	
    }
	
    private function get_cache($type,$expired,$db,$req_conditions){
		
		if ($expired==0)return false; 
		
		$dir_name=str_replace(',','_',$db);
		$hash=abs(crc32($req_conditions));
		$file_path="{$this->temp_path}/$dir_name/$type$hash";
		if (file_exists($file_path)){
			if (filemtime($file_path)>time()-$expired){
				return unserialize(file_get_contents($file_path));
			}else 
			unlink($file_path);			
		}
		return false;		
	}
	
	private function set_cache($type,$expired,$db,$req_conditions,$data){
	
		if ($expired==0)
			return false; 
		
		$dir_name=str_replace(',','_',$db);
		$hash=abs(crc32($req_conditions));
		$file_path="{$this->temp_path}/$dir_name/$type$hash";
		
		if (!file_exists("{$this->temp_path}/$dir_name")) 
			@mkdir("{$this->temp_path}/$dir_name");
			
		@file_put_contents($file_path,serialize($data));	
		return true;		
	}
	
	private function make_home_connection($arm=''){
		
		if ($this->local && !$this->connection_home) 
			$this->connection_home=&$this->connection;
		
		if (!$this->connection_home)
			$this->connection_home=new iserver64($this->params['irb64_host'],$this->params['irb64_port'],'',$this->params['irb64_user'],$this->params['irb64_password'],'utf',($arm ? $arm : 'R'));							

	}
	
	
}



/*include('../jirbis_configuration.php');
ilog::$debug_enable=true;
ilog::$debug_log_enable=true;
ilog::$log_permanent_writing=true;
ilog::$display_text=true;
$params=$CFG;
$params['connection_type']='iz39';
*/


/*
$params['url']='aleph.rsl.ru';
$params['is_piggy_back']=false;
$params['is_persistent']=false;
$params['login']='';
$params['password']='';

$params['timeout']=30;
$params['port']=9909;
$irb=new jwrapper(false,0,$params);

$res=$irb->req_full_count('rsl01','"G=1982');
echo "<h1>$res</h1>";
$res=$irb->FindRecords('rsl01','"G=1982"','',1,10);
print_r($res);
*/




/*$params['url']='ns1.gbs.spb.ru';
$params['port']=210;

$params['is_piggy_back']=true;
$params['is_persistent']=true;
$params['remote_format']='RUSMARC';
$params['remote_encoding']='utf';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,21,$params);

$res=$irb->req_full_count('plain','<.>A=иванов$<.>');
echo "<h1>$res</h1>";

$profile=array(
'full'=>array('format'=>'@jfull','type'=>'bo'),
'brief'=>array('format'=>'@brief','type'=>'bo'),
);


$records=$irb->find_jrecords('plain','<.>A=Иванов$<.>','',1,10,$profile);
print_r($records);

*/
/*$params['url']='www.benran.ru';
$params['port']=210;

$params['is_piggy_back']=false;
$params['is_persistent']=false;
$params['remote_format']='USMARC';
$params['remote_encoding']='win';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,0,$params);
$res=$irb->req_full_count('CATALOG_LNS','"G=1982"');*/


//echo $irb->connection->get_last_error_message();

/*echo "<h1>$res</h1>";
$res=$irb->FindRecords('CATALOG_LNS','"G=1982"','',1,10);
print_r($res);*/



/*
$params['url']='zserv.libfl.ru';
$params['port']=210;

$params['is_piggy_back']=false;
$params['is_persistent']=false;
$params['remote_format']='RUSMARC';
$params['remote_encoding']='utf';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,0,$params);
$res=$irb->req_full_count('books','<.>G=1982<.>');
//echo $irb->connection->get_last_error_message();

echo "<h1>$res</h1>";
$res=$irb->FindRecords('books','<.>A=иванов<.>','',1,10);
print_r($res);
*/

/*$params['url']='z3950.loc.gov';
$params['port']=7090;

$params['is_piggy_back']=true;
$params['is_persistent']=true;
$params['remote_format']='USMARC';
$params['remote_encoding']='utf';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,0,$params);
$res=$irb->req_full_count('voyager','<.>G=2001<.>');
//echo $irb->connection->get_last_error_message();

echo "<h1>$res</h1>";
$res=$irb->FindRecords('voyager','<.>T=система$<.>','',1,10);
print_r($res);*/

//mysql_real_escape_string 

//preg_replace(, '', $src)
//echo preg_replace('/\/|\\|\:|\*|\?|\<|\>|\|/',' ',serialize($res[1]));
//echo dechex(ord(substr($res[5]->GetField(200,1),9,1)));
//echo substr($res[5]->GetField(200,1),9,1);
/*$params['url']='193.233.14.5';
$params['port']=9999;

$params['is_piggy_back']=false;
$params['is_persistent']=false;
$params['remote_format']='RUSMARC';
$params['remote_encoding']='win';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,0,$params);
$res=$irb->req_full_count('katb','<.>G=2001<.>');
//echo $irb->connection->get_last_error_message();

echo "<h1>$res</h1>";
$res=$irb->FindRecords('katb','<.>A=иванов$<.>','',1,10);
print_r($res);
*/


/*$params['url']='nbmgu.ru';
$params['port']=210;

$params['is_piggy_back']=false;
$params['is_persistent']=false;
$params['remote_format']='USMARC';
$params['remote_encoding']='win';
$params['timeout']=30;
$params['login']='';
$params['password']='';

$irb=new jwrapper(false,0,$params);
$res=$irb->req_full_count('book','<.>G=2001<.>');
//echo $irb->connection->get_last_error_message();

echo "<h1>$res</h1>";
$res=$irb->FindRecords('book','<.>A=иванов$<.>','',1,10);
print_r($res);
*/

//$db_string, $search_expression, $seq = '', $first_number = 1, $portion = 0,$expired=0)
//$res=$irb->connection_home->apply_fst_to_array(APATH_DB,$this->params['irb64_format_base'],$this->params['fst_out_name']);  


//$records=unserialize('a:3:{i`:0;O:7:"jrecord":6:{s:5:"bl_id";i:0;s:9:"formating";a:2:{s:4:"full";a:3:{s:6:"format";s:5:"@kn_h";s:4:"type";s:2:"bo";s:5:"value";s:1180:"</><b> 32.97<br> Р 558<br> </b> <b> Рихтер, Д. </b> <b></b> <br><dd> Программирование серверных приложений для Microsoft Windows 2000 [Текст] : мастер-класс / Д. Рихтер, Д. Кларк ; пер. с англ. В. Г. Вшивцева. - СПб. : Питер ; М. : Рус. ред., 2001. - 566 с. + 1 эл. опт. диск (CD-ROM). - Предм. указ.: с. 557 - 566. - <b>ISBN </b>5-318-00296-X (в обл.). - <b>ISBN </b>5-7502-0137-6 : 312.50 р.<br><table width="100%"><tr> <td><b> ГРНТИ </b><td width="50%"><b> ББК </b></td> </tr><tr> <td>50.41.15<td width="50%">32.97</td> </tr></table><br><b> Рубрики: </b> <br>Операционные системы Windows 2000<br>Windows 2000<br><br><b> Кл.слова (ненормированные): </b> защита информации -- серверные приложения -- windows management instrumentation -- windows 2000 -- cgi<br><b> Доп. точки доступа: </b> <br>Кларк, Д.<br>Вшивцев, В. Г.экземпляры в отделах: </b> всего 1 : М (1)<br> Свободны: М (1)<br><br> ";}s:5:"brief";a:3:{s:6:"format";s:6:"@brief";s:4:"type";s:2:"bo";s:5:"value";s:258:"Рихтер Д. Программирование серверных приложений для Microsoft Windows 2000 [Текст] : мастер-класс / Д. Рихтер, Д. Кларк ; пер. с англ. В. Г. Вшивцева, 2001. - 566 с.";}}s:7:"Content";a:38:{i:101;a:1:{i:0;s:3:"rus";}i:919;a:1:{i:0;s:11:"^Arus^N0102";}i:920;a:1:{i:0;s:4:"PAZK";}i:999;a:1:{i:0;s:7:"0000000";}i:964;a:1:{i:0;s:8:"50.41.15";}i:700;a:1:{i:0;s:19:"^AРихтер^BД.";}i:903;a:1:{i:0;s:18:"32.97/Р558-701137";}i:606;a:2:{i:0;s:54:"^AОперационные системы Windows 2000";i:1;s:14:"^AWindows 2000";}i:906;a:1:{i:0;s:5:"32.97";}i:60;a:1:{i:0;s:1:"2";}i:3600;a:1:{i:0;s:4:"ИГ";}i:102;a:1:{i:0;s:2:"RU";}i:3000;a:1:{i:0;s:13:"^c52^273^3454";}i:3026;a:1:{i:0;s:0:"";}i:3011;a:1:{i:0;s:15:"^A0^B444732^DМ";}i:3013;a:1:{i:0;s:5:"32.97";}i:3706;a:1:{i:0;s:10:"РЕТРО";}i:3022;a:1:{i:0;s:0:"";}i:701;a:1:{i:0;s:20:"^AКларк^BД.^U2";}i:10;a:2:{i:0;s:23:"^A5-318-00296-X^D312.50";i:1;s:15:"^A5-7502-0137-6";}i:320;a:1:{i:0;s:36:"Предм. указ.: с. 557 - 566";}i:3333;a:1:{i:0;s:4:"UYUY";}i:3707;a:8:{i:0;s:10:"НЕТКК";i:1;s:10:"НЕТКК";i:2;s:10:"НЕТКК";i:3;s:10:"НЕТКК";i:4;s:10:"НЕТКК";i:5;s:10:"НЕТКК";i:6;s:10:"НЕТКК";i:7;s:10:"НЕТКК";}i:3700;a:1:{i:0;s:8:"20071221";}i:3703;a:1:{i:0;s:28:"СИНХРОНИЗИРУЕМ";}i:702;a:1:{i:0;s:38:"^4730 Пер.^AВшивцев^BВ. Г.";}i:3025;a:1:{i:0;s:0:"";}i:215;a:1:{i:0;s:55:"^A566^E1^2o=эл. опт. диск (CD-ROM)^3в обл.";}i:3010;a:1:{i:0;s:37:"КОРРЕКТИРОВАНО 20090210";}i:3602;a:1:{i:0;s:8:"20090210";}i:621;a:1:{i:0;s:5:"32.97";}i:907;a:39:{i:0;s:24:"^CКР^a20031021^BБЕП";i:1;s:24:"^CКТ^a20040906^BЛТВ";i:2;s:24:"^CКР^a20050126^BСКЕ";i:3;s:22:"^CС^A20051208^BЗЕА";i:4;s:24:"^CРК^A20060504^BСРБ";i:5;s:24:"^CИЗ^A20060510^BАЕГ";i:6;s:22:"^CС^A20060915^BСКЕ";i:7;s:22:"^CС^A20060920^BСКЕ";i:8;s:22:"^CС^A20061122^BСКЕ";i:9;s:24:"^CКР^A20061219^BАОА";i:10;s:24:"^CРК^A20061221^BСРБ";i:11;s:24:"^CРК^A20070413^BПРК";i:12;s:24:"^CИЗ^A20070925^BАЕГ";i:13;s:24:"^CИЗ^A20071015^BАЕГ";i:14;s:24:"^CКН^A20071015^BСРБ";i:15;s:24:"^CКР^A20071017^BСРБ";i:16;s:24:"^CКТ^A20071019^BСКЕ";i:17;s:22:"^CС^A20071023^BКРП";i:18;s:24:"^CКН^A20071221^BСРБ";i:19;s:24:"^CКН^A20071221^BСРБ";i:20;s:24:"^CКН^A20071221^BСРБ";i:21;s:24:"^CКР^A20080201^BАОА";i:22;s:24:"^CКН^A20071221^BСРБ";i:23;s:24:"^CКН^A20071221^BСРБ";i:24;s:24:"^CКН^A20071221^BСРБ";i:25;s:24:"^CКН^A20071221^BСРБ";i:26;s:24:"^CКН^A20071221^BСРБ";i:27;s:24:"^CКН^A20071221^BСРБ";i:28;s:24:"^CКН^A20071221^BСРБ";i:29;s:24:"^CКН^A20071221^BСРБ";i:30;s:24:"^CКН^A20071221^BСРБ";i:31;s:24:"^CКТ^A20080331^BСКЕ";i:32;s:24:"^CКТ^A20080421^BСКЕ";i:33;s:22:"^CС^A20090210^BАОА";i:34;s:24:"^CРК^A19970101^BПРК";i:35;s:24:"^CКТ^A20090402^BСКЕ";i:36;s:24:"^CКР^A20090922^BАОА";i:37;s:24:"^CРК^A20100927^BНТС";i:38;s:24:"^CРК^A20101129^BАОА";}i:210;a:2:{i:0;s:27:"^CПитер^AСПб.^D2001";i:1;s:22:"^CРус. ред.^AМ.";}i:900;a:1:{i:0;s:19:"^Ta^B05^C454^2j9^Xk";}i:200;a:1:{i:0;s:211:"^AПрограммирование серверных приложений для Microsoft Windows 2000^Eмастер-класс^FД. Рихтер, Д. Кларк ; пер. с англ. В. Г. Вшивцева";}i:610;a:5:{i:0;s:33:"ЗАЩИТА ИНФОРМАЦИИ";i:1;s:39:"СЕРВЕРНЫЕ ПРИЛОЖЕНИЯ";i:2;s:34:"WINDOWS MANAGEMENT INSTRUMENTATION";i:3;s:12:"WINDOWS 2000";i:4;s:3:"CGI";}i:908;a:1:{i:0;s:6:"Р 558";}i:910;a:1:{i:0;s:45:"^A0^B444732^C20010924^DМ^H00042577^U2005/225";}}s:3:"mfn";i:0;s:6:"Status";i:0;s:10:"RecVersion";i:0;}i:1;O:7:"jrecord":6:{s:5:"bl_id";i:0;s:9:"formating";a:2:{s:4:"full";a:3:{s:6:"format";s:5:"@kn_h";s:4:"type";s:2:"bo";s:5:"value";s:973:"</><b> 32.97<br> П 97<br> </b> <b> Пэдвик, Г. </b> <b></b> <br><dd> Microsoft Outlook 97 в подлиннике [Текст] : переводное издание / Г. Пэдвик. - СПб. : БХВ-Петербург, 1997. - 734 с. : ил. - Предм. указ.: с. 730 - 734. - <b>ISBN </b>5-7791-0055-1. - <b>ISBN </b>0-7897-1096-X : 130.00 р.<br><table width="100%"><tr> <td><b> ГРНТИ </b><td width="50%"><b> ББК </b></td> </tr><tr> <td>50.41.25<td width="50%">32.97</td> </tr></table><br><b> Рубрики: </b> <br>Информация -- Хранение<br><br><b> Кл.слова (ненормированные): </b> mail -- outlook 97 -- office 97 -- электронная почта -- архивация -- www -- web-страницы -- visual basic<br><b> Доп. точки доступа: </b> <br>Власенко, С.экземпляры в отделах: </b> всего 1 : ЧЗ (1)<br> Свободны: ЧЗ (1)<br><br> ";}s:5:"brief";a:3:{s:6:"format";s:6:"@brief";s:4:"type";s:2:"bo";s:5:"value";s:110:"Пэдвик Г. Microsoft Outlook 97 в подлиннике [Текст] / Г. Пэдвик, 1997. - 734 с.";}}s:7:"Content";a:29:{i:101;a:1:{i:0;s:3:"rus";}i:919;a:1:{i:0;s:11:"^Arus^N0102";}i:920;a:1:{i:0;s:4:"PAZK";}i:10;a:2:{i:0;s:23:"^A5-7791-0055-1^D130.00";i:1;s:15:"^A0-7897-1096-X";}i:215;a:1:{i:0;s:13:"^A734^Ca-ил";}i:320;a:1:{i:0;s:36:"Предм. указ.: с. 730 - 734";}i:903;a:1:{i:0;s:17:"32.97/П97-775163";}i:606;a:1:{i:0;s:40:"^AИнформация^BХранение";}i:964;a:1:{i:0;s:8:"50.41.25";}i:906;a:1:{i:0;s:5:"32.97";}i:700;a:1:{i:0;s:19:"^AПэдвик^BГ.";}i:3000;a:1:{i:0;s:27:"^Ta^B05^C22^2581^3454^Xk^Ta";}i:60;a:1:{i:0;s:1:"2";}i:3600;a:1:{i:0;s:4:"АТ";}i:102;a:1:{i:0;s:2:"RU";}i:3011;a:1:{i:0;s:15:"^A0^B444574^DМ";}i:3013;a:1:{i:0;s:5:"32.97";}i:210;a:1:{i:0;s:42:"^CБХВ-Петербург^AСПб.^D1997";}i:702;a:1:{i:0;s:36:"^4730 Пер.^AВласенко^BС.";}i:621;a:1:{i:0;s:5:"32.97";}i:3026;a:1:{i:0;s:0:"";}i:999;a:1:{i:0;s:8:"00000000";}i:907;a:13:{i:0;s:22:"^CС^A20030320^BБЕП";i:1;s:24:"^CКТ^A20030522^BАОА";i:2;s:24:"^CКР^A20040316^BБЕП";i:3;s:24:"^CКР^A20040406^BАОА";i:4;s:24:"^CКР^A20040414^BАОА";i:5;s:24:"^CКР^A20050131^BСКЕ";i:6;s:22:"^CС^A20050302^BЗЕА";i:7;s:24:"^CКР^A20051202^BЧНН";i:8;s:24:"^CКР^A20061006^BЧНН";i:9;s:24:"^CИЗ^A20090517^BАЕГ";i:10;s:24:"^CРК^A20100111^BАОА";i:11;s:24:"^CКР^A20100405^BПРК";i:12;s:24:"^CКР^A20100618^BКОИ";}i:900;a:1:{i:0;s:19:"^Ta^B05^C454^2j9^Xk";}i:3072;a:1:{i:0;s:8:"00018591";}i:200;a:1:{i:0;s:64:"^AMicrosoft Outlook 97 в подлиннике^FГ. Пэдвик";}i:610;a:8:{i:0;s:4:"MAIL";i:1;s:10:"OUTLOOK 97";i:2;s:9:"OFFICE 97";i:3;s:33:"ЭЛЕКТРОННАЯ ПОЧТА";i:4;s:18:"АРХИВАЦИЯ";i:5;s:3:"WWW";i:6;s:20:"WEB-СТРАНИЦЫ";i:7;s:12:"VISUAL BASIC";}i:908;a:1:{i:0;s:5:"П 97";}i:910;a:1:{i:0;s:47:"^A0^B444574^C19970313^DЧЗ^H00018591^U2003/170";}}s:3:"mfn";i:0;s:6:"Status";i:0;s:10:"RecVersion";i:0;}i:2;O:7:"jrecord":6:{s:5:"bl_id";i:0;s:9:"formating";a:2:{s:4:"full";a:3:{s:6:"format";s:5:"@kn_h";s:4:"type";s:2:"bo";s:5:"value";s:1284:"</><b> 32.97<br> Р 125<br> </b> <b> Рабин, Д. М.</b> <b></b> <br><dd> Музыка и компьютер. Настольная студия. Природа звука. MIDI-системы. Цифровое аудио. Секвенсоры. Оркестровка. Ансамблирование [Текст] : переводное издание / Д. М. Рабин ; пер. с англ. Р. Н. Онищенко, А. Э. Лашковского. - Минск : Попурри, 1998. - 271 с. : ил + 1 эл. опт. диск (CD-ROM). - (Digital Pro : от начинающих до специалистов). - Алф. указ.: с. 268 - 271. - <b>ISBN </b>985-438-199-4. - <b>ISBN </b>0-07-881209-7 : 102.00 р.<br><table width="100%"><tr> <td><b> ГРНТИ </b><td width="50%"><b> ББК </b></td> </tr><tr> <td>50.41.25<td width="50%">32.97</td> </tr><tr> <td>18.41<td width="50%"></td> </tr></table><br><b> Кл.слова (ненормированные): </b> midi -- секвенсоры -- компьютерная музыка -- звук -- обработка звука<br><b> Доп. точки доступа: </b> <br>Онищенко, Р. Н.А. Э.экземпляры в отделах: </b> всего 1 : М (1)<br> Свободны: М (1)<br><br> ";}s:5:"brief";a:3:{s:6:"format";s:6:"@brief";s:4:"type";s:2:"bo";s:5:"value";s:374:"Рабин Д. М. Музыка и компьютер. Настольная студия. Природа звука. MIDI-системы. Цифровое аудио. Секвенсоры. Оркестровка. Ансамблирование [Текст] / Д. М. Рабин ; пер. с англ. Р. Н. Онищенко, А. Э. Лашковского, 1998. - 271 с.";}}s:7:"Content";a:27:{i:101;a:1:{i:0;s:3:"rus";}i:919;a:1:{i:0;s:11:"^Arus^N0102";}i:920;a:1:{i:0;s:4:"PAZK";}i:999;a:1:{i:0;s:7:"0000000";}i:700;a:1:{i:0;s:21:"^AРабин^BД. М.";}i:210;a:1:{i:0;s:34:"^D1998^CПопурри^AМинск";}i:10;a:2:{i:0;s:23:"^A985-438-199-4^D102.00";i:1;s:15:"^A0-07-881209-7";}i:320;a:1:{i:0;s:32:"Алф. указ.: с. 268 - 271";}i:621;a:1:{i:0;s:5:"32.97";}i:60;a:1:{i:0;s:1:"2";}i:903;a:1:{i:0;s:18:"32.97/Р125-708802";}i:964;a:2:{i:0;s:8:"50.41.25";i:1;s:5:"18.41";}i:906;a:1:{i:0;s:5:"32.97";}i:215;a:1:{i:0;s:51:"^A271^Ca-ил^E1^2o=эл. опт. диск (CD-ROM)";}i:225;a:1:{i:0;s:70:"^ADigital Pro^EОт начинающих до специалистов";}i:3000;a:2:{i:0;s:14:"^c454^222^3581";i:1;s:14:"^c454^222^3581";}i:3600;a:1:{i:0;s:12:"МТСиБЖ";}i:102;a:1:{i:0;s:2:"BY";}i:3011;a:1:{i:0;s:15:"^A0^B442277^DМ";}i:3013;a:1:{i:0;s:5:"32.97";}i:702;a:2:{i:0;s:40:"^4730 Пер.^AОнищенко^BР. Н.";i:1;s:44:"^4730 Пер.^AЛашковский^BА. Э.";}i:900;a:1:{i:0;s:19:"^Ta^B05^C454^2j9^Xk";}i:907;a:9:{i:0;s:24:"^CКР^A20040316^BБЕП";i:1;s:22:"^CС^A20041111^BЗЕА";i:2;s:24:"^CКР^A20050131^BСКЕ";i:3;s:24:"^CКР^A20050310^BБЕП";i:4;s:24:"^CКТ^A20050425^BСРБ";i:5;s:21:"^CC^A20051209^BЗЕА";i:6;s:24:"^CРК^A20060608^BШЕК";i:7;s:24:"^CКР^A20100405^BПРК";i:8;s:24:"^CКР^A20101004^BАОА";}i:200;a:1:{i:0;s:326:"^AМузыка и компьютер. Настольная студия. Природа звука. MIDI-системы. Цифровое аудио. Секвенсоры. Оркестровка. Ансамблирование^FД. М. Рабин ; пер. с англ. Р. Н. Онищенко, А. Э. Лашковского";}i:610;a:5:{i:0;s:4:"MIDI";i:1;s:20:"СЕКВЕНСОРЫ";i:2;s:37:"КОМПЬЮТЕРНАЯ МУЗЫКА";i:3;s:8:"ЗВУК";i:4;s:29:"ОБРАБОТКА ЗВУКА";}i:908;a:1:{i:0;s:6:"Р 125";}i:910;a:1:{i:0;s:45:"^A0^B442277^C19980314^DМ^H00042578^U2003/170";}}s:3:"mfn";i:0;s:6:"Status";i:0;s:10:"RecVersion";i:0;}}');		


/*$profile=array(
'full'=>array('format'=>'@kn_h','type'=>'bo'),
'ozboz'=>array('format'=>'@ozboz','type'=>'bo'),
);


$records=$irb->reformat_records($records,$profile);

print_r($records);
*/




/*include('../jirbis_configuration.php');
ilog::$debug_enable=true;
ilog::$debug_log_enable=true;
ilog::$log_permanent_writing=true;
ilog::$display_text=true;
$params=$CFG;

$irb=new jwrapper(true,0);
print_r($irb->GetTermList('IBIS','!',1,'','','',0));
*/
/*
$profile=array(
'full'=>array('format'=>'@kn_h','type'=>'bo'),
'brief'=>array('format'=>'@brief','type'=>'bo'),
);


$records=$irb->find_jrecords('IBIS','<.>V=$<.>','',1,3,$profile);



//print_r($records);

$profile=array(
'full'=>array('format'=>'@kn_h','type'=>'bo'),
);

echo serialize($records);

$records=array_merge($records,$records=$irb->find_jrecords('NWPIL','<.>V=$<.>','',4,3,$profile));


$profile=array(
'full'=>array('format'=>'@kn_h','type'=>'bo'),
'brief'=>array('format'=>'@brief','type'=>'bo'),
'ozboz'=>array('format'=>'@ozboz','type'=>'bo'),
);*/

//print_r($records);



/*
print_r($irb->find_jrecords('NWPIL','<.>V=$<.>','',1,2,$profile));
echo $irb->last_req_full_count;
echo $irb->get_last_error_message();
*/

?>