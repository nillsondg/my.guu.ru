﻿<?php
class ji_upload{
	
	public $rec;
	public $file;
	public $ed_path;
	public $base;
	public $mfn;
	public $rec_id;
	private  $name;
	private  $extension;
	private  $relative_file_path;
	
	public function __construct($rec,$file,$ed_path,$base,$mfn,$rec_id=''){
		
		$this->rec=$rec;	
		$this->file=$file;	
		$this->ed_path=$ed_path;
		$this->base=$base;
		$this->mfn=$mfn;
		$this->rec_id=$rec_id;		
		
		
	/*	if (!is_uploaded_file($this->file['tmp_name']))  
			throw new Exception('Файл не был загружен',1);*/
		
		if(($parts=u::get_filename_parts($file['name']))<0) 
			throw new Exception('Не удаётся выделить расширение загруженного файла',2);
		
		$this->extension=$parts['extension'];
		$this->name=$parts['name'];		
	}
	public static function check_access(){
		
	}
	public function check($ext_string){		
		//if (!preg_match('/'.str_replace(',','|',$ext_string).'/i',$this->extension)) 
		if (strpos($ext_string,"*.$this->extension")===false && $ext_string!=='*.*')
			throw new Exception('Недопустимое расширение файла.',4);		
	}	
	
	public function set_file_path(){
		$code=($this->rec->GetField(903,1) ? $this->rec->GetField(903,1) : ($this->rec->GetField(952,1) ? $this->rec->GetField(952,1) : $this->rec->GetHash()) );
		$code_hash=md5($code);
		$dir_name1=substr($code_hash,0,4);
		$dir_name2=substr($code_hash,4,8);
		$this->relative_file_path="$dir_name1/$dir_name2/".u::to_translit($this->name).'.'.$this->extension;		
	}

	public function build_dirs_file_move(){
		if (!u::make_dirs_of_path("{$this->ed_path}/{$this->relative_file_path}/",$this->ed_path))
			throw new Exception('Не удаётся переместить файл',10);		 
			//move_uploaded_file
		 if (!@copy($this->file['tmp_name'],"{$this->ed_path}/{$this->relative_file_path}"))  
		 	throw new Exception('Не удаётся переместить файл',5);		 
	}
	
	
	public function update_rec(){
		if (($file_occ=$this->rec->SearchFieldOcc(951,$this->relative_file_path,'A',false))!==false){
			$this->rec->RemoveField(951,$file_occ);
		}
		// Здесь ещё нужно добавить права доступа и 
		$this->rec->AddField(951,"^A".$this->relative_file_path);								
	}
	
	public function update_cached_rec($rec){
		global $__db;
		$out=new output();
		$rec_cache=$out->get_rec_by_rec_id($this->rec_id);
		if (!$rec_cache) 	
			throw new Exception("Ошибка излечения записи с $rec_id",40);		
		$rec_cache->SetContent($rec->GetContent());
		$sql="
		UPDATE #__rec
		SET record=".idb::i()->Quote(serialize($rec_cache))."
		WHERE rec_id=$this->rec_id
		"; 
		idb::i()->setQuery($sql);
		idb::i()->Query();
		$sqlrez = idb::i()->getErrorNum();
		if ($sqlrez <> 0 && $sqlrez <> 1007) 
			throw new Exception('Ошибка обновления кэша записи в MySQL: '.idb::i()->getErrorMsg(),idb::i()->getErrorNum());		 
	}
	
	public static function show_form($bl_id,$rec_id,$mfn){		
		global $CFG;
		
		$st=new ji_st(JI_LOCK_NO);
		$bns=$st->get_bns_by_bl_id($bl_id);
		
		?>
			<script type="text/javascript">	
			
				jQuery(function($){
						$('#file_upload').uploadify({
							'formData'     : {
								'task'     : 'upload_file',	
								'timestamp' : '<?php echo time();?>',
								'token'     : '<?php echo md5($CFG['irb64_password'] . time());?>',
								'bl_id'     : '<?php echo $bl_id ?>',
								'rec_id'     : '<?php echo $rec_id ?>',					
								'mfn'     : '<?php echo $mfn ?>',
								'bns'     : '<?php echo $bns ?>'
								
							},
							'fileObjName':'files',				
							'swf'      : '<?php echo JI_PATH_IMAGES_NET.'/uploadify.swf' ?>',
							'uploader' : '<?php echo JI_PATH_COMPONENT_NET.'/'.JI_FILE_AJAX_PROVIDER ?>',
							'cancelImg'      : '<?php echo JI_PATH_IMAGES_NET.'/uploadify-cancel.png' ?>',
							'multi'          : true,
							'fileTypeExts' : '<?php echo $CFG['ed_avalable_extensions'] ?>',
							'SizeLimit': '<?php echo $CFG['ed_upload_file_limit'] ?>',
							'buttonText':'Нажмите для выбора файлов...',				
							'debug'    : false,
							'width'    : 350,
							//'removeTimeout':20,
							'removeCompleted':false,
							//Здесь может быть функция закрывания окна... 
							 'onUploadSuccess' : function(file, data, response){
								errors.init($("#upload_results"));				 	
							 	
								
							 	if (!response)
									errors.log(file.name +" - ошибка при загрузке",999);
								
								if (data.length>1)
									errors.log(file.name +" - "+data,999);
							}  
						});
					}); 
			//'HideButton':true,
			//'button_width': 300
			</script>

			<table class='model'>
			
				<tr>
					<th id='upload_recomendation'>
					ВЫБЕРИТЕ ФАЙЛЫ ДЛЯ ПОДКЛЮЧЕНИЯ
					</th>
				</tr>
<!--				<tr>
					<td>
						<fieldset>
							<legend>Доступ для категорий</legend>								
							<input name="rp_elements[]" value="document_form" checked="" type="checkbox">&nbsp;<label for="rp_elements[]">Не зарегистрированный</label><br>
							<input name="rp_elements[]" value="udk_bbk" type="checkbox">&nbsp;<label for="rp_elements[]">Студент</label><br>
							<input name="rp_elements[]" value="rubrics" type="checkbox">&nbsp;<label for="rp_elements[]">Преподаватель</label><br>
							<input name="rp_elements[]" value="keywords" type="checkbox">&nbsp;<label for="rp_elements[]">Сотрудник</label>
						</fieldset>						
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
							<legend>Доступ только в локальной сети</legend>								
								<label for="print_output">Нет</label>
							   <input checked="" name="print_output" value="word" type="radio">
							   <label for="print_output">Да</label>
							   <input name="print_output" value="printer" type="radio">							
						</fieldset>						
					</td>
				</tr>-->

				<tr>
					<td class="upload_for_cell">
						<form>
							<div id="queue"></div>
							<input id="file_upload" name="file_upload" type="file" multiple="true">		
						</form>
					</td>
				</tr>
				<tr>
					<td id="upload_results">
					</td>
				</tr>
				
			</table>
			
			<?php

	}
	
	public static function check_upload_rights($st){
		global $CFG;
		
		if (!$st->user)
			return false;
			
		if ($st->user->GetField(50,1) && strpos($CFG['ed_manipulators_categories'],$st->user->GetField(50,1))!==false)
			return true;	
		
	}
	
}
