<?php


/*============================== ШАГ 2 УДАЛИТЬ ИЗ ЭТОГО ФАЙЛА, ТАК КАК ЭТО ЧАСТЬ СЕРВЕРНОЙ ПОДСИСТЕМЫ!!!!!!!!!!!!!!==========================================================================================*/
require_once( 'jirbis_configuration.php' );
require_once( 'jirbis_link_libraries.php' );
require_once( JI_PATH_MODES_LOCAL.'/install/ji_install.php' );
require_once( JI_PATH_MODES_LOCAL.'/install/ji_install_show.php' );


ini_set('error_log',JI_PATH_DEBUG_LOCAL.'/'.JI_FILE_PHP_ERRORS_LOG);
//Переключить на OFF в рабочем режиме! 
ini_set('display_errors','On');
ini_set('max_execution_time','100');
ji_ilog::i()->debug(true);


$task=mosGetParam( $_REQUEST, 'task', '' );

$url=mosGetParam( $_REQUEST, 'url', '' );
$port=mosGetParam( $_REQUEST, 'port', 0 );

$login=mosGetParam( $_REQUEST, 'irb64_user', '' );
$password=mosGetParam( $_REQUEST, 'irb64_password', '' );

//Берём значения параметров из предыдущей формы
$shot_name=mosGetParam( $_REQUEST, 'shot_name', '' );
$full_name=mosGetParam( $_REQUEST, 'full_name', '' );
$email=mosGetParam( $_REQUEST, 'email', '' );


$ip_local=mosGetParam( $_REQUEST, 'ip_local', '' );
$ip_external=ji_install::get_client_ip();

$jirbis_dir=mosGetParam( $_REQUEST, 'jirbis_dir', '' );

$user_id=mosGetParam( $_REQUEST, 'id', '' );


$skip_registration=mosGetParam( $_REQUEST, 'skip_registration', '' );

//$db= new database( $CFG['mysql_host'], $CFG['mysql_user'], $CFG['mysql_password'], $CFG['mysql_database'], $CFG['mysql_database_prefix'] ,false);
idb::i()->_debug=false;

$answer=array();
$answer['success']=true;



try{
	switch($task){
		case  'users_update' : 
		$users_array_server=ji_distrib::get_users_array();
		$users_array_home=@unserialize(@urldecode(mosGetParam( $_REQUEST, 'serialized_users', '' )));
		ilog::arr('users_array',$users_array_home);
		if (!is_array($users_array_home) or count($users_array_home)<1)
			ji_ilog::i()->w("Данные пользователей из дома не корректны (не являются массивом)!",I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);		

		$users_array=ji_distrib::update_users_array($users_array_server,$users_array_home);			
		ji_distrib::set_users_array($users_array);
		answer::text(urlencode(serialize($users_array)));
		break;

				
		default : 
			if (!$skip_registration){
				// Если используется не joomla, а интегрированный вариант и указан прямой адрес, то ограничиваемся только удалением протокола 
				if (preg_match('{http://([^\?]+'.JI_FILE_AJAX_PROVIDER.')}si',$url,$p)){
					$url=$p[1];
				}elseif(!preg_match('{http://([^\?]+?)(?=/\w+\.\w+|$)}si',$url,$p)){
					// Для адреса типа http://vlibrarynew.gpntb.ru/2011-12-09-19-27-41.html берём только имя сервера, без Index.php или названия страцниц 2011-12-09-19-27-41.html
						ji_ilog::i()->w("Указанный адрес сервера ($url) не соответствует формату.Убедитесь, что он имеет вид: http://libserver.ru",I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);
				}
					
				$url=$p[1].'/'.JI_DIR_COMPONENT.'/'.JI_FILE_AJAX_PROVIDER;
					
				$res=ji_update::post($url,$port,$CFG['default_timeout'],array('task'=>'confirm_update_adress'));	
				if (trim($res)!=='TRUE')
					ji_ilog::i()->w("От указанного сервера получен некорректный ответ",I_ERROR,I_ERROR_LEVEL_CRITIICALY,0);
			}			
			
			if (!$user_id)
				break;
			$users_array=ji_distrib::get_users_array();
			//$id может быть пустым в старых системах, которые будут пытаться ре
			
			if (!isset($users_array[$user_id]))
				$users_array[$user_id]=array();
				
			$users_string_new=array(
					'id'=>$user_id,
					'url'=>$url,
					'port'=>$port,
					'login'=>$login,
					'password'=>$password,
					'full_name'=>$full_name,
					'shot_name'=>$shot_name,
					'email'=>$email,
					'ip_local'=>$ip_local,
					'ip_external'=>$ip_external,
					'jirbis_dir'=>$jirbis_dir,
					'install_date'=>time()
			);
			
			$users_array[$user_id]=array_merge($users_array[$user_id],$users_string_new);
			
					
			//if(!ji_install::add_sql_data('#__jusers',$server_req_array,true))
				if (($error=ji_distrib::set_users_array($users_array))<0)
					ji_ilog::i()->w('Ошибка добавлении в CSV файл пользователей J-ИРБИС 2.0. ',I_ERROR,I_ERROR_LEVEL_CRITIICALY,$error);
		break;			
	}	
}catch (Exception $e){
	ji_ilog::i()->w('Критическая ошибка: '.$e->getMessage(),I_ERROR,0,$e->getCode());
	$answer['success']=false;
}	


//$answer['errors_data']=errors::is_errors() ? ji_install_show::errors_output(errors::get_as_messages_array()) : array();
$answer['errors']=errors::is_errors() ? errors::get_objects_array() : array();

answer::jsone($answer);
?>