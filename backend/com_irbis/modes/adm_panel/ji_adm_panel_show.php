<?php




class ji_adm_panel_show{
		
	public  static function js_css(){
		?>
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/<?php echo u::ga($GLOBALS['CFG'],'adm_panel_theme','smoothness')?>/jquery-ui.min.css" rel="stylesheet" />		
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery-ui-combobox.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/ui.jqgrid.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jirbis_admin.css" rel="stylesheet" />
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.form.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/i18n/grid.locale-ru.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.jqGrid.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis_utils.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis_admin.js" type="text/javascript"></script>		
		<?php	
	}	

	
	public  static function js_settings($st){	
		?>		
		<script type="text/javascript">		
			var st=<?php echo json_encode($st) ?>;
		</script>
		<?php
	}	
	
	
	public static function template($template_path,$data){
		
		if (!($content=@file_get_contents($template_path))){
			echo 'Не найден файл шаблона по адресу: '.$template_path;
			return;
		}	
		
		$content=explode('<!-- DELIMITER -->',$content);
		
		if (!is_array($content) or count($content)!=3) {
			echo  'Неправильная разметка страницы! Код шаблона не обрамлён двумя разделителями <!-- DELIMITER --> !';
			return ;
		}		
		
		eval( '?>'.$content[1]);
	}	
		
	
	public static function error_messages_html(){
		if (($messages_array=errors::get_as_messages_array())){
			foreach($messages_array as $msg)
				echo "<span class='error_message_table'>$msg</span>";
		}
	}
	
	public static function error_messages_json(){
		answer::jsone(array('errors'=>errors::get_objects_array()));
	}
		
}

?>