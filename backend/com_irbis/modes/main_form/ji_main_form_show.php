<?
class ji_main_form_show{
	public  static function show_js_css(){
		?>
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/<?php echo u::ga($GLOBALS['CFG'],'frontend_theme','smoothness')?>/jquery-ui.min.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery.treeview.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/pagination.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/uploadify.css"  rel="stylesheet"  >
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery.rating.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jquery-ui-combobox.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/ui.jqgrid.css" rel="stylesheet" />
		<link type="text/css" href="<?php echo JI_PATH_CSS_NET ?>/jirbis.css" rel="stylesheet" />
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.timers.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.hotkeys.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.cookie.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.jstree.min.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.pagination.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.form.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/crc32.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.uploadify.min.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.rating.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery-ui-combobox.js" type="text/javascript"></script>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/i18n/grid.locale-ru.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jquery.jqGrid.min.js" type="text/javascript"></script>
	    <script src="<?php echo JI_PATH_JS_NET ?>/form-transfer.js" type="text/javascript"></script>
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis_utils.js" type="text/javascript"></script>
		
		
		<?php
		if (ji_st::i()->joomla_present && ji_st::i()->books_comments_enable){
				$style=JFactory::getDocument();
				// Более корректная форма для Joomla -- $this->baseurl
	 			$style->addScript(JI_PATH_CMS_NET . '/components/com_jcomments/js/jcomments-v2.3.js?v=8');
	 			$style->addScript(JI_PATH_CMS_NET . '/components/com_jcomments/libraries/joomlatune/ajax.js?v=4');
				$style->addStyleSheet(JI_PATH_CMS_NET . '/components/com_jcomments/tpl/default/style.css');		
		}

		self::get_js_settings();
		?>
		
		<script src="<?php echo JI_PATH_JS_NET ?>/jirbis.js" type="text/javascript"></script>	
		<?php	
	}
	
	
	public static function show_main_form(){ 
		?>
		<div id="dialog"></div>
		<table class="main" border="0">
			<!-- Заголовок --> 
			<tr>
				<td id="header_cell" <?php if (!ji_st::i()->header) echo 'style="display:none"' ?> >
				<?php  echo ji_st::i()->header ?>
				</td>
			</tr>
			<!-- Поисковая форма --> 
			<tr>
				<td id="form_cell"  >
				<div id="form_accordion" >
					<h3>
						<a href="#"> 
						<?php  echo ji_st::i()->search_mode_description; ?>				
						</a>
					</h3>
					<div id="form_div" >
					</div>
				</div>				
				</td>
			</tr>	

			<!-- Настройки пользователя --> 
			<tr>
				<td id="user_settings_cell" <?php if (!ji_st::i()->user_settings_enable) echo 'style="display:none"' ?> >
					<div id="user_settings_accordion" class="accordion">
						<h3><a href="#"> 
							Настройки отображения записей		
						</a></h3>
						<form id="set_selected_user_profile">
						<div class="form_div">					
							<fieldset>
								<legend>Показывать элементы</legend>	
							<div class="elements_to_show">
							<?php 
	
								 foreach(ji_st::i()->rp as $key=>$value){							 	
								 	//echo "<div class=\"showing_element\">";

								 	if ($value['type']==2) continue;
								 	if (array_search($key,ji_st::i()->bo_types_names)!==false) continue;

								 	echo '<label class="check_box_cell"><input name="rp_elements[]" value="'.$key.'" '.($value['type']==0 ? 'checked=""' : '').' type="checkbox">';
								 	echo '&nbsp;'.$value['title'].'</label>';
								 //echo "</div>";
								 }
	
							?>
							<div class="clear"></div>
							</div>						
							</fieldset>	
						
							<table class="model">
								<tr>
									<td class="formats_cell"   <?php if (!ji_st::i()->formats_chose) echo 'style="visibility:hidden"' ?> >
									Формат показа:
									<select class="format_select" name="profile_name">									
									<?php 
										if (ji_st::i()->rec_view_profiles){
										 foreach(ji_st::i()->rec_view_profiles as $key=>$value)
											 
											 	echo "<option ".(ji_st::i()->profile_name===$key ? "default=''" : '')." value=\"{$key}\">{$value['description']}</option>\n";
										}
									?>
									</select>
									
									</td>
									<td class="sort_cell"  <?php if (!ji_st::i()->sort_chose) echo 'style="visibility:hidden"' ?> >
									Сортировка: 
									<select class="sort_select" name="sort_name">
									<option value="">Нет сортировки</option>;
									<?php 
										// Все значения модифицируются только вручную. 
											if (ji_st::i()->sort_types){
												 foreach(ji_st::i()->sort_types as $key=>$value)
												 	echo "<option ".(ji_st::i()->sort_name===$key ? "default=''" : '')."value=\"{$key}\">{$value['description']}</option>\n";
										}
									?>
									</select>
									</td>
			
								</tr>
							</table>

						
						</form>
						</div>
					</div>				
					

				</td>
			</tr>	

			<!-- Индикатор прогресса --> 				
			<tr>
				<td id="progress_bar"   <?php if (!ji_st::i()->progress_bar_enable) echo 'style="display:none"' ?> >
					<table class="model" width="100%">
						<tr>	
							<td width="30">
								<div id="loading"></div>									
							</td>						
							<td>
								<div id="progress"></div>									
							</td>
							<td width="30">
								<div id="amount">0%</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>	

			<!-- Сортировка и выбор формата показа --> 				
			<tr>
				<td id="sort_and_formats" <?php if (!ji_st::i()->sort_chose && !ji_st::i()->formats_chose) echo 'style="display:none"' ?> >
				
				</td>
			</tr>	

			<!-- Расшифровка запроса --> 
			<tr>
				<td id="req_description_cell" <?php if (!ji_st::i()->req_description_enable) echo 'style="display:none"' ?> >
					<!-- формируется JS --> 		
				</td>
			</tr>	

			<!-- Сообщения об ошибках подключения --> 
			<tr>
				<td id="errors_cell"  <?php if (!@ji_st::i()->errors_reporting_enable) echo 'style="display:none"' ?> >
					<!-- формируется JS --> 				
				</td>
			</tr>	
			<!-- Отладочная информация --> 
			<tr>
				<td id="debug_cell" <?php if (!@ji_st::i()->debug_reporting) echo 'style="display:none"'; ?> >
					
						<table class="model">
						<tr>
							<td width="70%">
								<iframe id="debug_log" src="javascript:void(0)" style="width:100%;height:400px;"></iframe>
							</td>
							<td>
								<div  id="debug_messages"></div>
							</td>
						</tr>
						</table>						
						<input type="button" class="button" id="clean_cache" value="Очистить кэш">
						<input type="button" class="button" id="clean_session" value="Очистить сессию">
						<input type="button" class="button" id="clean_js_session" value="Обнулить JS сессию">
						<input type="button" class="button" id="show_session" value="Открыть сессию">
						<input type="button" class="button" id="show_log" value="Открыть протокол">
					<!-- формируется JS --> 				
				</td>
			</tr>	

			<tr>
				<td id="bo_list_cell">
				
				    <div id="fog_box">
                        <div id="show_results">
                        </div>
                    </div>
					<!-- формируется JS --> 				
				</td>
			</tr>	
			
			<!-- Показ записей --> 
			<tr>
				<td class="jpagination">
				<div id="paginator">
				</div>
					<!-- формируется JS -->  
				</td>
			</tr>
			<!-- Печать записей --> 
			<tr>
				<td id="print_cell" <?php if (!ji_st::i()->print_enable) echo 'style="display:none"' ?> >
				<div id="print_accordion"  class="accordion">
					<h3><a href="#"> 
						Печать записей				
					</a></h3>
					<div id="print_div">
						<form id="print_form">
						
						
							<!-- Индикатор прогресса --> 				
							
							<table class="model" id="progress_bar_print_table">
								<tr>	
									<td>
										<div id="progress_print"></div>									
									</td>
									<td width="30">
										<div id="amount_print">0%</div>
									</td>
								</tr>
							</table>

						
							<table class="model" border="0">
								<tr>
									<td>
								   
									<div class="float_field">
								   Распечатывать:<br>
									<select class="print_select" id="print_conditions" name="print_conditions">
										<option value="all" selected="">все найденные</option>
										<option value="checked">отмеченные</option>     
									</select>  
								   </div>
								   									
									<?php if (ji_st::i()->print_format_enable) { ?>	
										<div class="float_field">								
											Вид записей:<br>
										   <select class="print_select" id="print_format" name="profile_name">							
											<option value="" selected="">текущий профиль</option>		
											<?php	
										   	if (ji_st::i()->rec_view_profiles){
												 foreach(ji_st::i()->rec_view_profiles as $key=>$value)
													 
													 	echo "<option ".(ji_st::i()->profile_name===$key ? "default=''" : '')." value=\"{$key}\">{$value['description']}</option>\n";
												}
											?>	
											</select>  
										</div> 
								  	<?php }?>
								  
									

								  
								   <?php if (ji_st::i()->print_sort_enabe) { ?>
										<div class="float_field">									  
											Сортировка: <br>
											<select class="sort_select"  name="sort_name">
											<option value="">Нет сортировки</option>;
											<?php 
												// Все значения модифицируются только вручную. 
													if (ji_st::i()->sort_types){
														 foreach(ji_st::i()->sort_types as $key=>$value)
														 	echo "<option ".(ji_st::i()->sort_name===$key ? "default=''" : '')."value=\"{$key}\">{$value['description']}</option>\n";
												}
											?>
											</select>
										</div>
									<?php }?>
								  
									<?php if (ji_st::i()->print_titles_enable) { ?>
											  <div class="float_field">
													Заголовки: <br>
													<select class="print_titles" name="print_titles">
													<option value="">Нет заголовков</option>
													<option value="disc">Дисциплины</option>
													<option value="spec_disc">Специальности и дисциплины</option>
													</select>
											</div>
									<?php }?>
								   </td>
		   						   
								   <td width="250">
								   
									<fieldset>
										<legend>Формат печати</legend>	
										   <label for="print_output">MS Word</label>
										   <input type="radio" checked="" name="print_output" value="word">
										   <label for="print_output">Принтер</label>
										   <input type="radio"  name="print_output" value="printer">
									<?php if (ji_st::i()->print_send_email_enable) { ?>
   										   <label for="print_output">E-Mail</label>
										   <input type="radio" name="print_output" value="email">
									<?php }?>	   
									</fieldset>
									
									<div id="email_div" style="display:none">
										E-Mail: <input type="text" name="email" id="email" size="30">
									</div>
									
		   						   </td>
									
								   <td>
								   <input type="button" value="Печать" id="print_button" onclick="">
								   </td>
								 </tr>
								 <tr>
								 	<td>
										
								 </tr>
								</table>
						</form>
					</div>
				</div>				
			
				</td>
			</tr>	
		</table>		
	<?php	
	}
	
	public  static function get_js_settings(){	
		//global $CFG;
		
		?>
		<script type="text/javascript">
		
		var st={	
			"self_net_path":"<?php echo JI_FILE_PATH_AUTO_REQUESTS_NET_GLOBAL_PROVIDER ?>",
			"images_net_path":"<?php echo JI_PATH_IMAGES_NET ?>",
			"js_net_path":"<?php echo JI_PATH_JS_NET ?>",
			"form_mode":"main",			
			"search_mode":"<?php echo ji_st::i()->search_mode ?>",			
			"min_req_length":<?php echo ji_st::i()->min_req_length ?>,
			"keyup_delay":<?php echo ji_st::i()->keyup_delay ?>,
			"max_broad_live_time":<?php echo ji_st::i()->max_broad_live_time ?>,
			"max_result_for_printing":<?php echo ji_st::i()->max_result_for_printing ?>,
			"debug":<?php echo (int)ji_st::i()->debug_reporting ?>,
			"permanent_search":<?php echo ji_st::i()->permanent_search ?>,
			"reduce_afrer_search":<?php echo (int)ji_st::i()->forms_profile[ji_st::i()->search_mode]['reduce_afrer_search'] ?>,
			"req_form":<?php echo (int)ji_st::i()->forms_profile[ji_st::i()->search_mode]['reduce_afrer_search'] ?>,
			"max_sorted_records":<?php echo ji_st::i()->max_sorted_records ?>,
			"search_fali_link":"<?php echo ji_st::i()->search_fali_link ?>",
			"ji_path_cms_net":"<?php echo JI_PATH_CMS_NET ?>"
			
		};
		
		var def={
			"permanent_output":<?php echo (int)ji_st::i()->permanent_output ?>,		
			"recs_total":0,
			"portion_output":<?php echo ji_st::i()->portion_output ?>,
			"portion":<?php echo ji_st::i()->portion ?>,
			"first_number":1,
			"reqs_outputed":0,
			"recs_outputed":0,
			"last_output_time":0,
			"req":"",
			"finish_flag":"begin",
			"req_params":"",
			"req_id_client":"",
			"selected_search_flag":0,
			
			"task_broad":"search_broadcast",
			"task_renew":"show_results",
			"callback_finish_function":"search.renew_results",
			"callback_renew_function_name":"base.standart_output",
			"callback_renew_finish_function_name":"base.standart_finish"
		};
		
		</script>
		<?php
	}	
}

?>