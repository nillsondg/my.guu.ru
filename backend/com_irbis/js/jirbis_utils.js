jQuery.noConflict();

jQuery(function ($) {

/*$.browser = {};
$.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
$.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
$.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
$.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());*/
	
	errors = {
		self:this,
		_log_div:null,		
		init:function(log_div){
			//alert(log_div);
			self._log_div=(typeof log_div!="undefined") ? log_div : null ;
		},
		
        hi_output: function (errors,req_name) {
        	var req_name=(typeof req_name!="undefined") ? ' в запросе '+ req_name : '' ;
        	
            if (errors) {
                for (i = 0; i < errors.length; i++)
                    this.log("Ошибка"+req_name+": " + errors[i].error_message, errors[i].error_number);
            }
        },

        lo_output: function (xhr,req_name) {
        	var req_name=(typeof req_name!="undefined") ? ' в запросе '+ req_name : '' ;
        	if (xhr.statusText == 'abort') return;
            if (!xhr) {
                errors.log("Не опознанные низкоуровневые проблемы соединения с сервером "+req_name+" при AJAX. Возможно, сервер не доступен.", 0);
            } else {
                if (!xhr.statusText)
                    errors.log("Отсутствует значение xhr.data. Возможно, проблемы с доступностью сервера "+req_name+".", xhr.status);
                if (xhr.statusText == 'OK')
                    errors.log("Ошибка интерпретации JSON. Возможно, непредвиденные ошибки при выполнении запроса "+req_name+".", xhr.status);
                else {
                	if (xhr.statusText=='error')
                		errors.log("Ошибка AJAX при выполнении запроса: Ошибка связи с сервером или другая неопознанная ошибка "+req_name+".",0);
                		else 
                		errors.log("Ошибка AJAX при выполнении запроса"+req_name+": " + xhr.statusText,xhr.status);
                }
                    
            }
        },

        log: function (error_message, error_number) {
        	if (!self._log_div) 
        		self._log_div=$("#errors_cell");
        		
            self._log_div.html(_log_div.html() + "<p><span class=\"ui-state-error ui-corner-all\"><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span> " + error_message + "(" + error_number + ")</span></p>");
        },


        message: function (message) {
        	if (!self._log_div) 
        		self._log_div=$("#errors_cell");
        		
            self._log_div.html(_log_div.html() + "<p><span class=\"ui-state-default ui-corner-all\"><span class=\"ui-icon ui-icon-notice\" style=\"float: left; margin-right: .3em;\"></span> " + message + "</span></p>");
        },        
        clean: function () {        	
        	if (!self._log_div) 
        		self._log_div=$("#errors_cell");
        	
            self._log_div.html('');
        }
    };
    
    my_dialog = {
        loading: function () {
            $("#dialog").dialog({
                width: 200,
                height: 200,
                title: 'Загрузка....'
            });

            $("#dialog").html("<img src='" + st.images_net_path + "/loading_green.gif' border=0><br><br>Загрузка данных...");
        },
        show: function (msg) {
            $("#dialog").dialog({
                width: 300,
                height: 250,
                title: 'Информация',
                resizable: true,
                buttons: {
                    "OK": function () {
                        $(this).dialog("close");
                    }
                }
            });

            $("#dialog").html(msg);
        },
        destroy: function () {
            $("#dialog").dialog("destroy").empty();
        }
    };

            
});

