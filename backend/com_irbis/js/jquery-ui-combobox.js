(function( $ ) {
    $.widget( "ui.combobox", {
        _create: function() {

            if(!this.element.is("select")) return alert("Вместо SELECT, инициализирован другой элемент");

            this.wrapper = $( "<span>" )
                .addClass( "custom-combobox" )
                .insertAfter( this.element );
            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();

            return true;
        },

        _createAutocomplete: function() {
            var selected = this.element.children( ":selected"),
                value = selected.val() ? selected.text() : "";

            this.input = $( "<input type='text'>" );

            this.input.appendTo( this.wrapper)
                .val( value )
                .attr( "title", "" )
                .removeAttr("class")
                .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                .autocomplete({
                    delay: 0,
                    minLength: 0,
                    source: $.proxy( this, "_source" ),
                    combocache: {}
                })
                .tooltip({
                    tooltipClass: "ui-state-highlight"
                });

            //this.options.source && (this.options.cache = {});

            !this.input.attr("id") && this.input.attr("id", "ui-" + Math.random());

            var width = this.element.get(0).style.width;
            this.input.removeAttr("style").css("width", width ? width : this.element.attr("data-combobox-width"));


            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option && (ui.item.option.selected = true);
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });
                },

                autocompletechange: "_removeIfInvalid"
            });

            if(this.options.preload && this.options.source){
                var self = this;

                $.getJSON( this.options.source, function( data, status, xhr ) {

                    if(data.length && typeof data[0] == "object"){

                        if(!self._initAttr) self.input.attr("id", self.element.attr("id") ? "ui-" + self.element.attr("id") : "").attr("name", self.element.attr("name") ? "ui-" + self.element.attr("name") : "");

                        self.element.empty();

                        $.each(data, function(){
                            self.addOption(this.description, this.value, self._initDefaultValue ? false : this.selected);
                        });
                        self._initDefaultValue = true;


                    }else{
                        if(!self._initAttr) {
                            self.input.attr("id",self.element.attr("id") || "").attr("name",self.element.attr("name") || "");
                            self.element.attr("id", self.element.attr("id") ? "ui-" + self.element.attr("id") : "").attr("name", self.element.attr("name") ? "ui-" + self.element.attr("name") : "");
                        }
                        self.addOption("", "", true);
                        $.each(data, function(){
                            self.addOption(this, this, false);
                        });
                    }
                    self._initAttr = true;

                    self.options.onPreload && self.options.onPreload(self.element, self.input, data);

                });
            }
        },

        _createShowAllButton: function() {
            var input = this.input,
                wasOpen = false;

            $( "<a>" )
                .attr( "tabIndex", -1 )
                .attr( "title", "Показать весь список" )
                .tooltip()
                .appendTo( this.wrapper )
                .button({
                    icons: {
                        primary: "ui-icon-triangle-1-s"
                    },
                    text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "custom-combobox-toggle ui-corner-right" )
                .mousedown(function() {
                    wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                })
                .click(function() {
                    input.focus();

                    // Close if already visible
                    if ( wasOpen ) {
                        return;
                    }

                    // Pass empty string as value to search for, displaying all results
                    input.autocomplete( "search", "" );
                });
        },

        _source: function( request, response ) {

            if(this.options.source && !this.options.preload){
                var term = request.term,
                    self = this,
                    cache = this.input.autocomplete("option","combocache");

                if(cache == "nocache"){
                    this._sourceFormSelect( request, response );
                    return true;
                }

                if ( term in cache ) {
                    self._parseData( cache[ term ], request, response);
                    return true;
                }

                $.getJSON( this.options.source, request, function( data, status, xhr ) {

                    self._parseData( data, request, response);

                    self.input.autocomplete("option","combocache")[ term ] = data;
                });
            }else{
                if(!this._initAttr) this.input.attr("id", this.element.attr("id") ? "ui-" + this.element.attr("id") : "").attr("name", this.element.attr("name") ? "ui-" + this.element.attr("name") : "");
                this._initAttr = true;
                this._sourceFormSelect( request, response );
            }
            return true;
        },

        _initAttr: false,

        _initDefaultValue: false,

        _parseData: function(data, request, response){
            var self = this;

            if(data.length && typeof data[0] == "object"){

                if(!self._initAttr) this.input.attr("id", this.element.attr("id") ? "ui-" + this.element.attr("id") : "").attr("name", this.element.attr("name") ? "ui-" + this.element.attr("name") : "");

                self.element.empty();

                $.each(data, function(){
                    self.addOption(this.description, this.value, self._initDefaultValue ? false : this.selected);
                });
                self._initDefaultValue = true;
                self._sourceFormSelect( request, response );

            }else{
                if(!self._initAttr) {
                    this.input.attr("id",this.element.attr("id") || "").attr("name",this.element.attr("name") || "");
                    this.element.attr("id", self.element.attr("id") ? "ui-" + this.element.attr("id") : "").attr("name", this.element.attr("name") ? "ui-" + this.element.attr("name") : "");
                }
                response( data );
            }
            self._initAttr = true;
        },

        _sourceFormSelect: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( /*this.value &&*/ ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },

        _removeIfInvalid: function( event, ui ) {

            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }

            // Search for a match (case-insensitive)
            var value = this.input.val(),
                valueLowerCase = value.toLowerCase(),
                valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });

            // Found a match, nothing to do
            if ( valid ) {
                return;
            }

            // Remove invalid value
            this.input.val( "" ).data( "ui-autocomplete" ).term = "";
            this.element.val( "" );
        },

        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        },

        updateSelected: function(value){
            var select = this.element,
                input = this.wrapper.children('input');

            var text = select.children( ":selected" ).removeProp("selected").end()
                .children("option[value='"+value+"']").prop("selected", true).text();

            input.val(text);
            select.trigger('change');
        },

        addOption: function(text,value,selected){
            var select = this.element;
            select.append($('<option value="'+value+'">'+text+'</option>'));
            if (selected)
                this.updateSelected(value);
        }

    });

})( jQuery );