<?php
$_SERVER['ENV_DEV'] = 'dev';
$_SERVER['ENV_PROD'] = 'prod';
$config_json = file_get_contents("../../config.json");
$obj = json_decode($config_json);

$DB_NAME_DEV = $obj->$_SERVER['ENV_DEV']->db->database;
$DB_NAME_PROD = $obj->$_SERVER['ENV_PROD']->db->database;
$DB_SERVER = $obj->$_SERVER['ENV_DEV']->db->host;
$DB_USER = $obj->$_SERVER['ENV_DEV']->db->user;
$DB_PASSWORD = $obj->$_SERVER['ENV_DEV']->db->password;

echo("Dumping...\n");
exec("mysqldump -u $DB_USER --password=\"$DB_PASSWORD\"  --databases $DB_NAME_PROD --host $DB_SERVER -C -n > dump_$DB_NAME_PROD.sql");
exec("sed -i 's/`$DB_NAME_PROD`/`$DB_NAME_DEV`/g' dump_$DB_NAME_PROD.sql");
exec("sed -i 's/`root`@`localhost`/`$DB_USER`@`%`/g' dump_$DB_NAME_PROD.sql");
echo("Deploing..\n");
exec("mysql -u $DB_USER --password=\"$DB_PASSWORD\"  --database $DB_NAME_DEV --host $DB_SERVER  < dump_$DB_NAME_PROD.sql");
echo("OK\n");
?>
