Добро пожаловать в репозиторий my.guu.ru.

Чтобы перейти непосредственно к личному кабинету - нажмите [ТУТ](https://my.guu.ru)

Личный кабинет работает в режиме Beta теста.


Полезные ссылки:
[Wiki проекта](https://gitlab.com/sum_moscow/my.guu.ru/wikis/home)
[Исходные коды](https://gitlab.com/sum_moscow/my.guu.ru/tree/dev)
[Обновления](https://gitlab.com/sum_moscow/my.guu.ru/commits/dev)

## О проблемах с личным кабинетом сообщайте на email my@guu.ru ##