function SendRequisition(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '/api/users/me/dashInfo',
		data: {}
	};

	_page.normalizeData = function(data){
		var render_data = data.data;
		render_data.unread_messages_hidden = render_data.unread_messages == null ? 'hidden' : '';
		return render_data;
	};




	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){
		var send_requisition_form = $('#send_requisition_form');

		send_requisition_form.find(".input-spinner").each(function (i, el) {
			var $this = $(el),
				$minus = $this.find('button:first'),
				$plus = $this.find('button:last'),
				$input = $this.find('input'),

				minus_step = attrDefault($minus, 'step', -1),
				plus_step = attrDefault($minus, 'step', 1),

				min = attrDefault($input, 'min', null),
				max = attrDefault($input, 'max', null);


			$this.find('button').on('click', function (ev) {
				ev.preventDefault();

				var $this = $(this),
					val = $input.val(),
					step = attrDefault($this, 'step', $this[0] == $minus[0] ? -1 : 1);

				if (!step.toString().match(/^[0-9-\.]+$/)) {
					step = $this[0] == $minus[0] ? -1 : 1;
				}
				if (!val.toString().match(/^[0-9-\.]+$/)) {
					val = 0;
				}
				$input.val(parseFloat(val) + step).trigger('keyup');
			});

			$input.keyup(function () {
				if (min != null && parseFloat($input.val()) < min) {
					$input.val(min);
				}
				else if (max != null && parseFloat($input.val()) > max) {
					$input.val(max);
				}
			});
		});
		replaceCheckboxes();


		function makeError($input){
			var $parent = $input.parents(".form-group");
			$parent.addClass("has-error");
			$input.on("input change", function(){
				$parent.removeClass("has-error");
			});
		}
		function sendRequisitionAction() {
			var data = [],
				is_valid = true,
				error_text = '',
				$where = $("#where_input"),
				$seal_type = send_requisition_form.find('[name="seal_type"]:checked'),
				$extra = send_requisition_form.find('[name="additional_info"]:checked'),
				$quantity = send_requisition_form.find("#quantity_input");

			if($where.val().trim() == ""){
				is_valid = false;
				makeError($where);
				error_text = 'Заполнены не все необходимые поля';
			}
			if(!$seal_type.length || !$extra.length){
				is_valid = false;
				error_text = 'Отмечены не все обязательные пункты';
			}
			if(parseInt($quantity.val()) != $quantity.val()){
				is_valid = false;
				makeError($quantity);
				error_text = 'Введите адекватное число копий';
			}

			if(is_valid){
				data.push(
					{name: $where.attr('name'), value: $where.val()},
					{name: $seal_type.attr('name'), value: $seal_type.val()},
					{name: $extra.attr('name'), value: $extra.val()},
					{name: $quantity.attr('name'), value: $quantity.val()},
					{name: 'type', value: 'institute'}
				);
				$.ajax({
					url: 'api/requisitions/',
					method: 'POST',
					data: data,
					dataType: 'JSON',
					success: function(response) {
						showNotifier(response);
					}
				});
			}
			else{
				toastr.error(error_text);
			}
		}
		$("#send_requisition_button").on("click", sendRequisitionAction);

		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}