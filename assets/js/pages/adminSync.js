function AdminSync(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/users/stars',
		data: {}
	};
	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){
		var _html = $('<div>');
		res.data.forEach(function(value){
			_html.append(tmpl('user-item', value));
		});
		return {stars_html: _html};
	};


	_page.afterOpen = function(){
		var $search_i = $('.search-input'),
			is_active = false,
			$members = $('.members');

		$('.control-event-info').each(function(){
			var $btn = $(this),
				$td = $btn.parents('th');
			$btn.popover({
				placement: 'bottom',
				trigger: 'click',
				html: true,
				content: tmpl('control-event-popover-content', $td.data()).get(0).outerHTML
			});
		});

		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}