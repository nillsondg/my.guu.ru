function ExtLibs(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/libraries/access_data',
		data: {}
	};
	var _super_after_open = _page.afterOpen;

	_page.normalizeData = function(res){
		var zn = res.data.znanium;
		return {
			znanium_link: zn.link + '?' + [
				'domain=' + zn.domain,
				'id=' + zn.id,
				'lname=' + zn.lname,
				'login=' + zn.login,
				'name=' + zn.name,
				'patr=' + zn.patr,
				'time=' + zn.time,
				'sign=' + zn.sign
			].join('&')
		}
	};


	_page.afterOpen = function(){
		var $lib_link = $('.lib-link'),
			$close_lib_frame = $('.close-lib-frame');


		$lib_link.on('click', function(){

			function showLibFrame(lib_name, link, logo_url){

				$('.main-content')
					.addClass('horizontal-menu')
					.addClass('fullscreen');

				$('.libs-iframe-navbar').removeClass('hidden');

				_page.$bench.find('.libs-env').addClass('hidden');

				$('#page-content').show();

				$('#proxy-frame')
					.removeClass('hidden')
					.attr('src', link)
					.css('height', window.innerHeight - 5 + 'px');

				$('.lib-logo').attr('src', logo_url);
				$('.page-container').addClass('sidebar-collapsed');

				$('.lib-link')
					.removeClass('active')
					.filter('.' + lib_name + '-link')
					.addClass('active');
			}
			var $this = $(this);
			showLibFrame($this.data('name'), $this.data('link'), $this.data('lib-logo-url'));
		});

		$close_lib_frame.on('click', _page.beforeClose);
		_super_after_open.call(this);
	};



	_page.beforeClose = function(){
		$('.main-content')
			.removeClass('horizontal-menu')
			.removeClass('fullscreen');

		$('.libs-iframe-navbar').addClass('hidden');

		_page.$bench.find('.libs-env').removeClass('hidden');

		$('#page-content').show();

		$('#proxy-frame').addClass('hidden');
	};

	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}