function Index(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	var notifications = null;

	_page.settings = {
		url: '/api/users/me/notifications',
		data: {}
	};

	_page.normalizeData = function(data){
		var render_data = data.data,
			brs_new_count = render_data.brs ? render_data.brs.length : 0,
			timetable_count = render_data.timetable ? render_data.timetable.length : 0,
			requisitions_count = render_data.requisitions.institute ? render_data.requisitions.institute.my.length : 0;

		if (render_data.requisitions != undefined){
			if (render_data.requisitions.institute && render_data.requisitions.institute.hasOwnProperty('all')){
				requisitions_count = render_data.requisitions.institute.all.length
			}
		}

		notifications = data.data;

		_page.afterOpen = function(){
			var $timeline = $("#index_timeline");

			if (render_data.brs && render_data.brs.length != 0){
				$('.tile-brs').show('slow');
				$('.tile-brs .tile-stats').on("click", function(){
					$('.go-to-email-btn').remove();
					$timeline.slideUp("slow", function(){
						$timeline.empty();
						render_data.brs.forEach(function(notification) {
							var date = new Date(notification.change_date);
							$timeline.append(
								tmpl('timeline-brs-row',
									{
										change_date: notification.change_date.replace(" ", "T"),
										readable_change_date: date.getReadableText("short"),
										readable_change_time: date.toLocaleTimeString().slice(0,-3),
										subject_name: notification.subject_name,
										changer_name: notification.changer_name,
										mark: notification.mark,
										cuid: notification.cuid
									}
								)
							)
						});
						$timeline.slideDown("slow");
					})
				});
			}else if (!getUserInfo().is_student){
				$('.tile-brs').hide('slow')
			}else if(render_data.brs.length == 0){
				$('.tile-brs').show('slow').find(".num").text("Изменений нет").css('font-size', '30px');;
				$('.tile-brs .tile-stats').off("click");
			}

			if (timetable_count != 0){
				$('.tile-timetable').show('slow');
				$('.tile-timetable .tile-stats').on("click", function(){
					$('.go-to-email-btn').remove();
					$timeline.slideUp("slow", function(){
						$timeline.empty();
						render_data.timetable.forEach(function(notification) {
							var date = new Date(notification.last_change_time),
								weeks = ["", "в понедельник", "во вторник", "в среду", "в четверг", "в пятницу", "в субботу", "в воскресенье"],
								class_type_text = ["", "ой", "ой", "ей", "ой"];
							$timeline.append(
								tmpl('timeline-timetable-row',
									{
										change_date: notification.last_change_time.replace(" ", "T"),
										readable_change_date: date.getReadableText("short"),
										readable_change_time: date.toLocaleTimeString().slice(0,-3),
										day_type: weeks[notification.day_type],
										week_type: notification.week_type,
										class_type: notification.class_type,
										class_type_text: class_type_text[notification.class_type],
										class_format_text: notification.class_format_text,
										subject_name: notification.subject_name,
										professor_id: notification.professor_id,
										full_name: notification.first_name + ' ' + notification.last_name + ' ' + notification.father_name,
										kaf_name: notification.kaf_name.replace("Кафедра", "Кафедры"),
										building_type: notification.building_type,
										room: notification.room
									}
								)
							)
						});
						$timeline.slideDown("slow");
					})
				});
			}
			else if(timetable_count == 0){
				//$('.tile-timetable').show('slow').find(".num").text("Изменений нет").css('font-size', '30px');
				$('.tile-timetable .tile-stats').off("click");
			}

			if (requisitions_count != 0){
				$('.tile-requisitions').show('slow');
				$('.tile-requisitions .tile-stats').on("click", function(){
					$('.go-to-email-btn').remove();
					var requisitions = render_data.requisitions.institute.all.concat(render_data.requisitions.institute.my);

					$timeline.slideUp("slow", function(){
						$timeline.empty();
						requisitions.forEach(function(notification) {
							var date = new Date(notification.create_date);
							$timeline.append(
								tmpl('timeline-requisitions-row',
									{
										receive_date: notification.create_date.replace(" ", "T"),
										readable_receive_date: date.getReadableText("short"),
										readable_receive_time: date.toLocaleTimeString().slice(0,-3),
										to_organization: notification.to_organization
									}
								)
							)
						});
						$timeline.slideDown("slow");
					})
				});
			}
			else if(requisitions_count == 0){
				//$('.tile-requisitions').show('slow').find(".num").text("Изменений нет").css('font-size', '30px');
				$('.tile-requisitions .tile-stats').off("click");
			}

			updateTiles();
			$('.notification-bar').addClass('hidden');
			var unread_count = $('.notification-bar .unread-emails-count').text();
			if (isNaN(parseInt(unread_count))){

			}
			refreshNotifications();
			_super_after_open.call(this);

		};


		return {
			brs_new_count: brs_new_count,
			timetable_count: timetable_count,
			requisitions_count: requisitions_count
		};
	};

	window.updateTiles = function(){
		$(".tile-stats").each(function(i, el) {
			var $this = $(el), $num = $this.find('.num'), start = attrDefault($num, 'start', 0), end = attrDefault($num, 'end', 0), prefix = attrDefault($num, 'prefix', ''), postfix = attrDefault($num, 'postfix', ''), duration = attrDefault($num, 'duration', 1000), delay = attrDefault($num, 'delay', 1000);
			if (start < end) {
				if (typeof scrollMonitor == 'undefined') {
					$num.html(prefix + end + postfix);
				}
				else {
					var tile_stats = scrollMonitor.create(el);
					tile_stats.fullyEnterViewport(function() {
						var o = {curr: start};
						TweenLite.to(o, duration / 1000, {curr: end, ease: Power1.easeInOut, delay: delay / 1000, onUpdate: function() {
							$num.html(prefix + Math.round(o.curr) + postfix);
						}});
						tile_stats.destroy()
					});
				}
			}
		});
	};


	var _super_after_open = _page.afterOpen;

	_page.beforeClose = function(){
		$('.notification-bar').removeClass('hidden');
	};

	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;
}