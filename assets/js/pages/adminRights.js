function AdminRights(_url){

	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект;


	_page.settings = {
		url: '/api/admin/rights',
		data: {}
	};
	var _super_after_open = _page.afterOpen;



	_page.showTimetableRoles = function(){
		var $table = $('#timetable-users');
		$table.find('.user-line').remove();
		$.ajax({
			url: 'api/admin/usersWithRightsInModule/timetable',
			success: function(res){
				var options = [];

				options.push(tmpl('role-option', res.data.roles));
				res.data.users.forEach(function(value){
					var $line = tmpl('timetable-users-line', value);

					$line
						.find('.group-name select')
						.append(options);

					$line
						.find('select')
						.select2()
						.select2('val', value.group_id)
						.on('change', function(e){
							$.ajax({
								url: '/api/admin/timetable/' + value.user_id + '/' + value.group_id,
								type: 'PUT',
								data: {group_id: e.val},
								success: showNotifier
							});
						});
					$line
						.find('.user-id-input')
						.on('blur', function(){

						});
					$table.find('.add-user-in-timetable')
						.before($line);
				});
			}
		})
	};


	_page.normalizeData = function(res){

	};


	_page.afterOpen = function(){
		var $tab_items = $('.tab-item');
		$tab_items.each(function(){
			var $this = $(this);

			$this.off('click').on('click', function(){
				function changeTab(){
					$('.tab-pane').removeClass('active').eq($tab_items.index($this)).addClass('active');
					if ($this.data('click-handler') && (_page.hasOwnProperty($this.data('click-handler')))){
						if (_page[$this.data('click-handler')] instanceof Function){
							_page[$this.data('click-handler')].call();
						}
					}
				}
				changeTab();
			});
		});
		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}