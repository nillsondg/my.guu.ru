function Profile(_url){
	var _page = Page(_url); // вызвать конструктор родителя, получить родительский объект

	_page.settings = {
		url: '/api/users/me/fullInfo',
		type: 'GET',
		data: {}
	};

	_page.normalizeData = function(res){
		var data = {
			staff_hidden: (res.data.main_info.is_staff) ? 'hidden' : '',
			student_hidden: (res.data.main_info.is_student) ? 'hidden' : ''
		};
		if (res.data.main_info.birth_date){
			res.data.main_info.birth_date = moment(res.data.main_info.birth_date, 'YYYY-MM-DD').format('DD.MM.YYYY');
		}
		return $.extend({}, res.data.main_info, res.data.staff_info, res.data.student_info);
	};

	var _super_after_open = _page.afterOpen;

	_page.afterOpen = function(){

		function handleFileSelect(evt) {
			var files = evt.target.files; // FileList object
			for(var i = 0, f; f = files[i]; i++) {
				if (!f.type.match('image.*')) {
					continue;
				}
				if (f.size / 1024 > 4096){
					showNotifier({status: false, text: 'Извините, максимально допустимый развер аватара - 4 МБ. Уменьште изображение.'});
					return;
				}
				var reader = new FileReader();
				reader.onload = (function(the_file) {
					return function(e) {
						var file = e.target.result;
						$.ajax({
							url: '/api/users/me/avatar',
							type: 'POST',
							data: {
								file: file,
								file_name: the_file.name
							},
							success: function(res){
								$('.avatar-img').attr('src', e.target.result);
							}
						});
					};
				})(f);
				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		function updateProfileInfo(){
			var $btn = $(this),
				send_data = [];
			if ($btn.hasClass('disabled')) return;

			$btn.addClass('disabled');

			$('input,textarea').each(function(){
				var $this = $(this),
					_name = $this.attr('name'),
					_value = $this.val();
				if (_name == 'birth_date'){
					_value = moment(_value, 'DD/MM/YYYY');
					if (_value != null){
						_value = _value.format('YYYY-MM-DD')
					}
				}
				send_data.push({
					name: _name,
					value: _value
				});
			});
			$.ajax({
				url: 'api/users/me/',
				data: send_data,
				type: 'POST',
				success: function(res){
					showNotifier(res);
					$btn.removeClass('disabled');
				}
			});
		}

		function openMakeAvatarModal(){
			var $modal = $('#make-avatar-modal');
				$modal
					.on('hidden.bs.modal', function(){
						$('#live').data( "photobooth" ).destroy();
					})
					.on('shown.bs.modal', function(){
					$( "#image-shoot").addClass('hidden');
					$('#live').empty().photobooth().on("image",function( event, dataUrl ){
						$( "#image-shoot").removeClass('hidden').find("img").attr('src',  dataUrl);

						$(".upload-made-photo").removeClass('disabled').off('click').on('click', function(){
							$.ajax({
								url: '/api/users/me/avatar',
								type: 'POST',
								data: {
									file: dataUrl,
									file_name: 'generated-pic.png'
								},
								success: function(res){
									$('.avatar-img').attr('src', dataUrl);
									$modal.modal('hide');
								}
							});
						});
					});
				})
				.modal('show', {backdrop: 'static'});
		}

		$('#avatar-uploader').on('change', handleFileSelect);
		$('.make-avatar-btn').on('click', openMakeAvatarModal);
		$('.update-info').on('click', updateProfileInfo);

		_super_after_open.call(this);
		$("[data-mask]").each(function(){
			$(this).inputmask($(this).data('mask'), {});
		})
	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}