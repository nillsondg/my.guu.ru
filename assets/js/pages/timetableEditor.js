function TimetableEditor(_url){

	var _page = Page(_url), // вызвать конструктор родителя, получить родительский объект
		shifts = {
			times: {
				'1': ['8:15-9:45', '9:55-11:25', '11:35-13:05', '13:40-15:10'],
				'2': ['11:35-13:05', '13:15-14:45', '15:20-16:50', '17:00-18:30']
			},
			classes: {
				'1': 'switch-on',
				'2': 'switch-off'
			}
		},
		dragging_line = null,
		$paste_label = null;


	_page.settings = {
		url: '',
		data: {}
	};

	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){

		var updateKafsList = function(){
				$.ajax({
					url: 'api/dictionary/kafs',
					dataType: 'JSON',
					success: function(res){
						var opts = ['<option></option>'];
						res.data.forEach(function(value){
							opts.push('<option value="' + value.kaf_id + '">' + value.kaf_name + '</option>')
						});
						opts = opts.join();
						$('.kafs-list').html(opts).select2().each(function(){
							var $this = $(this),
								kaf_selected = $this.parents('tr').data('kaf-selected'),
								professor_selected = $this.parents('tr').data('professor-selected'),
								$pps_list = $this.parents('tr').find('select.pps-list');
							$pps_list.select2('disable');
							$this.off('change').on('change', function(e){
								kaf_selected = $this.parents('tr').data('kaf-selected');
								professor_selected = $this.parents('tr').data('professor-selected');
								$.ajax({
									url: 'api.php?act=kaf.getProfessorsList',
									data: {kaf_id: e.val ? e.val : kaf_selected},
									success: function(res){
										var prof_opts = ['<option></option>'];
										res.data.forEach(function(value){
											prof_opts.push('<option value="' + value.user_id + '">' + [value.first_name, value.last_name, value.father_name].join(' ') + '</option>');
										});
										$pps_list.select2('destroy')
											.html(prof_opts.join(''))
											.select2()
											.select2('enable');
										$('.select2-container.pps-list').css('width', '270px');
										if (!e.val){
											$pps_list.select2('val', professor_selected);
										}
									}
								})
							});
							if (!isNaN(parseInt(kaf_selected))){
								$this
									.select2('val', kaf_selected)
									.trigger('change');
							}
						});
						$('.select2-container.kafs-list').css('width', '270px');

					}
				});
			},
			bindFormHandlers = function(res){
				var empty_item = {
						building_type: '',
						subject_name: '',
						room: '',
						kaf_id: '',
						professor_id: ''
				}, curr_item = {},
					key = '';

				$('.timetable-item ').each(function(){
					var $tr = $(this);
					key = [$tr.data('day-type'), $tr.data('class-type'), $tr.data('week-type')].join('-');
					curr_item = (res.data.timetable[key]) ? res.data.timetable[key] : empty_item;

					$tr.find('.subject-name').val(curr_item.subject_name).typeahead({
						remote: 'http://my.guu.ru/api.php?act=suggests.subjects&q=%QUERY',
						displayKey: 'data'
					});

					$tr.find('.buildings-list.select2').select2().select2('val', curr_item.building_type);
					$tr.find('.room-number').val(curr_item.room).inputmask('Regex', {regex: '\\d+.*' });

					$tr.data('kaf-selected', curr_item.kaf_id);
					$tr.data('professor-selected', curr_item.professor_id);

					curr_item.class_format = curr_item.class_format ? curr_item.class_format : '9';

					$tr.find('.class-format-btn')
						.on('click', function(){
							var $this = $(this);
							$this.siblings('button').removeClass('active');
							$this.toggleClass('active');

						}).eq(curr_item.class_format).addClass('active');
					$tr.find('.info').popover({
						content: function(){
							var $this = $(this),
								errors = $this.data('errors'),
								warnings = $this.data('warnings'),
								result = '';
							if (errors.length > 0){
								result += '<div class="alert alert-danger"><strong>Ошибки!</strong><ul><li>' + errors.join('</li><li>') + '</li></ul></div>';
							}
							if (warnings.length > 0){
								result += '<div class="alert alert-warning"><strong>Предупреждения!</strong><ul><li>' + warnings.join('</li><li>') + '</li></ul></div>';
							}
							return result;
						},
						html: true,
						placement: 'left',
						title: 'Ошибки и предупреждения',
						trigger: 'hover'
					});
				});


				$('.pps-list').each(function(){
					$(this).select2().select2('disable');
				});

				updateKafsList();

				$('.clear-line').on('click', function(){
					var $this_tr = $(this).parents('tr');
					$this_tr.find('.subject-name').val('');
					$this_tr.find('.room-number').val('');
					$this_tr.find('select.buildings-list').select2('val', '');
					$this_tr.find('select.kafs-list').select2('val', '');
					$this_tr.find('select.pps-list').select2('val', '');
				});
				$('.save-timetable').removeClass('hidden').off('click').on('click', function(){
					var send_array = [],
						$btn = $(this);
					$('.timetable-item').each(function(index){
						var $this = $(this);
						var subject_name = $this.find('.subject-name').val().trim(),
							kaf_id = $this.find('select.kafs-list').select2('val'),
							professor_id = $this.find('select.pps-list').select2('val'),
							class_format = $this.find('.class-format-btn.active').data('value'),
							room = $this.find('.room-number input').val();

						send_array.push({name: 'day_type_' + index, value: $this.data('day-type')});
						send_array.push({name: 'class_type_' + index, value: $this.data('class-type')});
						send_array.push({name: 'week_type_' + index, value: $this.data('week-type')});
						send_array.push({name: 'subject_name_' + index, value: subject_name});
						send_array.push({name: 'building_type_' + index, value: $this.find('select.buildings-list').select2('val')});
						send_array.push({name: 'kaf_id_' + index, value: kaf_id});
						send_array.push({name: 'professor_id_' + index, value: professor_id});
						send_array.push({name: 'room_' + index, value: room});
						send_array.push({name: 'class_format_' + index, value: class_format});
					});

					$btn.popover({
						placement: 'left',
						content: 'Идет обновление данных, подождите пожалуйста... Скоро все будет...',
						title: 'Обновляемся.',
						html: true
					})
						.popover('show')
						.addClass('disabled');
					$.ajax({
						url: 'api/student_group/' + $('select.select2.profiles').select2('val') + '/timetable',
						type: 'POST',
						data: send_array,
						success: function(res){
							showNotifier(res);
							var sum_notifies_count = {
								errors: 0,
								warnings: 0
							};
							res.data.forEach(function(value, index){
								var $line = $('.timetable-item:eq(' + index + ')'),
									$btn = $line.find('.info'),
									errors_count = value.errors.length,
									warnings_count = value.warnings.length;
								if (errors_count > 0){
									$line.addClass('danger');
									$btn.removeClass('hidden');
								}else if (warnings_count > 0){
									$line.addClass('warning');
									$btn.removeClass('hidden');
								}
								$btn
									.data('warnings', value.warnings)
									.data('errors', value.errors);
								sum_notifies_count.warnings += warnings_count;
								sum_notifies_count.errors += errors_count;
							});
							$('.popover').addClass('popover-primary');
							$('.popover-title').html('Обновлено! ' +
								'<p> Предупреждений: <span class="badge badge-warning badge-roundless">' + sum_notifies_count.warnings +'</span></p>' +
								'<p> Ошибок: <span class="badge badge-danger badge-roundless">' + sum_notifies_count.errors +'</span></p>'
							);
							$('.popover-content').text('Данные обновлены!');

							setTimeout(function(){
								$btn.popover('hide').removeClass('disabled');
							}, 7000);
						}
					});
				});
			},

			showGroupsTimeTable = function(group_id){
				var buildTimeTable = function(info){
					var i = 0, k = 0, color_class = 'active',
						$table = $('.timetable-tbody').empty(),
						days_of_week = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
					for (; i < 6; i++){
						color_class = (i % 2  == 0) ? '' : 'active';
						/*Append day of week*/
							tmpl('timetable-day', {color_class: color_class, day_name: days_of_week[i]}, $table, 'append');
						for (k = 0; k < 4; k++){
							/* Append class type (class time)*/
							tmpl('timetable-class-type', {
								color_class: color_class,
								class_type: k,
								class_time: shifts.times['' + info.shift][k]
							}, $table, 'append');


							/*Append item for even weeks*/
							tmpl('timetable-class-info', {
								color_class: color_class,
								week_type_text: 'Нечетная',
								day_type: (i + 1),
								class_type: (k + 1),
								week_type: 1
							}, $table, 'append');

							/*Append item for odd weeks*/
							tmpl('timetable-class-info', {
								color_class: color_class,
								week_type_text: 'Четная',
								day_type: (i + 1),
								class_type: (k + 1),
								week_type: 2
							}, $table, 'append');
						}
					}
				};
				$.ajax({
					url: 'api/student_group/' + group_id + '/timetable',
					type: 'GET',
					success: function(res){
						buildTimeTable(res.data.group_info);
						bindFormHandlers(res);

						/*

						Draggable lines for timetable

						* */
						$paste_label = $('.copy-line-drag');
						$('.timetable-item').find('td.week-type').off('mousedown.timetable-copy-line').on('mousedown.timetable-copy-line', function(e){

							var  button = e.which || e.button;
							if (button != 1) return true; //not left button
							dragging_line = $(this).parents('tr');
							$(window).off('mousemove.timetable-copy-line').on('mousemove.timetable-copy-line', function(mouseMove){
								if (dragging_line){
									var $on_el = $(mouseMove.target);
									if ($on_el.is('td') && $on_el.parents('tr.timetable-item').length == 1){
										$paste_label
											.removeClass('label-info')
											.addClass('label-success')
											.text('Бросьте, чтобы скопировать');
									}else{
										$paste_label
											.removeClass('label-success')
											.addClass('label-info')
											.text('Перетащите на другую строку...');
									}
									$paste_label.css({
										left: mouseMove.clientX + 5 + 'px',
										top: mouseMove.clientY + 5 + 'px'
									}).show();
								}
							});
							return false;
						});

						$(window).on('mouseup', function(e){
							var $copy_to_tr = $(e.target).parents('tr.timetable-item');
							if (dragging_line && dragging_line.is($copy_to_tr) == false){

								$copy_to_tr.find('.subject-name').val(dragging_line.find('.subject-name').val());
								$copy_to_tr.find('select.buildings-list').select2('val',dragging_line.find('select.buildings-list').select2('val'));
								$copy_to_tr.find('input.room-number').val(dragging_line.find('input.room-number').val());
								$copy_to_tr
									.data({
										'kaf-selected': dragging_line.find('select.kafs-list').select2('val'),
										'professor-selected': dragging_line.find('select.pps-list').select2('val')
									})
									.find('select.kafs-list')
									.select2('val', dragging_line.find('select.kafs-list').select2('val'))
									.trigger('change');

								$copy_to_tr.find('.class-format-btn')
									.removeClass('active')
									.eq(dragging_line.find('.class-format-btn.active').data('value'))
								    .addClass('active');


								$paste_label.hide();
								$(window).off('mousemove.timetable-copy-line');
								dragging_line = null;
							}
						});
						/**/
					}
				});
			};
		$.ajax({
			url: 'api/dictionary/institutes',
			dataType: 'JSON',
			success: function(res){
				var $institutes = $('.select2.institutes').html("<option></option>"),
					$profiles = $('.select2.profiles').html("<option></option>"),
					$courses = $('.select2.courses');
				res.data.forEach(function(value){
					$institutes.append('<option value="' + value.institute_id + '" data-value="' + value.institute_id + '">' + value.institute_name + '</option>')
				});
				$institutes.select2();
				$profiles.select2().select2('disable');
				$institutes.off('change').on('change', function(inst_e){
					$profiles.select2('disable');
					$('.student-groups-shift').addClass('deactivate');
					$courses.select2('val', '').off('change').on('change', function(course_e){
						$('.student-groups-shift').addClass('deactivate');
						$profiles.select2('disable');
						$.ajax({
							url: 'api/institute/' + inst_e.val + '/student_groups/' +course_e.val,
							type: 'GET',
							success: function(res){
								$profiles.select2('destroy').html("<option></option>");
								res.data.forEach(function(value){
									$profiles.append('<option id="st_group_' + value.student_group_id + '" value="' + value.student_group_id + '" data-shift="' + value['shift'] + '">' + value.student_group_name + ' - ' + value['student_group_number'] + '</option>');
								});
								$profiles.select2().select2('enable').off('change').on('change', function(profile_e){
									var current_shift = $('#st_group_' + profile_e.val).data('shift');
									$('.student-groups-shift')
										.data('shift', current_shift)
										.find('.switch-toggler')
										.removeClass('switch-off switch-on')
										.addClass(shifts.classes[current_shift + '']);
									showGroupsTimeTable(profile_e.val);
								});
								$('.student-groups-shift')
									.removeClass('deactivate')
									.off('mouseup')
									.on('mouseup', function(){
										var new_shift = ($(this).find('.switch-toggler').hasClass(shifts.classes['1'])) ? 2 : 1,
											student_group_id = $profiles.select2('val');
										$('#st_group_' + student_group_id).data('shift', new_shift);
										for(var i = 0; i < 4; i++){
											$('.class-time-' + i).text(shifts.times['' + new_shift][i]);
										}
										$.ajax({
											url: 'api/student_group/' + student_group_id + '/shift',
											type: 'PUT',
											data: {
												shift: new_shift
											}
										});
									});
							}
						});
					});
				});
			}
		});
		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}