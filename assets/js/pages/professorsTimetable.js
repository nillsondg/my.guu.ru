function ProfessorsTimetable(_url){

	var _page = Page(_url), // вызвать конструктор родителя, получить родительский объект
		shifts = {
			times: {
				'1': ['8:15-9:45', '9:55-11:25', '11:35-13:05', '13:40-15:10'],
				'2': ['11:35-13:05', '13:15-14:45', '15:20-16:50', '17:00-18:30']
			}
		};


	_page.settings = {
		url: '',
		data: {}
	};

	var _super_after_open = _page.afterOpen;
	_page.afterOpen = function(){

		var buildTimeTable = function(res){
			var i = 0, k = 0, color_class = 'active',
				$table = $('.timetable-tbody').empty(),
				timetable = res.data.timetable,
				line = {}, key,
				color_class_for_day = '',
				week_type = 1,
				weeks_classes,
				start_date = new Date(window.getBRSInfo().semester_start),
				curr_date = new Date(start_date),
				_today = new Date(),
				today_text = [(_today.getMonth() + 1), _today.getDate(), ],
				days_of_week = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

			do{
				curr_date.setDate(curr_date.getDate() + 1);
				if (curr_date.getDay() == 0){ // Change week type on Monday
					week_type = (week_type == 1) ? 2 : 1;
				}
			}while(curr_date <= _today);

			for (i = 0; i < 6; i++){
				color_class = (i % 2  == 0) ? '' : 'active';
				if (i == _today.getDay() - 1){
					color_class_for_day = ' success';
					weeks_classes = [' ', ' success'];
				}else{
					color_class_for_day = '';

					weeks_classes = [' ', ' '];
				}
				/*Append day of week*/
				tmpl('my-timetable-day', {color_class: color_class + color_class_for_day, day_name: days_of_week[i]}, $table, 'append');
				for (k = 0; k < 4; k++){
					/* Append class type (class time)*/

					tmpl('my-timetable-class-type', {
						color_class: color_class,
						class_type: k,
						class_time: shifts.times['' + res.data.group_info.shift][k]
					}, $table, 'append');


					line = {
						color_class: (color_class + weeks_classes[0]),
						week_type_text: 'Нечетная',
						day_type: (i + 1),
						class_type: (k + 1),
						week_type: 1
					};
					key = [line.day_type, line.class_type, line.week_type].join('-');
					if (timetable.hasOwnProperty(key)){
						line = $.extend(line, timetable[key]);
					}

					/*Append item for even weeks*/
					tmpl('my-timetable-class-info', line, $table, 'append');


					/*Append item for odd weeks*/
					line = {
						color_class: (color_class + weeks_classes[1]),
						week_type_text: 'Четная',
						day_type: (i + 1),
						class_type: (k + 1),
						week_type: 2
					};
					key = [line.day_type, line.class_type, line.week_type].join('-');
					if (timetable.hasOwnProperty(key)){
						line = $.extend(line, timetable[key]);
					}

					tmpl('my-timetable-class-info', line, $table, 'append');
				}
			}
		};

		$.ajax({
			url: 'api.php?act=user.getMyTimetable',
			success: buildTimeTable
		});

		_super_after_open.call(this);

	};


	// (делаем вид, что объект создали мы, а не Page)
	_page.constructor = arguments.callee;
	return _page;

}